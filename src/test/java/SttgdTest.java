import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class SttgdTest {

    @Test
    public void testSttgd1c() {
        // actual result
        Instance result = Commons.readInput("/example_1c_I_STTGD.xml");
        assertNotNull(result);

        // expected result:
        // Grades(7, 3, #N_semester_3, #N_grade_1)
        // Grades(2, 3, #N_semester_4, #N_grade_2)

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        // Grades(7, 3, #N_semester_3, #N_grade_1) atom
        ArrayList<Term> terms_1 = new ArrayList<Term>();
        terms_1.add(Constant.fromInteger("module_id", 7));
        terms_1.add(Constant.fromInteger("student_id", 3));
        terms_1.add(new Null("semester", 3));
        terms_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_1);

        // Grades(2, 3, #N_semester_4, #N_grade_2) atom
        ArrayList<Term> terms_2 = new ArrayList<Term>();
        terms_2.add(Constant.fromInteger("module_id", 2));
        terms_2.add(Constant.fromInteger("student_id", 3));
        terms_2.add(new Null("semester", 4));
        terms_2.add(new Null("grade", 2));
        RelationalAtom grades_2 = new RelationalAtom("Grades", terms_2);

        // Grades(7, 3, #N_semester_4, #N_grade_2) atom
        ArrayList<Term> terms_3 = new ArrayList<Term>();
        terms_3.add(Constant.fromInteger("module_id", 7));
        terms_3.add(Constant.fromInteger("student_id", 3));
        terms_3.add(new Null("semester", 4));
        terms_3.add(new Null("grade", 2));
        RelationalAtom grades_3 = new RelationalAtom("Grades", terms_3);

        // Grades(2, 3, #N_semester_3, #N_grade_1) atom
        ArrayList<Term> terms_4 = new ArrayList<Term>();
        terms_4.add(Constant.fromInteger("module_id", 2));
        terms_4.add(Constant.fromInteger("student_id", 3));
        terms_4.add(new Null("semester", 3));
        terms_4.add(new Null("grade", 1));
        RelationalAtom grades_4 = new RelationalAtom("Grades", terms_4);

        // add all possible atoms to expected atoms set
        expectedAtoms.add(grades_1);
        expectedAtoms.add(grades_2);
        expectedAtoms.add(grades_3);
        expectedAtoms.add(grades_4);

        HashSet<RelationalAtom> resultAtoms = result.getRelationalAtoms();

        // count occurences; if exactly two atoms out of expectedAtoms
        // are present in the result and vice versa, the result is okay

        int occurrencesLeft = 0;
        for (RelationalAtom atom : expectedAtoms) {
            if (resultAtoms.contains(atom)) {
                occurrencesLeft++;
            }
        }

        int occurrencesRight = 0;
        for (RelationalAtom atom : resultAtoms) {
            if (expectedAtoms.contains(atom)) {
                occurrencesRight++;
            }
        }

        assertEquals(2, occurrencesLeft);
        assertEquals(2, occurrencesRight);
    }

    @Test
    public void testSttgd1cNegation() {
        // actual result
        Instance result = Commons.readInput("/example_1c_I_STTGD_negation.xml");
        assertNotNull(result);

        // expected result:
        // Grades(7, 3, #N_semester_3, #N_grade_1)
        // Grades(2, 3, #N_semester_4, #N_grade_2)

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        // Grades(7, 3, #N_semester_3, #N_grade_1) atom
        ArrayList<Term> terms_1 = new ArrayList<Term>();
        terms_1.add(Constant.fromInteger("module_id", 7));
        terms_1.add(Constant.fromInteger("student_id", 3));
        terms_1.add(new Null("semester", 3));
        terms_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_1);

        // Grades(2, 3, #N_semester_4, #N_grade_2) atom
        ArrayList<Term> terms_2 = new ArrayList<Term>();
        terms_2.add(Constant.fromInteger("module_id", 2));
        terms_2.add(Constant.fromInteger("student_id", 3));
        terms_2.add(new Null("semester", 4));
        terms_2.add(new Null("grade", 2));
        RelationalAtom grades_2 = new RelationalAtom("Grades", terms_2);

        // Grades(7, 3, #N_semester_4, #N_grade_2) atom
        ArrayList<Term> terms_3 = new ArrayList<Term>();
        terms_3.add(Constant.fromInteger("module_id", 7));
        terms_3.add(Constant.fromInteger("student_id", 3));
        terms_3.add(new Null("semester", 4));
        terms_3.add(new Null("grade", 2));
        RelationalAtom grades_3 = new RelationalAtom("Grades", terms_3);

        // Grades(2, 3, #N_semester_3, #N_grade_1) atom
        ArrayList<Term> terms_4 = new ArrayList<Term>();
        terms_4.add(Constant.fromInteger("module_id", 2));
        terms_4.add(Constant.fromInteger("student_id", 3));
        terms_4.add(new Null("semester", 3));
        terms_4.add(new Null("grade", 1));
        RelationalAtom grades_4 = new RelationalAtom("Grades", terms_4);

        // add all possible atoms to expected atoms set
        expectedAtoms.add(grades_1);
        expectedAtoms.add(grades_2);
        expectedAtoms.add(grades_3);
        expectedAtoms.add(grades_4);

        HashSet<RelationalAtom> resultAtoms = result.getRelationalAtoms();

        // count occurences; if exactly two atoms out of expectedAtoms
        // are present in the result and vice versa, the result is okay

        int occurrencesLeft = 0;
        for (RelationalAtom atom : expectedAtoms) {
            if (resultAtoms.contains(atom)) {
                occurrencesLeft++;
            }
        }

        int occurrencesRight = 0;
        for (RelationalAtom atom : resultAtoms) {
            if (expectedAtoms.contains(atom)) {
                occurrencesRight++;
            }
        }

        assertEquals(2, occurrencesLeft);
        assertEquals(2, occurrencesRight);
    }

    @Test
    public void testSttgd1cNegation2() {
        // actual result
        Instance result = Commons.readInput("/example_1c_I_STTGD_negation2.xml");
        assertNotNull(result);

        // expected result:
        // -

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testSchemaSorting() {
        Instance result = Commons.readInput("/example_wrong_order.xml");
        assertNotNull(result);

        // expected:
        ArrayList<Term> lastname = new ArrayList<Term>();
        lastname.add(Constant.fromString("lastname", "Sonnenschein"));
        RelationalAtom resultAtom = new RelationalAtom("Result", lastname);

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();
        expectedAtoms.add(resultAtom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

}
