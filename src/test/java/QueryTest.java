import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Term;
import term.Variable;
import term.VariableType;

public class QueryTest {

    @Test
    public void testQueryTgd() {
        // actual result
        Instance result = Commons.readInput("/example_5_Q_TGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "module_id", 1));
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "semester", 1));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_grades_2 = new ArrayList<Term>();
        terms_grades_2.add(new Variable(VariableType.FOR_ALL, "module_id", 2));
        terms_grades_2.add(new Variable(VariableType.FOR_ALL, "student_id", 2));
        terms_grades_2.add(new Variable(VariableType.EXISTS, "semester", 2));
        terms_grades_2.add(new Variable(VariableType.EXISTS, "grade", 2));
        RelationalAtom grades_2 = new RelationalAtom("Grades", terms_grades_2);

        expectedAtoms.add(grades_2);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(new Variable(VariableType.EXISTS, "student_id", 3));
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "lastname", 3));
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "firstname", 3));
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "course", 3));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_students_2 = new ArrayList<Term>();
        terms_students_2.add(new Variable(VariableType.FOR_ALL, "student_id", 2));
        terms_students_2.add(new Variable(VariableType.FOR_ALL, "lastname", 2));
        terms_students_2.add(new Variable(VariableType.FOR_ALL, "firstname", 2));
        terms_students_2.add(new Variable(VariableType.FOR_ALL, "course", 2));
        RelationalAtom students_2 = new RelationalAtom("Students", terms_students_2);

        expectedAtoms.add(students_2);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "module_id", 2));
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "student_id", 2));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(new Variable(VariableType.FOR_ALL, "module_id", 3));
        terms_participants_2.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
        
     // expected head:
        HashSet<RelationalAtom> expectedHeadAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> headTerms = new ArrayList<Term>();
        headTerms.add(new Variable(VariableType.FOR_ALL, "student_id", 2));
        headTerms.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        RelationalAtom headAtom = new RelationalAtom("", headTerms);

        expectedHeadAtoms.add(headAtom);
        
        assertEquals(expectedHeadAtoms, result.getHeadAtoms());
    }

    @Test
    public void testQueryTgdEgd() {
        // actual result
        Instance result = Commons.readInput("/example_6_Q_TGD_EGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "module_id", 3));
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "semester", 1));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        terms_students_1.add(Constant.fromInteger("lastname", 2));
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "firstname", 3));
        terms_students_1.add(new Variable(VariableType.EXISTS, "course", 2));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "module_id", 2));
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "student_id", 2));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(new Variable(VariableType.FOR_ALL, "module_id", 3));
        terms_participants_2.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
        
        // expected head:
        HashSet<RelationalAtom> expectedHeadAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> headTerms1 = new ArrayList<Term>();
        headTerms1.add(new Variable(VariableType.FOR_ALL, "student_id", 3));
        RelationalAtom headAtom1 = new RelationalAtom("", headTerms1);

        expectedHeadAtoms.add(headAtom1);        

        ArrayList<Term> headTerms2 = new ArrayList<Term>();
        headTerms2.add(new Variable(VariableType.FOR_ALL, "firstname", 3));
        RelationalAtom headAtom2 = new RelationalAtom("", headTerms2);

        expectedHeadAtoms.add(headAtom2);
       
        ArrayList<Term> headTerms3 = new ArrayList<Term>();
        headTerms3.add(new Variable(VariableType.FOR_ALL, "module_id", 1));
        headTerms3.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        headTerms3.add(Constant.fromInteger("count", 2));
        headTerms3.add(new Variable(VariableType.FOR_ALL, "firstname", 3));
        RelationalAtom headAtom3 = new RelationalAtom("Test_Head1", headTerms3);

        expectedHeadAtoms.add(headAtom3);

        ArrayList<Term> headTerms4 = new ArrayList<Term>();
        headTerms4.add(new Variable(VariableType.FOR_ALL,"firstname", 3));
        RelationalAtom headAtom4 = new RelationalAtom("Test_Head2", headTerms4);

        expectedHeadAtoms.add(headAtom4);

        
        assertEquals(expectedHeadAtoms, result.getHeadAtoms());
    }

    @Test
    public void testQueryTableau() {
        // actual result
        Instance result = Commons.readInput("/example_7_Q_Tableau.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "module_id", 1));
        terms_grades_1.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "bla", 31));
        terms_grades_1.add(new Variable(VariableType.EXISTS, "blub", 34));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        terms_students_1.add(new Variable(VariableType.EXISTS, "lastname", 2));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("course", "ITTI"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "module_id", 1));
        terms_participants_1.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
        
        // expected head:
        HashSet<RelationalAtom> expectedHeadAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> headTerms = new ArrayList<Term>();
        headTerms.add(new Variable(VariableType.FOR_ALL, "student_id", 1));
        headTerms.add(new Variable(VariableType.FOR_ALL, "module_id", 1));
        headTerms.add(Constant.fromString("course", "ITTI"));
        RelationalAtom headAtom = new RelationalAtom("", headTerms);

        expectedHeadAtoms.add(headAtom);
        
        assertEquals(expectedHeadAtoms, result.getHeadAtoms());
    }

}
