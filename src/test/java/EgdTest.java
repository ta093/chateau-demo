import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class EgdTest {

    @Test
    public void testEgd2() {
        // actual result
        Instance result = Commons.readInput("/example_2_I_EGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(new Null("course", 1));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(Constant.fromInteger("module_id", 2));
        terms_participants_2.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testEgd2HeadConstant() {
        // actual result
        Instance result = Commons.readInput("/example_2_I_EGD_Head_Constant.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromInteger("course", 1));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);        

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testEgd2DoubleExecution() {
        // actual result
        Instance result = Commons.readInput("/example_2_I_EGD_double_execution.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Miller"));
        terms_students_1.add(new Null("course", 1));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);        

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testEgd4Fail() {
        // actual result
        Instance result = Commons.readInput("/example_4_I_EGD_Fail.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

}
