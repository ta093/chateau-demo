import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Term;

public class FunctionsTest {

    @Test
    public void testCount() {
        Instance result = Commons.readInput("/example_count.xml");
        assertNotNull(result);

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> resultTerm = new ArrayList<Term>();
        resultTerm.add(Constant.fromInteger("x", 5));
        RelationalAtom resultAtom = new RelationalAtom("Res", resultTerm);

        expectedAtoms.add(resultAtom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testSum() {
        Instance result = Commons.readInput("/example_sum.xml");
        assertNotNull(result);

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> resultTerm = new ArrayList<Term>();
        resultTerm.add(Constant.fromInteger("x", 7190));
        RelationalAtom resultAtom = new RelationalAtom("Res", resultTerm);

        expectedAtoms.add(resultAtom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testAvg() {
        Instance result = Commons.readInput("/example_avg.xml");
        assertNotNull(result);

        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> resultTerm = new ArrayList<Term>();
        resultTerm.add(Constant.fromDouble("x", 1438.2));
        RelationalAtom resultAtom = new RelationalAtom("Res", resultTerm);

        expectedAtoms.add(resultAtom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
}
