import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class ConstraintTest {

    @Test
    public void testTgdWrong() {
        // actual result
        var result = Commons.checkInput("/example_1_I_TGD_wrong_constraint.xml");
        assertNotNull(result);        

        assertEquals(false, result);
    }

    @Test
    public void testTgdTrue() {
        // actual result
        var result = Commons.checkInput("/example_1_I_TGD.xml");
        assertNotNull(result);        

        assertEquals(true, result);
    }

    @Test
    public void testSTTgdWrong() {
        // actual result
        var result = Commons.checkInput("/example_1c_I_STTGD_wrong_constraint.xml");
        assertNotNull(result);        

        assertEquals(false, result);
    }

    @Test
    public void testSTTgdTrue() {
        // actual result
        var result = Commons.checkInput("/example_1c_I_STTGD.xml");
        assertNotNull(result);        

        assertEquals(true, result);
    }

    @Test
    public void testSTTgdTrueNegation() {
        // actual result
        var result = Commons.checkInput("/example_1c_I_STTGD_negation.xml");
        assertNotNull(result);        

        assertEquals(true, result);
    }
    
    @Test
    public void testEgdWrong() {
        // actual result
        var result = Commons.checkInput("/example_2_I_EGD_wrong_constraint.xml");
        assertNotNull(result);        

        assertEquals(false, result);
    }

    @Test
    public void testEgdTrue() {
        // actual result
        var result = Commons.checkInput("/example_2_I_EGD.xml");
        assertNotNull(result);        

        assertEquals(true, result);
    }
}
