import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class ComparisonsTest {

    @Test
    public void testLessThan() {
        Instance result = Commons.readInput("/example_comparisons_lt.xml");
        assertNotNull(result);

        // expected:
        // Grades(1, 2, #N_semester_4, #N_grade_1)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<>();

        // result atom
        ArrayList<Term> terms = new ArrayList<>();
        terms.add(Constant.fromInteger("student_id", 1));
        terms.add(Constant.fromInteger("module_id", 2));
        terms.add(new Null("semester", 4));
        terms.add(new Null("grade", 1));
        RelationalAtom atom = new RelationalAtom("Grades", terms);

        expectedAtoms.add(atom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testGreaterThan() {
        Instance result = Commons.readInput("/example_comparisons_gt.xml");
        assertNotNull(result);

        // expected:
        // Grades(2, 7, #N_semester_4, #N_grade_1)
        // Grades(3, 9, #N_semester_5, #N_grade_2)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<>();

        // result atom 1
        ArrayList<Term> terms1 = new ArrayList<>();
        terms1.add(Constant.fromInteger("student_id", 2));
        terms1.add(Constant.fromInteger("module_id", 7));
        terms1.add(new Null("semester", 4));
        terms1.add(new Null("grade", 1));
        RelationalAtom atom1 = new RelationalAtom("Grades", terms1);

        expectedAtoms.add(atom1);

        // result atom 2
        ArrayList<Term> terms2 = new ArrayList<>();
        terms2.add(Constant.fromInteger("student_id", 3));
        terms2.add(Constant.fromInteger("module_id", 9));
        terms2.add(new Null("semester", 5));
        terms2.add(new Null("grade", 2));
        RelationalAtom atom2 = new RelationalAtom("Grades", terms2);

        expectedAtoms.add(atom2);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testMinimum() {
        Instance result = Commons.readInput("/example_min.xml");
        assertNotNull(result);

        // expected: Res(1)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<>();

        // result atom
        ArrayList<Term> terms = new ArrayList<>();
        terms.add(Constant.fromInteger("b", 1));
        RelationalAtom atom = new RelationalAtom("Res", terms);

        expectedAtoms.add(atom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testMaximum() {
        Instance result = Commons.readInput("/example_max.xml");
        assertNotNull(result);

        // expected: Res(12)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<>();

        // result atom
        ArrayList<Term> terms = new ArrayList<>();
        terms.add(Constant.fromInteger("b", 12));
        RelationalAtom atom = new RelationalAtom("Res", terms);

        expectedAtoms.add(atom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

}
