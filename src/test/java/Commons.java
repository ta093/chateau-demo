import java.io.File;
import java.net.URL;

import chase.Chase;
import instance.Instance;
import io.InputReader;
import io.SingleInput;

public class Commons {
    public static Instance readInput(String filepath) {
        InputReader reader = new InputReader();
        SingleInput input;

        // read example file
        URL filePath = Commons.class.getResource(filepath);
        File file = new File(filePath.getPath());

        try {
            input = reader.readFile(file);
        } catch (Exception e) {
            input = null;
            return null;
        }      

        // actual result
        Instance result = Chase.chase(input.getInstance(), input.getConstraints());
        return result;
    }
    public static Boolean checkInput(String filepath) {
        InputReader reader = new InputReader();
        SingleInput input;
        Instance instance;

        // read example file
        URL filePath = Commons.class.getResource(filepath);
        File file = new File(filePath.getPath());

        try {
            input = reader.readFile(file);
        } catch (Exception e) {
            input = null;
            return null;
        }

        instance = input.getInstance();

        var checkSuccess = instance.checkConstraints(input.getConstraints());

        return checkSuccess;
    }
}
