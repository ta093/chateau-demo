import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class WeakAcyclicityTest {

    @Test
    public void testSttgd() {
        // actual result
        Instance result = Commons.readInput("/example_6_WA.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_courses_1 = new ArrayList<Term>();
        terms_courses_1.add(Constant.fromString("course", "Electrical engineering"));
        terms_courses_1.add(Constant.fromString("institute", "IOF"));
        terms_courses_1.add(new Null("councilspokesman", 1));
        RelationalAtom courses_1 = new RelationalAtom("Courses", terms_courses_1);

        expectedAtoms.add(courses_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("course", "Electrical engineering"));
        terms_students_1.add(Constant.fromString("institute", "IOF"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_students_2 = new ArrayList<Term>();
        terms_students_2.add(new Null("councilspokesman", 1));
        terms_students_2.add(new Null("lastname", 1));
        terms_students_2.add(new Null("firstname", 1));
        terms_students_2.add(Constant.fromString("course", "Electrical engineering"));
        terms_students_2.add(Constant.fromString("institute", "IOF"));
        RelationalAtom students_2 = new RelationalAtom("Students", terms_students_2);

        expectedAtoms.add(students_2);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(Constant.fromInteger("module_id", 2));
        terms_participants_2.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

}
