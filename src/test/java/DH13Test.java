import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Term;
import term.Variable;
import term.VariableType;

public class DH13Test {

    @Test
    public void test() {
        // actual result
        Instance result = Commons.readInput("/example_DH13.xml");
        assertNotNull(result);

        // expected result
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_T = new ArrayList<Term>();
        terms_T.add(new Variable(VariableType.EXISTS, "z", 1));
        terms_T.add(new Variable(VariableType.EXISTS, "u", 1));
        RelationalAtom atom_T = new RelationalAtom("T", terms_T);

        expectedAtoms.add(atom_T);

        ArrayList<Term> terms_R = new ArrayList<Term>();
        terms_R.add(new Variable(VariableType.FOR_ALL, "x", 1));
        terms_R.add(new Variable(VariableType.EXISTS, "w", 1));
        terms_R.add(new Variable(VariableType.EXISTS, "y", 1));
        RelationalAtom atom_R = new RelationalAtom("R", terms_R);

        expectedAtoms.add(atom_R);

        ArrayList<Term> terms_S = new ArrayList<Term>();
        terms_S.add(new Variable(VariableType.EXISTS, "y", 1));
        terms_S.add(new Variable(VariableType.EXISTS, "z", 1));
        RelationalAtom atom_S = new RelationalAtom("S", terms_S);

        expectedAtoms.add(atom_S);
        
        ArrayList<Term> terms_VT = new ArrayList<Term>();
        terms_VT.add(new Variable(VariableType.EXISTS, "z", 1));
        terms_VT.add(new Variable(VariableType.EXISTS, "u", 1));
        RelationalAtom atom_VT = new RelationalAtom("VT", terms_VT);

        expectedAtoms.add(atom_VT);

        ArrayList<Term> terms_VR = new ArrayList<Term>();
        terms_VR.add(new Variable(VariableType.FOR_ALL, "x", 1));
        terms_VR.add(new Variable(VariableType.EXISTS, "y", 1));
        RelationalAtom atom_VR = new RelationalAtom("VR", terms_VR);

        expectedAtoms.add(atom_VR);

        ArrayList<Term> terms_VS = new ArrayList<Term>();
        terms_VS.add(new Variable(VariableType.EXISTS, "y", 1));
        terms_VS.add(new Variable(VariableType.EXISTS, "z", 1));
        RelationalAtom atom_VS = new RelationalAtom("VS", terms_VS);

        expectedAtoms.add(atom_VS);
        
        ArrayList<Term> terms_VRS = new ArrayList<Term>();
        terms_VRS.add(new Variable(VariableType.EXISTS, "x", 1));
        terms_VRS.add(new Variable(VariableType.EXISTS, "z", 1));
        RelationalAtom atom_VRS = new RelationalAtom("VRS", terms_VRS);

        expectedAtoms.add(atom_VRS);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
}
