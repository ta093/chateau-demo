import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import chase.Chase;
import constraints.Constraint;
import instance.Instance;
import io.InputReader;
import io.SingleInput;
import term.Constant;
import term.Null;
import term.Term;

public class EqualVariableTest {
    
    @Test
    public void testBodyTrue() {
        InputReader reader = new InputReader();
        SingleInput input;
        Instance instance;

        // read example file
        URL filePath = getClass().getResource("/example_equal_variable_true.xml");
        File file = new File(filePath.getPath());

        try {
            input = reader.readFile(file);
            instance = input.getInstance();

            LinkedHashSet<Constraint> constraints = new LinkedHashSet<>();
            for (Constraint b : input.getConstraints()) {
                constraints.add(b);
            }

            // actual result
            Instance result = Chase.chase(instance, constraints);
            
            // expected result:
            HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();
            
            ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
            terms_grades_1.add(Constant.fromInteger("module_id", 2));
            terms_grades_1.add(Constant.fromInteger("student_id", 3));
            terms_grades_1.add(new Null("semester", 1));
            terms_grades_1.add(new Null("grade", 1));
            RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);
            
            expectedAtoms.add(grades_1);                                 
            
            assertEquals(expectedAtoms, result.getRelationalAtoms());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testBodyFalse() {
        InputReader reader = new InputReader();
        SingleInput input;
        Instance instance;

        // read example file
        URL filePath = getClass().getResource("/example_equal_variable_false.xml");
        File file = new File(filePath.getPath());

        try {
            input = reader.readFile(file);
            instance = input.getInstance();

            LinkedHashSet<Constraint> constraints = new LinkedHashSet<>();
            for (Constraint b : input.getConstraints()) {
                constraints.add(b);
            }

            // actual result
            Instance result = Chase.chase(instance, constraints);

            // expected result:
            HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

            assertEquals(expectedAtoms, result.getRelationalAtoms());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testHead() {
        InputReader reader = new InputReader();
        SingleInput input;
        Instance instance;

        // read example file
        URL filePath = getClass().getResource("/example_equal_variable_head.xml");
        File file = new File(filePath.getPath());

        try {
            input = reader.readFile(file);
            instance = input.getInstance();

            LinkedHashSet<Constraint> constraints = new LinkedHashSet<>();
            for (Constraint b : input.getConstraints()) {
                constraints.add(b);
            }

            // actual result
            Instance result = Chase.chase(instance, constraints);

            // expected result:
            HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

            ArrayList<Term> terms_grades = new ArrayList<Term>();
            terms_grades.add(Constant.fromInteger("module_id", 2));
            terms_grades.add(Constant.fromInteger("student_id", 3));
            terms_grades.add(Constant.fromInteger("semester", 3));
            terms_grades.add(Constant.fromInteger("grade", 2));
            RelationalAtom grades = new RelationalAtom("Grades", terms_grades);

            expectedAtoms.add(grades);

            assertEquals(expectedAtoms, result.getRelationalAtoms());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
