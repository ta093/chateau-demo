import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class TgdTest {

    @Test
    public void testTgd1() {
        // actual result
        Instance result = Commons.readInput("/example_1_I_TGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(Constant.fromInteger("module_id", 7));
        terms_grades_1.add(Constant.fromInteger("student_id", 3));
        terms_grades_1.add(new Null("semester", 2));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(new Null("student_id", 1));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("course", "Electrical engineering"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_students_2 = new ArrayList<Term>();
        terms_students_2.add(Constant.fromInteger("student_id", 3));
        terms_students_2.add(Constant.fromString("lastname", "Miller"));
        terms_students_2.add(Constant.fromString("firstname", "Max"));
        terms_students_2.add(Constant.fromString("course", "Electrical engineering"));
        RelationalAtom students_2 = new RelationalAtom("Students", terms_students_2);

        expectedAtoms.add(students_2);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        terms_participants_1.add(new Null("semester", 1));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testTgd1b() {
        // actual result
        Instance result = Commons.readInput("/example_1b_I_TGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Null("module_id", 1));
        terms_grades_1.add(Constant.fromInteger("student_id", 3));
        terms_grades_1.add(new Null("semester", 1));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("coruse", "Electrical engineering"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(Constant.fromInteger("module_id", 2));
        terms_participants_2.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        ArrayList<Term> terms_participants_3 = new ArrayList<Term>();
        terms_participants_3.add(new Null("module_id", 1));
        terms_participants_3.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_3 = new RelationalAtom("Participants", terms_participants_3);

        expectedAtoms.add(participants_3);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testTgd1d() {
        // actual result
        Instance result = Commons.readInput("/example_1d_I_TGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Null("module_id", 1));
        terms_grades_1.add(Constant.fromInteger("student_id", 3));
        terms_grades_1.add(new Null("semester", 1));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("course", "Electrical engineering"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);

        expectedAtoms.add(students_1);

        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);

        expectedAtoms.add(participants_1);

        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(Constant.fromInteger("module_id", 2));
        terms_participants_2.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);

        expectedAtoms.add(participants_2);

        ArrayList<Term> terms_participants_3 = new ArrayList<Term>();
        terms_participants_3.add(new Null("module_id", 1));
        terms_participants_3.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_3 = new RelationalAtom("Participants", terms_participants_3);

        expectedAtoms.add(participants_3);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }

    @Test
    public void testTgd3() {
        // actual result
        Instance result = Commons.readInput("/example_3_I_TGD.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();
            
        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(Constant.fromInteger("module_id", 7));
        terms_grades_1.add(Constant.fromInteger("student_id", 3));
        terms_grades_1.add(new Null("semester", 1));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);
        
        expectedAtoms.add(grades_1);
        
        ArrayList<Term> terms_grades_2 = new ArrayList<Term>();
        terms_grades_2.add(Constant.fromInteger("module_id", 2));
        terms_grades_2.add(Constant.fromInteger("student_id", 3));
        terms_grades_2.add(new Null("semester", 2));
        terms_grades_2.add(new Null("grade", 2));
        RelationalAtom grades_2 = new RelationalAtom("Grades", terms_grades_2);
      
        expectedAtoms.add(grades_2);
        
        ArrayList<Term> terms_students_1 = new ArrayList<Term>();
        terms_students_1.add(Constant.fromInteger("student_id", 3));          
        terms_students_1.add(Constant.fromString("lastname", "Miller"));
        terms_students_1.add(Constant.fromString("firstname", "Max"));
        terms_students_1.add(Constant.fromString("course", "Electrical engineering"));
        RelationalAtom students_1 = new RelationalAtom("Students", terms_students_1);
        
        expectedAtoms.add(students_1);
        
        ArrayList<Term> terms_students_2 = new ArrayList<Term>();
        terms_students_2.add(Constant.fromInteger("student_id", 1));          
        terms_students_2.add(Constant.fromString("lastname", "May"));
        terms_students_2.add(Constant.fromString("firstname", "Maria"));
        terms_students_2.add(Constant.fromString("course", "Electrical engineering"));
        RelationalAtom students_2 = new RelationalAtom("Students", terms_students_2);
        
        expectedAtoms.add(students_2);
        
        ArrayList<Term> terms_participants_1 = new ArrayList<Term>();
        terms_participants_1.add(Constant.fromInteger("module_id", 7));
        terms_participants_1.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_1 = new RelationalAtom("Participants", terms_participants_1);
        
        expectedAtoms.add(participants_1);
        
        ArrayList<Term> terms_participants_2 = new ArrayList<Term>();
        terms_participants_2.add(Constant.fromInteger("module_id", 2));
        terms_participants_2.add(Constant.fromInteger("student_id", 3));
        RelationalAtom participants_2 = new RelationalAtom("Participants", terms_participants_2);
        
        expectedAtoms.add(participants_2);
        
        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }      
}
