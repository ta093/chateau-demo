import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import atom.RelationalAtom;
import instance.Instance;
import term.Constant;
import term.Null;
import term.Term;

public class ConstantsTest {

    @Test
    public void testConstants() {
        // actual result
        Instance result = Commons.readInput("/example_constants.xml");
        assertNotNull(result);

        // expected result:
        // single RelationalAtom: Result(Sonnenschein)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        // Result(Sonnenschein) atom
        ArrayList<Term> terms = new ArrayList<Term>();
        terms.add(Constant.fromString("lastname", "Sonnenschein"));
        RelationalAtom atom = new RelationalAtom("Result", terms);

        expectedAtoms.add(atom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testHeadConstants() {
        // actual result
        Instance result = Commons.readInput("/example_head_constants.xml");
        assertNotNull(result);

        // expected result:
        // single RelationalAtom: Result(Sonnenschein)
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        // Result(Sonnenschein) atom
        ArrayList<Term> terms = new ArrayList<Term>();
        terms.add(Constant.fromString("lastname", "Test-Mathematics"));
        RelationalAtom atom = new RelationalAtom("Result", terms);

        expectedAtoms.add(atom);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testConstantSelection() {
        // actual result
        Instance result = Commons.readInput("/example_constants_selection.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(new Null("module_id", 1));
        terms_grades_1.add(Constant.fromInteger("student_id", 1));
        terms_grades_1.add(new Null("semester", 1));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
    
    @Test
    public void testConstantSelectionJoin() {
        // actual result
        Instance result = Commons.readInput("/example_constants_selection_join.xml");
        assertNotNull(result);

        // expected result:
        HashSet<RelationalAtom> expectedAtoms = new HashSet<RelationalAtom>();

        ArrayList<Term> terms_grades_1 = new ArrayList<Term>();
        terms_grades_1.add(Constant.fromInteger("module_id", 2));
        terms_grades_1.add(Constant.fromInteger("student_id", 1));
        terms_grades_1.add(new Null("semester", 3));
        terms_grades_1.add(new Null("grade", 1));
        RelationalAtom grades_1 = new RelationalAtom("Grades", terms_grades_1);

        expectedAtoms.add(grades_1);

        assertEquals(expectedAtoms, result.getRelationalAtoms());
    }
}
