package atom;

import java.util.LinkedHashSet;

import homomorphism.TermMapping;
import term.Term;

/**
 * The class defines equality atoms and consists of two {@link term.Term}
 * objects where an equality is to be expressed.
 *
 * @author Martin Jurklies
 */
public class EqualityAtom extends Atom {
	private Term term1;
	private Term term2;

	/**
	 * constructor
	 *
	 * @param term1 the first term
	 * @param term2 the second term
	 */
	public EqualityAtom(Term term1, Term term2) {
		this.term1 = term1;
		this.term2 = term2;
	}

	/**
	 * constructor
	 *
	 * @param atom
	 */
	public EqualityAtom(EqualityAtom atom) {
		this.term1 = atom.term1;
		this.term2 = atom.term2;
	}

	/**
	 * @return the first term
	 */
	public Term getTerm1() {
		return this.term1;
	}

	/**
	 * @param t1 the first term to set
	 */
	public void setTerm1(Term t1) {
		this.term1 = t1;
	}

	/**
	 * @return the second term
	 */
	public Term getTerm2() {
		return this.term2;
	}

	/**
	 * @param t2 the second term to set
	 */
	public void setTerm2(Term t2) {
		this.term2 = t2;
	}

	/**
	 * The method applies some given {@code mappings} to both terms of this object
	 * and checks if these mapped terms are equal.
	 *
	 * @param mappings the mapping to apply to the terms
	 * @return {@code true} if both mapped terms are equal, {@code false} otherwise
	 */
	public boolean fulfillsEqualityWithMappings(LinkedHashSet<TermMapping> mappings) {
		Term tempTerm1 = term1;
		Term tempTerm2 = term2;

		for (TermMapping mapping : mappings) {
			tempTerm1 = mapping.apply(tempTerm1);
			tempTerm2 = mapping.apply(tempTerm2);
		}

		return tempTerm1.equals(tempTerm2);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.term1.toString() + " = " + this.term2.toString();
	}

	@Override
	public EqualityAtom copy() {
		return new EqualityAtom(this.term1.copy(),this.term2.copy());
	}
}
