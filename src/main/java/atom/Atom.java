package atom;

/**
 * The class defines atoms that are used in {@link instance.Instance},
 * {@link integrityConstraint.integrityConstraint} and {@link instance.Query}.
 * The class is abstract is is only used for inheritance of the classes
 * {@link atom.RelationalAtom} and {@link atom.EqualityAtom}.
 *
 * @author Martin Jurklies
 */
public abstract class Atom {

	public abstract Atom copy();
}
