package atom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import functions.Function;
import term.Term;
import term.Variable;
import termination.Adornment;

/**
 * The class defines relational atoms and consists of a relation name and a list
 * of {@link term.Term} objects.
 *
 * @author Martin Jurklies
 */
public class RelationalAtom extends Atom implements Comparable<RelationalAtom> {
	private String relationName;
	private ArrayList<Term> terms;
	private ArrayList<Adornment> adornments;
	private boolean isNegated;
	private boolean isOrigin;
	private ProvenanceInformation provInfo;
	private ArrayList<Function<?>> functions;

	/**
	 * empty constructor
	 */
	public RelationalAtom() {
		this(null, null, false, false, null);
	}

	/**
	 * copy constructor
	 *
	 * @param atom the relational atom to copy
	 */
	public RelationalAtom(RelationalAtom atom) {
		this(atom.getName(), atom.terms, atom.isNegated, atom.isOrigin, atom.getProvInfo().copy());
	}

	/**
	 * constructor
	 */
	public RelationalAtom(String name) {
		this(name, null);
	}

	/**
	 * constructor
	 */
	public RelationalAtom(String relationName, ArrayList<Term> terms) {
		this(relationName, terms, false);
	}

	/**
	 * constructor
	 */
	public RelationalAtom(String relationName, ArrayList<Term> terms, boolean negated) {
		this(relationName, terms, negated, false, null);
	}

	/**
	 * constructor
	 */
	public RelationalAtom(String relationName, ArrayList<Term> terms, boolean negated, boolean isOrigin,
			ProvenanceInformation provInfo) {
		this.relationName = relationName != null ? relationName : "";
		this.terms = terms != null ? new ArrayList<Term>(terms) : new ArrayList<Term>();
		this.isNegated = negated;
		this.isOrigin = isOrigin;
		this.provInfo = provInfo != null ? provInfo : new ProvenanceInformation(generateProvenanceId(relationName));
	}

	/**
	 * @return the relation name
	 */
	public String getName() {
		return this.getRelationName();
	}

	/**
	 * @return the relation name
	 */
	public String getRelationName() {
		return relationName;
	}

	/**
	 * @param relationName the relation name to set
	 */
	public void setName(String relationName) {
		this.relationName = relationName;
	}

	/**
	 * @return the list of terms
	 */
	public ArrayList<Term> getTerms() {
		return this.terms;
	}

	/**
	 * @param terms the term list to set
	 */
	public void setTerms(ArrayList<Term> terms) {
		this.terms = terms;
	}

	/**
	 * @return the list of adornments (corresponding to the list of terms)
	 */
	public ArrayList<Adornment> getAdornments() {
		return adornments;
	}

	/**
	 * @param adornments the adornment list to set
	 */
	public void setAdornments(ArrayList<Adornment> adornments) {
		this.adornments = adornments;
	}

	/***
	 *
	 * @return boolean isNegated
	 */
	public boolean isNegated() {
		return isNegated;
	}

	/**
	 *
	 * @return boolean isOrigin
	 */
	public boolean isOrigin() {
		return isOrigin;
	}

	/**
	 *
	 * @param isOrigin
	 */
	public void setOrigin(boolean isOrigin) {
		this.isOrigin = isOrigin;
	}

	/**
	 *
	 * @return ProvenanceInformation
	 */
	public ProvenanceInformation getProvInfo() {
		return provInfo;
	}

	/**
	 *
	 * @param ProvenanceInformation provInfo
	 */
	public void setProvInfo(ProvenanceInformation provInfo) {
		this.provInfo = provInfo;
	}

	public void setFunctions(ArrayList<Function<?>> functions) {
		this.functions = functions;
	}

	/**
	 * Returns the functions connected to an atom.
	 * 
	 * @return A list of functions
	 */
	public ArrayList<Function<?>> getFunctions() {
		return this.functions;
	}

	/**
	 * Returns a single function for a specific target variable.
	 * 
	 * @param variableName The target variable (aka the variable the function shall
	 *                     be applied on)
	 * @return The function for that specific variable
	 */
	public Function<?> getFunctionForVariable(Variable variable) {
		for (Function<?> function : functions) {
			if (function.getVariable().equals(variable)) {
				return function;
			}
		}

		return null;
	}

	public HashSet<Variable> getFunctionTargets() {
		HashSet<Variable> targets = new HashSet<>();

		if (functions != null) {
			for (Function<?> f : functions) {
				targets.add(f.getVariable());
			}
		}

		return targets;
	}

	/**
	 * each Adornment in the adornments-list will be transfered to the corresponding
	 * term in the term-list
	 */
	public void transferAdornments() {
		if ((this.adornments == null) || (this.terms == null)) {
			return;
		}

		for (int i = 0; i < this.terms.size(); ++i) {
			if (i < this.adornments.size()) {
				this.getTerms().get(i).setAdornment(this.adornments.get(i));
			}
		}
	}

	/**
	 * the adornments-list will be recreated with Adornments from terms of the
	 * term-list
	 */
	public void receiveAdornments() {
		if (this.terms == null) {
			return;
		}

		this.setAdornments(new ArrayList<Adornment>());
		for (int i = 0; i < this.terms.size(); ++i) {
			this.getAdornments().add(i, this.getTerms().get(i).getAdornment());
		}
	}

	/**
	 * @return if Adornments from adornment-list are identical with Adornments from
	 *         terms of the term-list
	 */
	public boolean isConsistent() {
		// something is wrong, but the problem is not consistency
		if ((this.adornments == null) || (this.terms == null)) {
			return true;
		}

		for (int i = 0; i < this.terms.size(); ++i) {
			if (i < this.adornments.size()) {
				if (!this.getTerms().get(i).getAdornment().equals(this.adornments.get(i))) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Checks if there exists a homomorphism from this object to another relational
	 * atom.
	 *
	 * @param other the other relational atom
	 * @return {@code true} if there is a homomorphism from this object to the
	 *         other, {@code false} otherwise
	 */
	public boolean hasHomomorphismTo(RelationalAtom other) {
		ArrayList<Term> thisTerms = this.terms;
		ArrayList<Term> otherTerms = other.terms;

		if (this.getRelationName().equals(other.getRelationName()) && thisTerms.size() == otherTerms.size()) {
			for (int i = 0; i < thisTerms.size(); i++) {
				if (thisTerms.get(i).hasHomomorphismTo(otherTerms.get(i))) {
					continue;
				} else {
					return false;
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * !must still be replaced with the generation method from PROSA!
	 * 
	 * @return new provenance id
	 */
	public String generateProvenanceId(String relationName) {
		Random rand = new Random(System.currentTimeMillis());
		int randomValue = Math.abs(rand.nextInt());
		return relationName.concat(Integer.toString(hashCode() * randomValue));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		if (this.isNegated()) {
			builder.append("!-");
		}

		builder.append(this.getRelationName());
		builder.append("(");

		ArrayList<Term> terms = this.terms;
		int termSize = terms.size();
		String sepTerm = "";

		for (int i = 0; i < termSize; i++) {
			builder.append(sepTerm);
			sepTerm = ", ";
			builder.append(terms.get(i).toString());
		}

		builder.append(")");
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getRelationName() == null) ? 0 : getRelationName().hashCode());
		result = prime * result + ((terms == null) ? 0 : terms.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RelationalAtom)) {
			return false;
		}

		RelationalAtom other = (RelationalAtom) obj;

		if (getRelationName() == null) {
			if (other.getRelationName() != null) {
				return false;
			}

		}

		else if (!getRelationName().equals(other.getRelationName())) {
			return false;
		}

		if (terms == null) {
			if (other.terms != null) {
				return false;
			}
		}

		else if (!terms.equals(other.terms)) {
			return false;
		}

		if (isOrigin != other.isOrigin) {
			return false;
		}

		return true;
	}

	/**
	 * Compares to RelationalAtoms by its relation name.
	 *
	 * @param atom The other atom.
	 * @return 1 if {@code atom} is "greater" than {@code this}, 0 if {@code atom}
	 *         equals {@code this}, -1 if {@code atom} is "smaller" than
	 *         {@code this}
	 */
	@Override
	public int compareTo(RelationalAtom atom) {
		return this.getName().compareTo(atom.getName());
	}

	@Override
	public RelationalAtom copy() {
		var termsCopy = new ArrayList<Term>();
		this.terms.forEach(term -> termsCopy.add(term.copy()));
		return new RelationalAtom(relationName, termsCopy, isNegated, isOrigin, provInfo);
	}
}
