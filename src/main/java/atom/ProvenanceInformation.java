package atom;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class ProvenanceInformation {
	private String id;
	private LinkedHashSet<String> whereTupleProvenance;
	private LinkedHashSet<String> whereRelationsProvenance;
	private ArrayList<ArrayList<String>> whyProvenance;

	public ProvenanceInformation(String id) {
		this.id = id;
		this.whyProvenance = new ArrayList<ArrayList<String>>();
	}

	public ProvenanceInformation(String id, LinkedHashSet<String> whereTupleProvenance,
			LinkedHashSet<String> whereRelationsProvenance, ArrayList<ArrayList<String>> whyProvenance) {
		this.id = id;
		this.whereTupleProvenance = whereTupleProvenance;
		this.whereRelationsProvenance = whereRelationsProvenance;
		this.whyProvenance = whyProvenance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LinkedHashSet<String> getWhereTupleProvenance() {
		return whereTupleProvenance;
	}

	public void setWhereTupleProvenance(LinkedHashSet<String> whereTupleProvenance) {
		this.whereTupleProvenance = whereTupleProvenance;
	}

	public LinkedHashSet<String> getWhereRelationsProvenance() {
		return whereRelationsProvenance;
	}

	public void setWhereRelationsProvenance(LinkedHashSet<String> whereRelationsProvenance) {
		this.whereRelationsProvenance = whereRelationsProvenance;
	}

	public ArrayList<ArrayList<String>> getWhyProvenance() {
		return whyProvenance;
	}

	public void setWhyProvenance(ArrayList<ArrayList<String>> whyProvenance) {
		this.whyProvenance = whyProvenance;
	}

	public ProvenanceInformation copy() {
		return new ProvenanceInformation(id, whereTupleProvenance, whereRelationsProvenance, whyProvenance);
	}

	public String getPolynom() {
		StringBuilder polynom = new StringBuilder();
		for (int i = 0; i < whyProvenance.size(); i++) {
			var witnesses = new ArrayList<>(whyProvenance.get(i));
			if (i != 0) {
				polynom.append("+");
			}

			StringBuilder innerPolynom = new StringBuilder();
			for (int j = 0; j < witnesses.size(); j++) {
				if (j == 0) {
					innerPolynom.append("(");
				} else {
					innerPolynom.append("*");
				}

				innerPolynom.append(witnesses.get(j).toString());

				if (j == witnesses.size() - 1) {
					innerPolynom.append(")");
				}
			}

			polynom.append(innerPolynom.toString());
		}

		return polynom.toString();
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();

		// id
		output.append("ID: ").append(id).append(System.lineSeparator());

		// where
		output.append("Where: ").append(String.join(", ", whereTupleProvenance));
		output.append(" (");
		output.append(String.join(", ", whereRelationsProvenance));
		output.append(")").append(System.lineSeparator());

		// why
		ArrayList<String> witnessSetStrings = new ArrayList<String>();
		for (ArrayList<String> tupleList : whyProvenance) {
			String tupleListString = "{" + String.join(", ", tupleList) + "}";
			witnessSetStrings.add(tupleListString);
		}

		output.append("Why: ").append("{");
		output.append(String.join(", ", witnessSetStrings));
		output.append("}").append(System.lineSeparator());

		// how
		output.append("How: ").append(getPolynom()).append(System.lineSeparator());

		return output.toString();
	}
}