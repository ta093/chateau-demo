package homomorphism;

import atom.RelationalAtom;
import instance.Instance;
import javafx.util.Pair;
import term.Term;

/**
 * The class defines a term mapping which replaces one term with another. It
 * consist of two {@link term.Term} objects where this replacement relation is
 * expressed.
 *
 * @author Martin Jurklies
 */
public class TermMapping {
	private Term source;
	private Term target;

	// gives the possibility to replace single constants from new tuples
	// with other constants from original tuples with bEgds
	// maps the hash code of the atom to the position of the term in the atom
	private Pair<RelationalAtom, Integer> sourceTermRelationPosition;
	private Pair<RelationalAtom, Integer> targetTermRelationPosition;

	/**
	 * constructor
	 */
	public TermMapping(Term source, Term target) {
		this.source = source;
		this.target = target;
	}

	/**
	 * @return the mapping source
	 */
	public Term getSource() {
		return this.source;
	}

	/**
	 * @param source the mapping source to set
	 */
	public void setMappingSource(Term source) {
		this.source = source;
	}

	/**
	 * @return the mapping target
	 */
	public Term getTarget() {
		return this.target;
	}

	/**
	 * @param target the mapping target to set
	 */
	public void setMappingTarget(Term target) {
		this.target = target;
	}

	/**
	 *
	 * @return sourceTermRelationPosition
	 */
	public Pair<RelationalAtom, Integer> getSourceTermRelationPosition() {
		return sourceTermRelationPosition;
	}

	/**
	 *
	 * @param sourceTermRelationPosition
	 */
	public void setSourceTermRelationPosition(Pair<RelationalAtom, Integer> sourceTermRelationPosition) {
		this.sourceTermRelationPosition = sourceTermRelationPosition;
	}

	/**
	 *
	 * @return targetTermRelationPosition
	 */
	public Pair<RelationalAtom, Integer> getTargetTermRelationPosition() {
		return targetTermRelationPosition;
	}

	/**
	 *
	 * @param targetTermRelationPosition
	 */
	public void setTargetTermRelationPosition(Pair<RelationalAtom, Integer> targetTermRelationPosition) {
		this.targetTermRelationPosition = targetTermRelationPosition;
	}

	/**
	 * Applies the mapping to a term.
	 *
	 * @param term the term to that the mapping has to be applied
	 * @return {@code target} if the mapping has been applied, {@code source}
	 *         otherwise
	 */
	public Term apply(Term term) {
		// current term is Constant
		if (term.isConstant()) {
			return term;
		}

		// current term and mapping source are Nulls
		else if (term.isNull() && this.source.isNull()) {
			// current term and mapping source are the same Null
			if (term.equals(source)) {
				return target;
			} else {
				return term;
			}
		}

		// current term and mapping source are Variables
		else if (term.isVariable() && this.source.isVariable()) {
			// current term and mapping source are the same Variable
			if (term.equals(source)) {
				return target;
			} else {
				return term;
			}
		}

		// current term and mapping source are different
		// mapping is not applied
		return term;
	}

	/**
	 * Applies the mapping to a relational atom.
	 *
	 * @param atom the atom to that the mapping has to be applied
	 * @return a relational atom where all terms specified by the mapping are
	 *         replaced
	 */
	public RelationalAtom apply(RelationalAtom atom) {
		RelationalAtom mappedAtom = new RelationalAtom(
				atom.getName(),
				null,
				atom.isNegated(),
				atom.isOrigin(),
				atom.getProvInfo());

		for (Term term : atom.getTerms()) {
			Term mappedTerm = this.apply(term);
			mappedAtom.getTerms().add(mappedTerm);
		}

		return mappedAtom;
	}

	/**
	 * Applies the mapping to an instance.
	 *
	 * @param instance the instance to that the mapping has to be applied
	 * @return an instance where all terms specified by the mapping are replaced
	 */
	public Instance apply(Instance instance) {
		Instance mappedInstance = new Instance(null, instance.getSchema(), instance.getOriginTag(),
				instance.getSchemaTags(), instance.getHeadAtoms());

		for (RelationalAtom atom : instance.getRelationalAtoms()) {
			RelationalAtom mappedAtom = this.apply(atom);
			mappedInstance.addRelationalAtom(mappedAtom);
		}

		return mappedInstance;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(source.toString());
		builder.append(" -> ");
		builder.append(target.toString());

		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());

		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TermMapping)) {
			return false;
		}

		TermMapping other = (TermMapping) obj;

		if (source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!source.equals(other.source)) {
			return false;
		}

		if (target == null) {
			if (other.target != null) {
				return false;
			}
		}

		else if (!target.equals(other.target)) {
			return false;
		}

		return true;
	}
}
