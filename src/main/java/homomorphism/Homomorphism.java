package homomorphism;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import atom.EqualityAtom;
import atom.RelationalAtom;
import comparisons.Comparison;
import comparisons.ComparisonType;
import constraints.Constraint;
import constraints.Tgd;
import functions.Function;
import instance.Instance;
import javafx.util.Pair;
import term.Constant;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * The class represents a homomorphism and consists of a set of
 * {@link homomorphism.TermMapping} objects.
 *
 * @author Martin Jurklies
 */
public class Homomorphism {
	private LinkedHashSet<TermMapping> mappings;

	/**
	 * empty constructor
	 */
	public Homomorphism() {
		this((LinkedHashSet<TermMapping>) null);
	}

	/**
	 * copy constructor
	 *
	 * @param homomorphism the homomorphism to copy
	 */
	public Homomorphism(Homomorphism homomorphism) {
		this(homomorphism.mappings);
	}

	/**
	 * constructor
	 *
	 * @param mappings the mappings
	 */
	public Homomorphism(LinkedHashSet<TermMapping> mappings) {
		if (mappings == null) {
			mappings = new LinkedHashSet<TermMapping>();
		}

		this.mappings = new LinkedHashSet<TermMapping>(mappings);
	}

	/**
	 * @return the term mappings
	 */
	public LinkedHashSet<TermMapping> getTermMappings() {
		return mappings;
	}

	/**
	 * @param mappings the term mappings to set
	 */
	public void setTermMappings(LinkedHashSet<TermMapping> mappings) {
		this.mappings = mappings;
	}

	/**
	 * Adds a new mapping to the set of mappings of this object.
	 *
	 * @param newMapping the new mapping to add
	 * @return {@code true} if the new mapping has been added,
	 *         {@code false} if two different mappings with the same source terms
	 *         and different target terms are to be applied
	 */
	public boolean addMapping(TermMapping newMapping) {
		// look for already existing mappings that have the current sourceTerm of the
		// sourceAtom as the mapping source
		for (TermMapping mapping : this.mappings) {
			// there can't be two different mappings with the same source term and different
			// targets
			if (newMapping.getSource().equals(mapping.getSource())
					&& !newMapping.getTarget().equals(mapping.getTarget())) {
				return false;
			}
		}

		this.getTermMappings().add(newMapping);
		return true;
	}

	/**
	 * Generates new mappings for two given relational atoms. If mappings are
	 * generated, these will be added to the set of mapping of this object
	 * immediately.
	 *
	 * @param sourceAtom the source atom of the potentially new mappings
	 * @param targetAtom the target atom of the potentially new mappings
	 */
	public boolean generateMappingsFor(RelationalAtom sourceAtom, RelationalAtom targetAtom) {
		ArrayList<Term> sourceTerms = sourceAtom.getTerms();
		ArrayList<Term> targetTerms = targetAtom.getTerms();

		// test if the two atoms have the same relation name and same size
		if (sourceAtom.getName().equals(targetAtom.getName()) && sourceTerms.size() == targetTerms.size()) {
			for (int i = 0; i < sourceTerms.size(); i++) {
				TermMapping newMapping = new TermMapping(sourceTerms.get(i), targetTerms.get(i));

				// give the possibility to replace single constants from new tuples
				// with other constants from original tuples with bEgds
				newMapping.setSourceTermRelationPosition(new Pair<>(sourceAtom, i));
				newMapping.setTargetTermRelationPosition(new Pair<>(targetAtom, i));

				if (!this.addMapping(newMapping)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Applies the mappings of this object to a given relational atom.
	 * New version where a homomorphism can no longer overwrite its own changes.
	 *
	 * @param atom the relational atom to that the mappings have to be applied
	 * @return the mapped atom
	 */
	public RelationalAtom applyMappingsTo(RelationalAtom atom) {
		RelationalAtom mappedAtom = new RelationalAtom(atom);

		// 2 temporary atoms to be able to remove the terms inside foreach and not
		// change the original atom
		RelationalAtom tempAtomOuter = new RelationalAtom(atom);
		RelationalAtom tempAtomInner = new RelationalAtom(atom);

		int termIndex;

		for (TermMapping mapping : this.mappings) {
			// after foreach loop, remove mapped terms from tempAtomOuter
			tempAtomOuter.getTerms().clear();
			tempAtomOuter.getTerms().addAll(0, tempAtomInner.getTerms());

			for (Term term : tempAtomOuter.getTerms()) {
				termIndex = mappedAtom.getTerms().indexOf(term);
				Term mappedTerm = mapping.apply(term).copy();

				if (!mappedTerm.equals(term)) {
					// replace mapped term in mappedAtoms
					mappedAtom.getTerms().remove(termIndex);
					mappedAtom.getTerms().add(termIndex, mappedTerm);

					// remove mapped term from tempAtomInner
					tempAtomInner.getTerms().remove(term);
				}
			}
		}

		return mappedAtom;
	}

	/**
	 * Applies the mappings of this object to a given instance.
	 *
	 * @param instance the instance to that the mappings have to be applied
	 * @return the mapped instance
	 */
	public Instance applyMappingsTo(Instance instance) {
		Instance mappedInstance = new Instance(instance);

		for (TermMapping mapping : this.mappings) {
			mappedInstance = mapping.apply(mappedInstance);
		}

		return mappedInstance;
	}

	/**
	 * Composes the mappings of this object with the mappings of another
	 * homomorphism.
	 *
	 * @param other the other homomorphism
	 * @return a homomorphism with cumulative mappings of the two previous
	 *         homomorphisms
	 */
	public Homomorphism composeWith(Homomorphism other) {
		Homomorphism composedH = new Homomorphism(this);

		for (TermMapping thisMapping : this.mappings) {
			for (TermMapping otherMapping : other.mappings) {
				if (thisMapping.getTarget().equals(otherMapping.getSource())) {
					TermMapping composedMapping = new TermMapping(thisMapping.getSource(),
							otherMapping.getTarget());

					composedH.mappings.remove(thisMapping);

					// give the possibility to replace single constants from new tuples
					// with other constants from original tuples with bEgds
					composedMapping.setSourceTermRelationPosition(thisMapping.getSourceTermRelationPosition());
					composedMapping.setTargetTermRelationPosition(otherMapping.getTargetTermRelationPosition());

					composedH.addMapping(composedMapping);
					composedH.addMapping(otherMapping);
				}
			}
		}

		return composedH;
	}

	/**
	 * Checks if this object is a trigger for a given instance and a given integrity
	 * constraint.
	 *
	 * @param instance            the instance for that has to be checked if this
	 *                            homomorphism is a trigger
	 * @param integrityConstraint the constraint for that has to be checked if this
	 *                            homomorphism is a trigger
	 * @return {@code true} if this object is a trigger for the instance and
	 *         constraint, {@code false} otherwise
	 */
	public boolean isTriggerFor(Instance instance, Constraint integrityConstraint) {
		boolean isTrigger = true;

		ConstraintAtomsLoop: for (RelationalAtom constraintAtom : integrityConstraint.getBody()) {
			boolean hasPartialTrigger = false;

			InstanceAtomsLoop: for (RelationalAtom instanceAtom : instance.getRelationalAtoms()) {
				// hasHomomorphismTo test only when applyMappingsTo maps constraintAtom
				// otherwise the variables from the Constraints are also checked
				// and e.g. from a Constraints existential variable is not mapped to a instance
				// null
				boolean hasHomomorphism = false;

				if (constraintAtom.getRelationName().equals(instanceAtom.getRelationName())
						&& constraintAtom.getTerms().size() == instanceAtom.getTerms().size()) {
					hasHomomorphism = true;

					TermMappingsLoop: for (TermMapping termMapping : this.getTermMappings()) {
						TermMappingAtomsLoop: for (int i = 0; i < constraintAtom.getTerms().size(); i++) {
							// "unpack" terms
							Term constraintTerm = constraintAtom.getTerms().get(i);
							Term instanceTerm = instanceAtom.getTerms().get(i);

							// compare source
							if (termMapping.getSource().equals(constraintTerm)) {
								hasHomomorphism = termMapping.apply(constraintTerm).hasHomomorphismTo(instanceTerm);

								if (!hasHomomorphism) {
									break TermMappingAtomsLoop;
								}
							}
						}

						if (!hasHomomorphism) {
							break TermMappingsLoop;
						}
					}
				}

				hasPartialTrigger = hasHomomorphism || hasPartialTrigger;

				if (hasPartialTrigger) {
					break InstanceAtomsLoop;
				}
			}

			// toggle hasPartialTrigger if atom is negated
			if (constraintAtom.isNegated()) {
				hasPartialTrigger = !hasPartialTrigger;
			}

			isTrigger = isTrigger && hasPartialTrigger;

			if (!isTrigger) {
				break ConstraintAtomsLoop;
			}
		}

		return isTrigger;
	}

	/**
	 * Checks if this object is an active trigger for a given instance and a given
	 * integrity constraint.
	 *
	 * @param instance   the instance for that has to be checked if this
	 *                   homomorphism is an active trigger
	 * @param constraint the constraint for that has to be checked if this
	 *                   homomorphism is an active trigger
	 * @return {@code true} if this object is an active trigger for the instance and
	 *         constraint, {@code false} otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean isActiveTriggerFor(Instance instance, Constraint constraint) {
		// constraint is tgd
		if (constraint instanceof Tgd) {
			boolean instanceSatisfiesHead = true;

			for (RelationalAtom constraintHeadAtom : (LinkedHashSet<RelationalAtom>) constraint.getHead()) {
				RelationalAtom mappedAtom = applyMappingsTo(constraintHeadAtom);

				mappedAtom = applyComparisons((Tgd) constraint, mappedAtom);

				instanceSatisfiesHead = isContained(instance, mappedAtom);

				if (!instanceSatisfiesHead) {
					break;
				}
			}

			// true = no new facts would be generated; false = active Trigger
			return !instanceSatisfiesHead;
		}

		// constraint is egd
		else {
			boolean mappingSatisfiesHead = true;

			for (EqualityAtom constraintAtom : (LinkedHashSet<EqualityAtom>) constraint.getHead()) {
				mappingSatisfiesHead = constraintAtom.fulfillsEqualityWithMappings(mappings);

				if (!mappingSatisfiesHead) {
					break;
				}
			}
			// true = no equality was violated; false = active Trigger
			return !mappingSatisfiesHead;
		}
	}

	@SuppressWarnings("unchecked")
	public RelationalAtom applyComparisons(Tgd tgd, RelationalAtom newAtom) {
		LinkedHashSet<Comparison<Function<?>>> comparisons = new LinkedHashSet<>();

		// filter function comparisons and determine all affected variables
		LinkedHashSet<Variable> variablesToReplace = new LinkedHashSet<>();

		if (tgd.getHeadComparisons() != null) {
			for (Comparison<?> comparison : tgd.getHeadComparisons()) {
				if (comparison.getType() == ComparisonType.FUNCTION) {
					comparisons.add((Comparison<Function<?>>) comparison);
					variablesToReplace.add(comparison.getLeftSide());
				}
			}
		}

		// check if atom contains any comparison variables at all
		for (Variable variable : variablesToReplace) {
			if (newAtom.getTerms().contains(variable)) {
				break;
			}

			return newAtom;
		}

		for (Comparison<Function<?>> comparison : comparisons) {
			// replace each variable with the function result
			for (Variable variable : variablesToReplace) {
				if (comparison.getLeftSide().equals(variable)) {
					Constant result = comparison.getRightSide().toConstant(mappings);

					// replace variable with result constant
					int index = newAtom.getTerms().indexOf(variable);
					newAtom.getTerms().set(index, result);

					break;
				}

			}
		}

		return newAtom;
	}

	/**
	 * Like HashSet.contains() but without inclusion of #E and null values
	 * checks if the instance already contains all facts that the tgd head would
	 * generate,
	 * or contains facts that can be mapped to the new facts (according to
	 * homomorphism rules)
	 * E => E | V | Constant | Null
	 * V => Constant
	 *
	 * @return
	 */
	private boolean isContained(Instance instance, RelationalAtom headAtom) {
		boolean isContained = false;

		for (RelationalAtom instanceAtom : instance.getRelationalAtomsBySchema(headAtom.getName())) {
			for (int i = headAtom.getTerms().size() - 1; i >= 0; i--) {
				isContained = true;
				Term headAtomTerm = headAtom.getTerms().get(i);

				if (!instanceAtom.getTerms().get(i).equals(headAtomTerm)) {
					// move on if head is a variable
					if (headAtomTerm.isVariable()) {
						Variable headAtomVariable = (Variable) headAtomTerm;
						VariableType variableType = headAtomVariable.getVariableType();

						if (variableType == VariableType.EXISTS) {
							// variable is existence quantified and can be mapped to any instance term
							continue;
						} else if (variableType == VariableType.FOR_ALL) {
							if (instanceAtom.getTerms().get(i).isConstant()) {
								// variable is all-quantified and can be mapped to constants
								continue;
							}
						}
					}

					isContained = false;
					break;
				}
			}

			if (isContained) {
				return true;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		String sepMapping = "";

		builder.append("{");

		for (TermMapping mapping : this.mappings) {
			builder.append(sepMapping);
			builder.append(mapping.toString());
			sepMapping = ", ";
		}

		builder.append("}");

		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mappings == null) ? 0 : mappings.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Homomorphism)) {
			return false;
		}

		Homomorphism other = (Homomorphism) obj;

		if (mappings == null && other.mappings != null) {
			return false;
		} else if (!mappings.equals(other.mappings)) {
			return false;
		}

		return true;
	}
}
