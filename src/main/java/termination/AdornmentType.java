package termination;

/**
 * Adornments can either be free (f-adornments) or bounded (b-adornments). While
 * existentially quantified variables always carry f-Adornments, constants
 * always carry b-adornments.
 * 
 * @author Andreas Görres
 */
public enum AdornmentType {
	
	FREE("f", "free"),
	
	BOUNDED("b", "bounded");

	private final String token;
	
	private final String typeName;

	private AdornmentType(String token, String type) {
		this.token = token;
		this.typeName = type;
	}

	@Override
	public String toString() {
		return token;
	}

	public String getToken() {
		return token;
	}

	public String getTypeName() {
		return typeName;
	}
}
