package termination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Egd;
import constraints.Constraint;
import constraints.Tgd;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * Most of the powerful termination tests (e.g. constraint-rewriting) work on
 * TGDs only. If you want to test a set of integrity constraints containing both
 * TGDs and EGDs, you can use the Substitution-free-Simulation (SfS) Algorithm
 * to transforms the heterogeneous set of constraints into a set of TGDs with
 * very similar behavior. To start this EGD-rewriting, call
 * {@code SfS.doSfS(integrityconstraints)} with your
 * {@code integrityconstraints}. Warning: EGDs might break an infinity loop of
 * chase steps and thereby cause the termination of the chase. SfS does not
 * simulate this behavior. However, if EGDs prevent the termination of the
 * chase, chase on the rewritten set of TGDs won't terminate either. Warning:
 * SfS does change the rewritten constraints - if you want to perform other
 * tests on the original set of constraints or if you want to performe the
 * actual chase, you should save those constraints or reload them.
 * 
 * @author Andreas Görres
 */
public class SfS {

	/**
	 * Utility function that gives you the highest index number of all variables in
	 * a constraint, so you can introduce new variables without repeating yourself.
	 * 
	 * @param constraint the EGD or TGD to be analyzed
	 * @return the highest index number of all variables in the constraint
	 */
	private static int getMaxIndex(Constraint constraint) {
		int max = 0;
		for (RelationalAtom atom : constraint.getBody()) {
			for (Term t : atom.getTerms()) {
				if (t.isVariable()) {
					Variable var = (Variable) t;
					if (var.getIndex() > max)
						max = var.getIndex();
				}
			}
		}
		if (constraint instanceof Tgd) {
			Tgd tgd = (Tgd) constraint;
			for (RelationalAtom atom : tgd.getHead()) {
				for (Term t : atom.getTerms()) {
					if (t.isVariable()) {
						Variable var = (Variable) t;
						if (var.getIndex() > max)
							max = var.getIndex();
					}
				}
			}
		}
		if (constraint instanceof Egd) {
			Egd egd = (Egd) constraint;
			for (EqualityAtom atom : egd.getHead()) {
				Term t1 = atom.getTerm1();
				if (t1.isVariable()) {
					Variable var1 = (Variable) t1;
					if (var1.getIndex() > max)
						max = var1.getIndex();
				}
				Term t2 = atom.getTerm2();
				if (t2.isVariable()) {
					Variable var2 = (Variable) t2;
					if (var2.getIndex() > max)
						max = var2.getIndex();
				}

			}
		}
		return max;
	}

	/**
	 * We replace all EGDs with head t1=t2 by TGDs with head Eq(t1, t2) and hope
	 * that nobody will ever use "Eq" to name an actual table in the database.
	 * 
	 * @param constraints set of integrity constraints to be rewritten
	 * @return rewritten set of TGDs
	 */
	private static Set<Tgd> constraintsToTgds(Set<Constraint> constraints) {
		Set<Tgd> tgds = new HashSet<Tgd>();
		for (Constraint constraint : constraints) {
			tgds.add(constraintToTgd(constraint));
		}
		return tgds;
	}

	/**
	 * @param integrity constraint to be rewritten
	 * @return rewritten TGD
	 */
	private static Tgd constraintToTgd(Constraint constraint) {
		if (constraint instanceof Tgd)
			return (Tgd) constraint;
		Tgd tgd = null;
		if (constraint instanceof Egd) {
			Egd egd = (Egd) constraint;
			LinkedHashSet<RelationalAtom> newhead = new LinkedHashSet<RelationalAtom>();
			HashSet<EqualityAtom> oldhead = egd.getHead();
			for (EqualityAtom oldheadatom : oldhead) {
				ArrayList<Term> newheadterms = new ArrayList<Term>();
				newheadterms.add(oldheadatom.getTerm1());
				newheadterms.add(oldheadatom.getTerm2());
				RelationalAtom newheadpart = new RelationalAtom("Eq", newheadterms);
				newhead.add(newheadpart);
			}
			tgd = new Tgd(egd.getBody(), newhead);
		}
		return tgd;
	}

	/**
	 * Singularized integrity constraints substitute repeated variables (e.g.
	 * R(x,x)) and constants with fresh variables that are set equal to the original
	 * variable or constant. "Set equal" means they are added to the Eq-Relation:
	 * R(x,x,1) => R(x,y,z), Eq(x,y), Eq(1,z).
	 * 
	 * @param constraints the integrity constraints to be singularized
	 * @return the singularized set of tgds
	 */
	private static Set<Tgd> singularize(Set<Constraint> constraints) {
		Set<Tgd> tgds = constraintsToTgds(constraints);
		Set<Tgd> result = new HashSet<Tgd>();
		for (Tgd t : tgds) {
			result.add(singularization(t));
		}
		return result;
	}

	private static Tgd singularization(Tgd tgd) {
		int maxindex = getMaxIndex(tgd);
		HashSet<RelationalAtom>newBodyAtoms=new HashSet<RelationalAtom>();
		for (RelationalAtom atom : tgd.getBody()) {
			ArrayList<Term> newTerms = new ArrayList<Term>();
			for (Term t : atom.getTerms()) {
				if (t.isConstant()) {
					maxindex = maxindex + 1;
					Variable vc = new Variable(VariableType.FOR_ALL, t.toString(), maxindex);
					Term t2 = vc;
					newTerms.add(t2);
					ArrayList<Term> eqterms = new ArrayList<Term>();
					eqterms.add(t);
					eqterms.add(t2);
					RelationalAtom eqatom = new RelationalAtom("Eq", eqterms);
					newBodyAtoms.add(eqatom);
					//tgd.addToBody(eqatom);
				} else
					newTerms.add(t);
			}
			atom.setTerms(newTerms);
			newBodyAtoms.add(atom);

		}
		tgd.getBody().addAll(newBodyAtoms);
		//Tgd tgd2 = new Tgd(tgd.getBody(), tgd.getHead());
		boolean notfirst = false;
		ArrayList<RelationalAtom> zwischenspeicher = new ArrayList<RelationalAtom>();
		for (RelationalAtom a0 : tgd.getBody()) {
			for (Term t0 : a0.getTerms()) {
				if (t0.isVariable()) {
					Variable var0 = (Variable) t0;

					notfirst = false;
					for (RelationalAtom atom1 : tgd.getBody()) {
						// we don't need to singularize the Eq-Atoms we just created using
						// singularization
						if (!atom1.getName().equals("Eq")) {
							ArrayList<Term> newTerms = new ArrayList<Term>();
							for (Term t1 : atom1.getTerms()) {
								if (t0.equals(t1)) {
									if (notfirst) {
										maxindex = maxindex + 1;
										Variable newVar = new Variable(var0.getVariableType(), var0.getIndexName(),
												maxindex);
										Term newTerm = newVar;
										newTerms.add(newTerm);

										ArrayList<Term> eqterms = new ArrayList<Term>();
										eqterms.add(t0);
										eqterms.add(newTerm);
										RelationalAtom eqatom = new RelationalAtom("Eq", eqterms);
										zwischenspeicher.add(eqatom);
									} else {
										newTerms.add(t1);
										notfirst = true;
									}
								} else
									newTerms.add(t1);

							}
							atom1.setTerms(newTerms);
						}
					}
				}
			}
		}
		for (RelationalAtom a : zwischenspeicher) {
			tgd.addToBody(a);
		}
		return tgd;
	}

	/**
	 * The Eq-relation is an equivalence relation, so it is reflexive, symmetric and
	 * transitive.
	 * 
	 * @param constraints2 the integrity constraints to be rewritten
	 * @return rewritten set of TGDs
	 */
	private static Set<Tgd> getEqAx(Set<Constraint> constraints2) {
		Set<Tgd> constraints = constraintsToTgds(constraints2);
		Set<Tgd> newconstraints = new HashSet<Tgd>();
		Variable x1 = new Variable(VariableType.FOR_ALL, "x", 1);
		Variable y1 = new Variable(VariableType.FOR_ALL, "y", 1);
		Variable x2 = new Variable(VariableType.FOR_ALL, "x", 2);
		Variable y2 = new Variable(VariableType.FOR_ALL, "y", 2);
		Variable z2 = new Variable(VariableType.FOR_ALL, "z", 2);
		Term x_1 = x1;
		Term y_1 = y1;
		Term x_2 = x2;
		Term y_2 = y2;
		Term z_2 = z2;
		ArrayList<Term> body1 = new ArrayList<Term>();
		body1.add(x_1);
		body1.add(y_1);
		LinkedHashSet<RelationalAtom> b1 = new LinkedHashSet<RelationalAtom>();
		b1.add(new RelationalAtom("Eq", body1));
		ArrayList<Term> head1 = new ArrayList<Term>();
		head1.add(y_1);
		head1.add(x_1);
		LinkedHashSet<RelationalAtom> h1 = new LinkedHashSet<RelationalAtom>();
		h1.add(new RelationalAtom("Eq", head1));
		Tgd symmetry = new Tgd(b1, h1);
		ArrayList<Term> body2a = new ArrayList<Term>();
		body2a.add(x_2);
		body2a.add(y_2);
		ArrayList<Term> body2b = new ArrayList<Term>();
		body2b.add(y_2);
		body2b.add(z_2);
		LinkedHashSet<RelationalAtom> b2 = new LinkedHashSet<RelationalAtom>();
		b2.add(new RelationalAtom("Eq", body2a));
		b2.add(new RelationalAtom("Eq", body2b));
		ArrayList<Term> head2 = new ArrayList<Term>();
		head2.add(x_2);
		head2.add(z_2);
		LinkedHashSet<RelationalAtom> h2 = new LinkedHashSet<RelationalAtom>();
		h2.add(new RelationalAtom("Eq", head2));
		Tgd transitivity = new Tgd(b2, h2);
		newconstraints.add(symmetry);
		newconstraints.add(transitivity);
		HashMap<String, Integer> relations = new HashMap<String, Integer>();
		for (Constraint constr : constraints) {
			for (RelationalAtom ra : constr.getBody()) {
				relations.put(ra.getName(), ra.getTerms().size());
			}
			if (constr instanceof Tgd) {
				Tgd t = (Tgd) constr;
				for (RelationalAtom ra : t.getHead()) {
					relations.put(ra.getName(), ra.getTerms().size());
				}
			}
		}
		Set<String> relnames = relations.keySet();
		for (String relname : relnames) {
			Tgd tgd = new Tgd(new LinkedHashSet<RelationalAtom>(), new LinkedHashSet<RelationalAtom>());
			ArrayList<Term> body = new ArrayList<Term>();
			int n = relations.get(relname);
			for (int i = 0; i < n; ++i) {
				Variable v = new Variable(VariableType.FOR_ALL, "v", i);
				body.add(v);
				ArrayList<Term> head = new ArrayList<Term>();
				head.add(v);
				head.add(v);
				tgd.addToHead(new RelationalAtom("Eq", head));
			}
			tgd.addToBody(new RelationalAtom(relname, body));
			newconstraints.add(tgd);
		}
		return newconstraints;
	}

	/**
	 * Static method that performs Simulation-free Substitution.
	 * 
	 * @param constraints the set of integrity constraints to be rewritten
	 * @return return the rewritten set of TGDs
	 */
	public static Set<Tgd> doSfS(Set<Constraint> constraints) {
		// copy the constraints so that the termination test can modify them
        var constraintsCopy1 = new LinkedHashSet<Constraint>();
        var constraintsCopy2 = new LinkedHashSet<Constraint>();
        constraints.forEach(contraint -> {
        	constraintsCopy1.add(contraint.copy());
        	constraintsCopy2.add(contraint.copy());
        });
        
		Set<Tgd> set1 = getEqAx(constraintsCopy1);
		Set<Tgd> set2 = singularize(constraintsCopy2);
		set1.addAll(set2);
		//showSfS(set1);
		return set1;
	}

//	private static void showSfS(Set<Tgd> tgds) {
//		for (IntegrityConstraint c : tgds) {
//			System.out.println(c + System.lineSeparator());
//		}
//	}

	/**
	 * If you want to automate the testing process, the program needs to know if it
	 * needs to perform SfS or not, since SfS makes Constraint-Rewriting much more
	 * time consuming. Therefore, you should only perform SfS if this method returns
	 * {@code true}.
	 * 
	 * @param constraints the set of integrity constraints to be analyzed
	 * @return {@code true} if the integrity constraints contain EGDs, {@code false}
	 *         otherwise
	 */
	public static boolean containsEGD(Set<Constraint> constraints) {
		for (Constraint con : constraints) {
			if (con instanceof Egd)
				return true;
		}
		return false;
	}
}
