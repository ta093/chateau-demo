package termination;

public enum CheckType {
        WEAK_ACYCLICITY("weak acyclicity", "tgds are not weakly acyclic, Standard CHASE might not terminate.",
                        "tgds are weakly acyclic, Standard CHASE will definitely terminate."),
        RICH_ACYCLICITY("rich acyclicity", "tgds are not richly acyclic, Standard CHASE might not terminate.",
                        "tgds are richly acyclic, Standard CHASE will definitely terminate."),
        SAFEFTY("safety", "tgds are not safe, Standard CHASE might not terminate.",
                        "tgds are safe, Standard CHASE will definitely terminate."),
        ACYCLICITY("acyclicity",
                        "Constraint rewriting shows that tgds are not acyclic. Standard CHASE probably won't terminate.",
                        "Constraint rewriting shows that tgds are acyclic. Standard CHASE will definitely terminate."),
        ACYCLICITY_WITH_EGD_REWRITING("acyclicity with egd rewriting",
                        "Constraint rewriting shows that tgds/egds are not acyclic. Standard CHASE probably won't terminate.",
                        "Constraint rewriting shows that tgds/egds are acyclic. Standard CHASE will definitely terminate."),
        EGDS("correct egds", "At least one egd is defined incorrectly.", "egds are defined correctly."),
        TGDS("correct tgds", "At least one tgd is defined incorrectly.", "tgds are defined correctly."),
        STTGDS("correct s-t tgds",
                        "At least one s-t tgd is defined incorrectly. Maybe a wrong source or target tag has been set.",
                        "s-t tgds are defined correctly."),
        NEGATIONS("correct negations", "At least one negation is used incorrectly.", "Negations are used correctly.");

        public final String displayName;
        public final String positiveMessage;
        public final String negativeMessage;

        private CheckType(String name, String positiveMessage, String negativeMessage) {
                this.displayName = name;
                this.positiveMessage = positiveMessage;
                this.negativeMessage = negativeMessage;
        }
}
