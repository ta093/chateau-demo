package termination;

import java.util.ArrayList;

import atom.RelationalAtom;
import constraints.Tgd;

/**
 * This class provides us with a general test for cyclicity in a graph. The
 * algorithm can't be used to search for "special cycles" (which would be needed
 * for the Weak Acyclicity criterion), so the algorithm in
 * {@link termination.Tester} is a bit more complex than the one provided here.
 * In theory, the algorithm could be used for general cycles, however I only
 * implemented nodes for RelationalAtoms, because I need those for the
 * Constraint-Rewriting-Algorithm. To search for cycles in a graph with a
 * different node type, just extend the GeneralNode-class.
 * 
 * @author Andreas Görres
 */
public class GraphUtilities {

	private ArrayList<GeneralNode> graph = new ArrayList<GeneralNode>();
	private ArrayList<GeneralNode> visited = new ArrayList<GeneralNode>();
	private ArrayList<GeneralNode> finished = new ArrayList<GeneralNode>();

	/**
	 * constructor
	 */
	public GraphUtilities() {
	}

	public ArrayList<GeneralNode> getGraph() {
		return graph;
	}

	/**
	 * @param graph the graph to be set
	 */
	public void setGraph(ArrayList<GeneralNode> graph) {
		this.graph = graph;
	}

	/**
	 * Prints all vertices and edges (links) of the graph.
	 * 
	 */
	public void showGraph() {
		for (GeneralNode node : graph) {
			System.out.println(node);
			for (GeneralNode edge : node.linksTo) {
				System.out.println("\tlinks to: " + edge);
			}
		}
	}

	/**
	 * Searching for a cycle in the graph using depth first search.
	 * 
	 * @return {@code true} if a circle has been found, {@code false} otherwise
	 */
	public boolean searchCycle() {

		finished = new ArrayList<GeneralNode>();
		//showGraph();

		for (GeneralNode n1 : this.graph) {
			visited = new ArrayList<GeneralNode>();
			if (n1 instanceof Node) {

				Node n = (termination.GraphUtilities.Node) n1;
				if (searchCycle(n))
					return true;
			}
		}
		System.out.println("kein Zyklus");
		return false;
	}

	/**
	 * Recursive part of the method.
	 * 
	 * @param node the current starting node
	 * @return {@code true} if a cycle has been detected, {@code false} otherwise
	 */
	public boolean searchCycle(Node n) {
		if (visited.contains(n)) {
			return true;
		}

		if (finished.contains(n)) {
			return false;
		}

		visited.add(n);
		for (GeneralNode n1 : n.getLinksTo()) {
			if (searchCycle((Node) n1))
				return true;
		}
		finished.add(n);
		return false;
	}

	private int universalIndex = 1;

	/**
	 * Get or create a node for an atom.
	 * 
	 * @param atom the atom the required node should have
	 * @return a node with the required atom - if the node is not part of the graph
	 *         yet, it will be created and inserted into the graph
	 */
	public Node getNode(RelationalAtom atom) {
		for (GeneralNode n1 : this.graph) {
			if (n1 instanceof Node) {
				Node n = (termination.GraphUtilities.Node) n1;
				if (n.getAtom().getName().equals(atom.getName())
						&& (n.getAtom().getAdornments().equals(atom.getAdornments())))
					return n;
			}
		}
		Node node = new Node(atom);
		node.index = universalIndex;
		++universalIndex;
		this.graph.add(node);
		return node;
	}

	/**
	 * The {@code getNode()} method gives no explanation if a new node was created.
	 * This method won't return the required node, but only checks if it already
	 * exists in the graph.
	 * 
	 * @param atom the atom the required node should have
	 * @return {@code true} if a node with the atom is part of the graph,
	 *         {@code false} otherwise
	 */
	public boolean hasNode(RelationalAtom atom) {
		for (GeneralNode n1 : this.graph) {
			if (n1 instanceof Node) {
				Node n = (termination.GraphUtilities.Node) n1;
				if (n.getAtom().getName().equals(atom.getName())
						&& (n.getAtom().getAdornments().equals(atom.getAdornments())))
					return true;
			}
		}
		return false;
	}

	/**
	 * Inner class for abstract nodes.
	 */
	public abstract class GeneralNode {
		private ArrayList<GeneralNode> linksTo = new ArrayList<GeneralNode>();;

		/**
		 * Creates an edge to an other node.
		 * 
		 * @param node the node the edge is targeted at
		 */
		public void addLink(GeneralNode node) {
			if (!this.linksTo.contains(node))
				this.linksTo.add(node);
		}

		/**
		 * @return all nodes reachable with a single edge
		 */
		public ArrayList<GeneralNode> getLinksTo() {
			return linksTo;
		}
	}

	/**
	 * Inner class for nodes containing atoms.
	 */
	public class Node extends GeneralNode {
		private RelationalAtom atom;
		private Tgd creator = null;
		public int index = 0;

		/**
		 * constructor
		 */
		public Node(RelationalAtom atom) {
			this.atom = atom;
		}

		public RelationalAtom getAtom() {
			return atom;
		}

		/**
		 * @return the String representation of the nodes's atom including its
		 *         adornments
		 */
		public String toString() {
			return index + ": " + ConstraintRewriting.stringifyAtom(atom);
		}

		public Tgd getCreator() {
			return creator;
		}

		public void setCreator(Tgd creator) {
			this.creator = creator;
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof Node) {
				if (((Node) o).getAtom().getAdornments().equals(this.getAtom().getAdornments())
						&& ((Node) o).getAtom().getName().equals(this.getAtom().getName()))
					return true;
			}
			return false;
		}
	}
}
