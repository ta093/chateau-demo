package termination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import atom.RelationalAtom;
import constraints.Constraint;
import constraints.Tgd;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * This class performs the Adn++-algorithm to determine cyclicity of TGDs.
 * General integrity constraints will be analyzed (use
 * {@code prepareConstraintAdn()}), however, EGDs will be ignored (which could
 * lead to false results). If you wish to include EGDs into your analysis,
 * perform EGD-rewriting (SfS) first. To start the rewriting process of TGDs,
 * use the boolean method {@code prepareAdn()}, which returns false if the TGDs
 * are acyclic (which guarantees chase termination). Warning:
 * Constraint-rewriting tends to be quite slow on large TGD sets, especially
 * after performing SfS.
 * 
 * @author Andreas Görres
 */
public class ConstraintRewriting {

	/**
	 * constructor
	 */
	public ConstraintRewriting() {
	}

	/**
	 * Every TGD receives an id, the index. Copies of the TGD will keep the index of
	 * the original TGD.
	 * 
	 * @param tgds the tgds in need of an id
	 */
	private static void nameTGDs(Set<Tgd> tgds) {
		int i = 1;
		for (Tgd t : tgds) {
			t.setIndex(i);
			++i;
		}
	}

	/**
	 * Adornments of a TGD's atoms and terms are consistent.
	 * 
	 * @param t the testet TGD
	 * @return {@code true} if the TGD is consistent, {@code false} otherwise
	 */
	private static boolean tgdIsConsistent(Tgd t) {
		for (RelationalAtom a : t.getBody()) {
			if (!a.isConsistent())
				return false;
		}
		for (RelationalAtom b : t.getHead()) {
			if (!b.isConsistent())
				return false;
		}
		return true;
	}

	/**
	 * Adornments of two TGDs can be substituted.
	 * 
	 * @param source the newly created adorned TGD
	 * @param target a previously created adorned TGD
	 * @return {@code true} if the TGD is consistent, {@code false} otherwise
	 */
	private static boolean substitutable(Tgd source, Tgd target) {
		if (!tgdIsConsistent(target)) {
			return false;
		}
		if (compareTGDs(source, target)) {
			return false;
		}
		if (source.getIndex() != target.getIndex())
			return false;
		boolean full = true;
		for (RelationalAtom a : source.getHead()) {
			for (Term t : a.getTerms()) {
				if (t.isVariable()) {
					if (((Variable) t).getVariableType() == VariableType.EXISTS)
						full = false;
				}
			}
		}
		if (full)
			return false;
		HashMap<Adornment, Adornment> substitutions = new HashMap<Adornment, Adornment>();
		for (RelationalAtom ra1 : source.getBody()) {
			for (RelationalAtom ra2 : target.getBody()) {
				if (ra1.equals(ra2)) {
					for (int i = 0; i < ra1.getTerms().size(); ++i) {
						if (((ra1.getTerms().get(i).getAdornment().isB())
								&& (ra2.getTerms().get(i).getAdornment().isF()))
								|| ((ra1.getTerms().get(i).getAdornment().isF())
										&& (ra2.getTerms().get(i).getAdornment().isB()))) {
							return false;
						}
						if (substitutions.containsKey(ra1.getTerms().get(i).getAdornment())) {
							if (!substitutions.get(ra1.getTerms().get(i).getAdornment())
									.equals(ra2.getTerms().get(i).getAdornment()))
								return false;
						} else {
							substitutions.put(ra1.getTerms().get(i).getAdornment(),
									ra2.getTerms().get(i).getAdornment());
						}
					}
				}
			}
		}
		for (RelationalAtom ra1 : source.getHead()) {
			for (RelationalAtom ra2 : target.getHead()) {
				if (ra1.equals(ra2)) {
					for (int i = 0; i < ra1.getTerms().size(); ++i) {
						if (((ra1.getTerms().get(i).getAdornment().isB())
								&& (ra2.getTerms().get(i).getAdornment().isF()))
								|| ((ra1.getTerms().get(i).getAdornment().isF())
										&& (ra2.getTerms().get(i).getAdornment().isB()))) {
							return false;
						}
						if (substitutions.containsKey(ra1.getTerms().get(i).getAdornment())) {
							if (!substitutions.get(ra1.getTerms().get(i).getAdornment())
									.equals(ra2.getTerms().get(i).getAdornment()))
								return false;
						} else {
							substitutions.put(ra1.getTerms().get(i).getAdornment(),
									ra2.getTerms().get(i).getAdornment());
						}
					}
				}
			}
		}
		for (Adornment a : substitutions.keySet()) {
			if (substitutions.containsValue(a)) {
				if ((!a.equals(substitutions.get(a))))
					return false;
			}
		}
		return true;
	}

	/**
	 * @param a1 a predicate from TGD t1's head
	 * @param t2 the TGD that might be triggered by t1
	 * @return {@code true} if t1<t2, {@code false} otherwise
	 */
	private static boolean pseudoPrecedence(RelationalAtom a1, Tgd t2) {
		for (RelationalAtom a2 : t2.getBody()) {
			if (pseudoPrecedence(a1, a2))
				return true;
		}
		return false;
	}

	/**
	 * Used not only for the pseudoprecedence of TGDs, but also to detect which
	 * atoms of those TGDs could transfer their adornments. Special problem cases
	 * detected here: TGD t1 generates a constant, but TGD t2 requires a different
	 * constant, t1 generates a null value, but t2 requires a constant.
	 * 
	 * @param a1 a predicate from TGD t1's head
	 * @param a2 a predicate from TGD t2's body
	 * 
	 * @return {@code true} if t1<t2 and a1 could transfer its adornments to a2,
	 *         {@code false} otherwise
	 */
	private static boolean pseudoPrecedence(RelationalAtom a1, RelationalAtom a2) {
		if (a1.getName().equals(a2.getName()) && (a1.getTerms().size() == a2.getTerms().size())) {

			for (int i = 0; i < a2.getTerms().size(); ++i) {
				if (a2.getTerms().get(i).isConstant()) {
					if (a1.getTerms().get(i).isConstant()) {
						return a1.getTerms().get(i).equals(a2.getTerms().get(i));

					}
					if (a1.getTerms().get(i).isVariable()) {
						Variable v = (Variable) a1.getTerms().get(i);
						if (v.getVariableType() == VariableType.EXISTS)
							return false;
					}
				}
			}

			return true;
		}
		return false;
	}

	/**
	 * You would expect that variables with identical names have identical
	 * adornments (at least in the same TGD), but they might be different
	 * Java-objects. Here, we use the terms of one atom - probably an atom with
	 * freshly adorned terms - to adjust the adornments of all the other terms in
	 * the TGD (one might restrict this to variables, but right now, constants have
	 * identical b-adornments anyway).
	 * 
	 * @param vorlage the atom with "correctly" adorned terms
	 * @param tgd     the TGD containing inconsistently adorned terms
	 */
	private static void propagateOnSameTerm(RelationalAtom vorlage, Tgd tgd) {
		for (Term t : vorlage.getTerms()) {
			for (RelationalAtom a : tgd.getBody()) {
				for (Term t2 : a.getTerms()) {
					if (t.equals(t2))
						t2.setAdornment(t.getAdornment());
				}
			}
			for (RelationalAtom a : tgd.getHead()) {
				for (Term t2 : a.getTerms()) {
					if (t.equals(t2))
						t2.setAdornment(t.getAdornment());
				}
			}
		}
	}

	/**
	 * After calling {@code propagateOnSameTerm()}, there are still two problems: 1)
	 * We adjusted the term adornments, but the atom adornments might be
	 * inconsistent, 2) we did not adjust adornments of existentially quantified
	 * variables (which depend on the adornments of variables we previously
	 * adjusted).
	 * 
	 * @param t the TGD whose head's adornments need to be adjusted if the body is
	 *          consistent
	 * @return {@code true} if the body of t is consistent, {@code false} otherwise
	 */
	private static boolean headAdornment(Tgd t) {
		if (!isConsistent(t))
			return false;
		forceHeadAdornment(t);
		return true;
	}

	/**
	 * Adornments of existentially quantified variables depend on the adornments of
	 * all universally quantified variables of the TGD's head. Since this adornment
	 * is new, the atom's adornment is obviously now inconsistent and needs to be
	 * adjusted to the new adornment.
	 * 
	 * @param t the TGD whose head's adornments need to be adjusted
	 * @return {@code true} if the new mapping has been added, {@code false}
	 *         otherwise
	 */
	private static void forceHeadAdornment(Tgd t) {
		t.setAdornmentsForSkolemization();
		for (RelationalAtom a1 : t.getHead()) {
			a1.receiveAdornments();
		}
	}

	/**
	 * Atoms have an adornment for each of their terms. Those terms have their own
	 * adornments. In consistent atoms, those adornments match perfectly. Consistent
	 * TGD only contain consistent atoms (in the body, atoms in the head should
	 * never be inconsistent).
	 * 
	 * @param t the TGD tested for inconsistencies
	 * @return {@code true} if the TGD is consistent, {@code false} otherwise
	 */
	private static boolean isConsistent(Tgd t) {
		for (RelationalAtom a1 : t.getBody()) {
			if (!a1.isConsistent()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Duplicates a TGD including all of its adornments. Terms are duplicated, but
	 * the terms' variables remain identical Java-objects - however, we never change
	 * those anyway.
	 * 
	 * @param t the TGD to be duplicated
	 * @return a new Java-object with properties identical to t
	 */
	private static Tgd duplicateTGD(Tgd t1) {
	    LinkedHashSet<RelationalAtom> body = new LinkedHashSet<RelationalAtom>();
	    LinkedHashSet<RelationalAtom> head = new LinkedHashSet<RelationalAtom>();
		for (RelationalAtom a : t1.getBody()) {
			ArrayList<Term> terms = new ArrayList<Term>();
			for (Term t : a.getTerms()) {
				terms.add(t.copy());
			}

			RelationalAtom a2 = new RelationalAtom(a.getName(), terms);
			a2.setAdornments(a.getAdornments());
			body.add(a2);
		}
		for (RelationalAtom a : t1.getHead()) {
			ArrayList<Term> terms = new ArrayList<Term>();
			for (Term t : a.getTerms()) {
				terms.add(t.copy());
			}

			RelationalAtom a2 = new RelationalAtom(a.getName(), terms);
			a2.setAdornments(a.getAdornments());
			head.add(a2);
		}
		Tgd t2 = new Tgd(body, head);
		t2.setIndex(t1.getIndex());
		return t2;
	}

	/**
	 * Compare two TGDs. The TGDs don't have to be consistent since adornments of
	 * terms are also compared. Simply comparing the TGD with {@code equals()}
	 * wouldn't compare the adornments at all, and comparing the results of
	 * {@code stringifyTGD()} would compare adornments of atoms, but not of terms.
	 * 
	 * @param t1 the first TGD
	 * @param t2 the second TGD
	 * @return {@code true} if t1 and t2 are equal, {@code false} otherwise
	 */
	private static boolean compareTGDs(Tgd t1, Tgd t2) {
		if (t1.getIndex() != t2.getIndex())
			return false;
		if (!stringifyTGD(t1).equals(stringifyTGD(t2)))
			return false;
		for (RelationalAtom a : t1.getBody()) {
			for (RelationalAtom a2 : t2.getBody()) {
				if (a.equals(a2)) {
					if (!a.getAdornments().equals(a2.getAdornments()))
						return false;
					for (int i = 0; i < a.getTerms().size(); ++i) {
						if (!a.getTerms().get(i).getAdornment().equals(a2.getTerms().get(i).getAdornment()))
							return false;
					}
				}

			}

		}
		for (RelationalAtom a : t1.getHead()) {
			for (RelationalAtom a2 : t2.getHead()) {
				if (a.equals(a2)) {
					if (!a.getAdornments().equals(a2.getAdornments()))
						return false;
					for (int i = 0; i < a.getTerms().size(); ++i) {
						if (!a.getTerms().get(i).getAdornment().equals(a2.getTerms().get(i).getAdornment()))
							return false;
					}
				}

			}

		}
		return true;
	}

	/**
	 * Utility function that transforms a set of TGDs into a list of TGDs.
	 * 
	 * @param set the set of TGDs
	 * @return a list containing the set's TGDs
	 */
	private static ArrayList<Tgd> setToList(Set<Tgd> set) {
		ArrayList<Tgd> list = new ArrayList<Tgd>();
		for (Tgd t : set) {
			list.add((Tgd) t);
		}
		return list;
	}

	/**
	 * Since Adn++ only accepts TGDs, we have to filter the original set of
	 * integrity constraints and remove EGDs. Of course, EGD-rewriting removes EGDs
	 * anyway, so we won't call {@code extractTGDs()} after performing SfS.
	 * 
	 * @param set the set of integrity constraint
	 * @return a subset of the original set containing only its TGDs
	 */
	public static Set<Tgd> extractTGDs(Set<Constraint> set) {
		Set<Tgd> list = new HashSet<Tgd>();
		for (Constraint t : set) {
			if (t instanceof Tgd)
				list.add((Tgd) t);
		}
		return list;
	}

	/**
	 * All Adornments are b-Adornments.
	 * 
	 * @param a an adorned relational atom
	 * @return {@code true} if all of the atom's adornments are b, {@code false}
	 *         otherwise
	 */
	private boolean isB(RelationalAtom a) {
		for (Adornment ad : a.getAdornments()) {
			if (ad.isF())
				return false;
		}
		return true;
	}

	private GraphUtilities graph = new GraphUtilities();

	/**
	 * After parsing an xml- or txt-file, we generally have a set of integrity
	 * constraints. However, we could have performed EGD-rewriting, which creates a
	 * set of TGDs. In the first case, we have to exclude the EGDs first. Returns
	 * {@code false} if chase termination is guaranteed.
	 * 
	 * @param constraints the set of EGDs and TGDs
	 * @return {@code true} if the integrity constraints are not acyclic,
	 *         {@code false} otherwise
	 */
	public boolean prepareConstraintAdn(Set<Constraint> constraints) {
		return prepareAdn(extractTGDs(constraints));
	}

	/**
	 * Performs the initial phase of constraint-rewriting. However, the separation
	 * of {@code prepareAdn()} and {@code adnPlusPlus()} is simply for clarity.
	 * Algorithm uses a list semantics, but calling it repeatedly might result in
	 * differently ordered lists (and therefore run time). Returns {@code false} if
	 * chase termination is guaranteed.
	 * 
	 * @param tgds a set of TGDs
	 * @return {@code true} if the TGDs are not acyclic, {@code false} otherwise
	 */
	public boolean prepareAdn(Set<Tgd> tgds) {
		// gibt urspruenglichen TGDs eindeutigen Index
		nameTGDs(tgds);
		ArrayList<Tgd> base = setToList(tgds);
		ArrayList<RelationalAtom> preds = new ArrayList<RelationalAtom>();
		for (Tgd t : base) {
			for (RelationalAtom a : t.getBody()) {
				// Adornments der Terme werden zu Adornments des Atoms
				a.receiveAdornments();
			}
			// erzeuge f-Adornments der existenzquantifizierten Variablen
			t.setAdornmentsForSkolemization();
			for (RelationalAtom a : t.getHead()) {
				a.receiveAdornments();
			}
			for (RelationalAtom head : t.getHead()) {
				boolean unknown = true;
				for (RelationalAtom pred : preds) {
					if (stringifyAtomhead(pred).equals(stringifyAtomhead(head)))
						unknown = false;
				}
				if (unknown) {
					// neue Praedikate werden gespeichert
					preds.add(head);
				}
			}
		}
		return adnPlusPlus(preds, base);
	}

	/**
	 * The main phase of Adn++. Method is renamed to "adnPP()" in the written thesis
	 * (due to lack of space). The arguments "predsunion" and "base" also have
	 * different names. Returns {@code false} if chase termination is guaranteed.
	 * 
	 * @param predsunion the list of adorned predicates created in the initial phase
	 *                   of Adn++
	 * @param base       the list of adorned TGDs created in the initial phase of
	 *                   Adn++
	 * @return {@code true} if the TGDs are not acyclic, {@code false} otherwise
	 */
	private boolean adnPlusPlus(ArrayList<RelationalAtom> predsunion, ArrayList<Tgd> base) {

		boolean cyk = false;
		// to efficiently compare head and body of new and old TGDs via their hash
		HashSet<String> bodystrings = new HashSet<String>();
		HashSet<String> headstrings = new HashSet<String>();
		for (Tgd t : base) {
			String tst = stringifyTGD(t);
			bodystrings.add(tst.split("->")[0].trim());
			headstrings.add(tst.split("->")[1].trim());
		}
		// Cycle through adorned predicates
		for (int predindex = 0; predindex < predsunion.size(); ++predindex) {
			RelationalAtom p = predsunion.get(predindex);
			// create if not exist new predicate knot
			GraphUtilities.Node node1 = graph.getNode(p);
			// Cycle through adorned TGDs
			for (int tgdindex = 0; tgdindex < base.size(); ++tgdindex) {
				Tgd t = base.get(tgdindex);
				// p can be inserted into the body of t and is compatible with
				if (pseudoPrecedence(p, t)) {
					for (RelationalAtom p2 : t.getBody()) {
						if (pseudoPrecedence(p, p2) && (!p.getAdornments().equals(p2.getAdornments())) && (isB(p2))) {
							Tgd adornedTGD = duplicateTGD(t);
							for (RelationalAtom p3 : adornedTGD.getBody()) {
								if (p2.equals(p3)) {
									p3.setAdornments(p.getAdornments());
									// Adornments are transferred to atom
									for (int i = 0; i < p3.getTerms().size(); ++i) {
										Term con = p3.getTerms().get(i);
										if (con.isConstant())
											p3.getAdornments().set(i, new Adornment());
									}
									// Adornments become Adornments on terms
									p3.transferAdornments();
									// equal terms receive equal adornments
									propagateOnSameTerm(p3, adornedTGD);
								}
							}
							// Adornments of the TGD-Body are not consistent
							if (!headAdornment(adornedTGD)) {
								boolean tgdisnew1 = true;
								if (bodystrings.contains(stringifyTGD(adornedTGD).split("->")[0]))
									tgdisnew1 = false;
								if (tgdisnew1) {
									// nicht konsistente TGD wird vielleicht spaeter konsistent wenn weitere adornte
									// Praedikate eingefuegt wurden
									// not consistent TGD becomes perhabs consistent if further adorned predicates were inserted
									base.add(adornedTGD);
									String atgt = stringifyTGD(adornedTGD);
									bodystrings.add(atgt.split("->")[0].trim());
									headstrings.add(atgt.split("->")[1].trim());
								}
							}
							// Adornments are consistent, Adornments for #E variables in the head are redefined
							else {
								boolean tgdisnew = true;
								if (headstrings.contains(stringifyTGD(adornedTGD).split("->")[1].trim()))
									tgdisnew = false;
								if (!bodystrings.contains(stringifyTGD(adornedTGD).split("->")[0].trim()))
									base.add(adornedTGD);
								// Test the substitution of the Adornments
								if (tgdisnew) {
									String btst = stringifyTGD(adornedTGD);
									bodystrings.add(btst.split("->")[0].trim());
									headstrings.add(btst.split("->")[1].trim());
									cyk = false;
									for (Tgd oldTGD : base) {
										// the adornments of the TGDs are substitutable
										if (substitutable(adornedTGD, oldTGD)) {
											for (RelationalAtom sub : oldTGD.getHead()) {
												GraphUtilities.Node node3 = graph.getNode(sub);
												// predicate graph is expaned
												node1.addLink(node3);
											}
											// TGDs not acyclic, Chase probably don't terminate
											if (graph.searchCycle())
												return true;
											// didn't found a cycle through subsitution
											// found a converging structure
											cyk = true;
										}
									}
								}
								// Substitution not successful, safe all new adorned predicates of the head
								if (tgdisnew && (!cyk)) {
									for (RelationalAtom pred : adornedTGD.getHead()) {
										boolean unknown = true;
										for (RelationalAtom exis : predsunion) {
											if ((stringifyAtomhead(pred).equals(stringifyAtomhead(exis))))
												unknown = false;
										}
										if (unknown) {
											predsunion.add(pred);
											// expand predicate graph
											GraphUtilities.Node node2 = graph.getNode(pred);
											node1.addLink(node2);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		// TGDs are acyclic
		// Chase terminate for sure
		return false;
	}

	/**
	 * Creates a String representation of a relational atom including all of its
	 * adornments and terms (but not the terms' adornments, so inconsistencies can't
	 * be detected this way). The {@code toString()}-method would omit the
	 * adornments.
	 * 
	 * @param ra1 the relational atom to be stringified
	 * @return the String representation of ra1
	 */
	public static String stringifyAtom(RelationalAtom ra1) {
		String result = "";
		result += ra1.getName() + "^";
		for (int i = 0; i < ra1.getAdornments().size(); ++i) {
			Adornment a = ra1.getAdornments().get(i);
			result += a;
		}
		result += " (";
		for (int i = 0; i < ra1.getTerms().size(); ++i) {
			Term t = ra1.getTerms().get(i);
			result += t;
			if (i < (ra1.getTerms().size() - 1))
				result += ", ";

		}
		result += ")";
		return result;
	}

	/**
	 * Creates a String representation of a relational atom including all of its
	 * adornments, but without the terms. The {@code toString()}-Method would omit
	 * the adornments, but include the terms, while {@code stringifyAtom()} would
	 * include both terms and adornments.
	 * 
	 * @param ra1 the relational atom to be stringified without its terms
	 * @return the String representation of ra1
	 */
	private static String stringifyAtomhead(RelationalAtom ra1) {
		String result = "";
		result += ra1.getName() + "^";
		for (int i = 0; i < ra1.getAdornments().size(); ++i) {
			Adornment a = ra1.getAdornments().get(i);
			result += a;
		}

		return result;
	}

	/**
	 * Creates a String representation of a TGD including all of its adornments (but
	 * not the terms' adornments, so inconsistencies can't be detected this way).
	 * The {@code toString()}-method would omit the adornments and add line breaks
	 * after each atom, while this method doesn't add line separators. You would
	 * expect that the String representation of a TGD is not unique (since body and
	 * head are both sets of relational atoms), but I found it to be sufficient for
	 * testing equality of TGDs (and to detect the presence of a TGD-representation
	 * in a hashset of TGD-representations). For detecting containment in a hashset
	 * directly, I would have to modify the {@code equals()} and
	 * {@code hash()}-methods of TGDs (and even relational atoms), which might
	 * interfere with the actual chase.
	 * 
	 * @param tgd the TGD to be stringified
	 * @return the String representation of the TGD
	 */
	private static String stringifyTGD(Tgd tgd) {
		if (tgd == null)
			return "(no TGD)";
		String result = "";
		for (RelationalAtom ra1 : tgd.getBody()) {
			result += ra1.getName() + "^";
			for (int i = 0; i < ra1.getAdornments().size(); ++i) {
				Adornment a = ra1.getAdornments().get(i);
				result += a;
			}
			result += " (";
			for (int i = 0; i < ra1.getTerms().size(); ++i) {
				Term t = ra1.getTerms().get(i);
				result += t;
				if (i < (ra1.getTerms().size() - 1))
					result += ", ";
			}
			result += "), ";
		}
		if (result.contains(","))
			result = result.substring(0, result.lastIndexOf(","));
		result += " -> ";
		for (RelationalAtom ra1 : tgd.getHead()) {
			result += ra1.getName() + "^";
			for (int i = 0; i < ra1.getAdornments().size(); ++i) {
				Adornment a = ra1.getAdornments().get(i);
				result += a;
			}
			result += " (";
			for (int i = 0; i < ra1.getTerms().size(); ++i) {
				Term t = ra1.getTerms().get(i);
				result += t;
				if (i < (ra1.getTerms().size() - 1))
					result += ", ";

			}
			result += "), ";
		}
		if (result.contains(","))
			result = result.substring(0, result.lastIndexOf(","));
		return result;
	}

}
