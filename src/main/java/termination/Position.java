package termination;

import java.util.ArrayList;

/**
 * This class represents a position in a relation. Therefore, objects of the
 * class are characterized by the relation (that is, its name) and one of the
 * relation's attributes (that is, its index number). Positions are part of a
 * directed graph and can have to types of edges to other positions: normal
 * edges and special edges. Normal edges copy value already present in the
 * database into a newly created tuple, whereas special edges (might) (might)
 * generate fresh null values in a newly created tuple. Positions are used not
 * only for the weak acyclicity criterion, but also for rich acyclicity and
 * safety. For the safety criterion, we differentiate between "affected"
 * positions (positions that can carry null values) and "unaffected" null values
 * (positions that can never carry null values).
 * 
 * @author Andreas Görres
 */
public class Position {

	private String relationName;
	private int index;
	private ArrayList<Position> edges = new ArrayList<Position>();
	private ArrayList<Position> specialedges = new ArrayList<Position>();
	private boolean affected = false;

	/**
	 * @return can this position carry a null-value? Needed for the safety
	 *         criterion, not for WA
	 */
	public boolean isAffected() {
		return affected;
	}

	/**
	 * @param affected makes the position affected ({@code true}) or unaffected
	 *                 ({@code false})
	 */
	public void setAffected(boolean affected) {
		this.affected = affected;
	}

	/**
	 * @return the name of the position's relation
	 */
	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

	/**
	 * @return the position's index (in a relation)
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to be set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the positions reachable with a single normal edge
	 */
	public ArrayList<Position> getEdges() {
		return edges;
	}

	/**
	 * @param edges the edges to be set
	 */
	public void setEdges(ArrayList<Position> edges) {
		this.edges = edges;
	}

	/**
	 * @return the positions reachable with a single special (generating) edge
	 */
	public ArrayList<Position> getSpecialedges() {
		return specialedges;
	}

	/**
	 * @param specialedges the special edges to be set
	 */
	public void setSpecialedges(ArrayList<Position> specialedges) {
		this.specialedges = specialedges;
	}

	/**
	 * Adds a normal edge to the node.
	 * 
	 * @param p target position of the special edge
	 */
	public void addEdges(Position p) {
		if (!this.edges.contains(p))
			this.edges.add(p);
	}

	/**
	 * Adds a special edge to the node.
	 * 
	 * @param p target position of the special edge
	 */
	public void addSpecialedges(Position p) {
		if (!this.specialedges.contains(p))
			this.specialedges.add(p);
	}

	/**
	 * Used for the safety criterion
	 */
	public void removeAllEdges() {
		this.edges = new ArrayList<Position>();
		this.specialedges = new ArrayList<Position>();
	}

	/**
	 * Constructor
	 * 
	 * @param relationName name from the atom used to create the position
	 * @param index        the position of the attribute in the term-arraylist of
	 *                     the atom
	 */
	public Position(String relationName, int index) {
		this.relationName = relationName;
		this.index = index;
	}
}
