package termination;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Set;

import atom.Atom;
import atom.RelationalAtom;
import constraints.Constraint;
import constraints.Tgd;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * The class Tester provides the algorithms for three very similar termination
 * criteria: Weak Acyclicity (WA) itself, Rich Acyclicity (RA) and Safety (SC).
 * Rich Acyclicity indicates the termination of the Naive Oblivious Chase (not
 * implemented in ChaTEAU), whereas Safety is simply a more powerful version of
 * Weak Acyclicity. The methods {@code checkWeakAcyclicity(constraints)},
 * {@code checkRichAcyclicity(constraints)} and {@code checkSafety(constraints)}
 * each return a boolean which informs you if a dangerous cycle has been found.
 * Therefore, a positive result serves as a warning sign - do not start the
 * Chase algorithm since it might not terminate.
 * 
 * @author Andreas Görres
 */
public class Tester {

	private ArrayList<Position> positions = new ArrayList<Position>();

	/**
	 * constructor
	 */
	public Tester() {
	}

	/**
	 * Gets or, if it doesn't already exist, creates a position characterized by a
	 * relation and an index.
	 * 
	 * @param posname  the name of the position's relation
	 * @param posindex the position's index
	 * @return the position characterized by posname and posindex
	 */
	public Position getPosition(String posname, int posindex) {
		for (Position pos : this.positions) {
			if (pos.getRelationName().equals(posname) && (pos.getIndex() == posindex)) {
				return pos;
			}
		}
		Position pos = new Position(posname, posindex);
		positions.add(pos);
		return pos;
	}

	/**
	 * Safety criterion is based on the notion of "affected" positions. Here,
	 * positions are tested and, if necessary, marked as affected. First part of the
	 * recursive algorithm: Positions of existence quantified variables are always
	 * affected.
	 * 
	 * @param constraints the integrity constraints whose positions are marked as
	 *                    affected if they can carry a null value
	 */
	private void affectInit(Set<Constraint> constraints) {
		for (Constraint ic1 : constraints) {
			if (ic1 instanceof Tgd) {
				Tgd ic = (Tgd) ic1;
				for (Atom headatom2 : ic.getHead()) {
					if (headatom2 instanceof RelationalAtom) {
						RelationalAtom exatom = (RelationalAtom) headatom2;
						ListIterator<Term> li3 = exatom.getTerms().listIterator();
						String relname3 = exatom.getName();
						while (li3.hasNext()) {
							int index3 = li3.nextIndex();
							Term headterm2 = li3.next();
							if (headterm2.isVariable()) {
								Variable exterm = (Variable) headterm2;
								if (exterm.getVariableType() == VariableType.EXISTS) {
									getPosition(relname3, index3).setAffected(true);
									;
								}
							}
						}
					}
				}
			}
		}
		affect(constraints);
	}

	/**
	 * Second, recursive part of the algorithm: If a variable is in an affected
	 * body-position and all body-positions of this variable are affected: affect
	 * all head positions of this variable. Affected positions are recursively
	 * propagated until nothing changes anymore.
	 * 
	 * @param constraints the integrity constraints whose positions are marked as
	 *                    affected if they can carry a null value
	 */
	private void affect(Set<Constraint> constraints) {
		boolean changed = false;
		for (Constraint ic : constraints) {
			if (ic instanceof Tgd) {
				for (RelationalAtom bodyatom : ic.getBody()) {
					ListIterator<Term> li1 = bodyatom.getTerms().listIterator();
					String relname1 = bodyatom.getName();
					while (li1.hasNext()) {
						boolean notallaffected = true;
						int index1 = li1.nextIndex();
						Term bodyterm1 = li1.next();
						if (getPosition(relname1, index1).isAffected()) {
							notallaffected = false;
							if (bodyterm1.isVariable()) {
								Variable bodyterm = (Variable) bodyterm1;
								for (RelationalAtom bodyatom3 : ic.getBody()) {
									ListIterator<Term> li3 = bodyatom3.getTerms().listIterator();
									String relname3 = bodyatom3.getName();
									while (li3.hasNext()) {
										int index3 = li3.nextIndex();
										Term bodyterm3 = li3.next();
										if (bodyterm3.isVariable()) {
											Variable secondbodyterm = (Variable) bodyterm3;
											if ((bodyterm.getIndexName().equals(secondbodyterm.getIndexName()))
													&& (bodyterm.getIndex() == secondbodyterm.getIndex())) {
												if (!getPosition(relname3, index3).isAffected()) {
													notallaffected = true;
												}
											}
										}
									}
								}
								if (!notallaffected) {
									for (Atom headatom1 : ic.getHead()) {
										if (headatom1 instanceof RelationalAtom) {
											RelationalAtom headatom = (RelationalAtom) headatom1;
											ListIterator<Term> li2 = headatom.getTerms().listIterator();
											String relname2 = headatom.getName();
											while (li2.hasNext()) {
												int index2 = li2.nextIndex();
												Term headterm1 = li2.next();
												if (headterm1.isVariable()) {
													Variable headterm = (Variable) headterm1;
													if (headterm.getVariableType() == VariableType.FOR_ALL) {
														if ((headterm.getIndexName().equals(bodyterm.getIndexName()))
																&& (headterm.getIndex() == bodyterm.getIndex())) {
															if (!getPosition(relname2, index2).isAffected()) {
																getPosition(relname2, index2).setAffected(true);
																changed = true;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (changed)
			affect(constraints);
	}

	/**
	 * The safety criterion is almost identical to WA, but no edges originate in
	 * unaffected positions .
	 */
	private void removeUnaffectedEdges() {
		for (Position p : positions) {
			if (!p.isAffected())
				p.removeAllEdges();
		}
	}

	/**
	 * Same variable in body and head: draw edge between the respective positions,
	 * draw special edges to all head positions of existentially quantified
	 * variables. For rich acyclicity: Draw special edges from positions of each
	 * body variable to all head positions of existentially quantified variables
	 * 
	 * @param constraints the integrity constraints to be tested
	 * @param rich        if rich=={@code true}, rich acylicity will be tested
	 *                    instead of WA or safety
	 */
	private void drawEdges(Set<Constraint> constraints, boolean rich) {
		boolean allquant; // variable in the body is universally quantified in body and head
		for (Constraint ic : constraints) {
			if (ic instanceof Tgd) {
				for (RelationalAtom bodyatom : ic.getBody()) {
					ListIterator<Term> li1 = bodyatom.getTerms().listIterator();
					String relname1 = bodyatom.getName();
					while (li1.hasNext()) {
						allquant = false;
						int index1 = li1.nextIndex();
						Term bodyterm1 = li1.next();
						if (bodyterm1.isVariable()) {
							Variable bodyterm = (Variable) bodyterm1;
							for (Atom headatom1 : ic.getHead()) {
								if (headatom1 instanceof RelationalAtom) {
									RelationalAtom headatom = (RelationalAtom) headatom1;
									ListIterator<Term> li2 = headatom.getTerms().listIterator();
									String relname2 = headatom.getName();
									while (li2.hasNext()) {
										int index2 = li2.nextIndex();
										Term headterm1 = li2.next();
										if (headterm1.isVariable()) {
											Variable headterm = (Variable) headterm1;
											if (headterm.getVariableType() == VariableType.FOR_ALL) {
												if ((headterm.getIndexName().equals(bodyterm.getIndexName()))
														&& (headterm.getIndex() == bodyterm.getIndex())) {
													allquant = true;
													getPosition(relname1, index1)
															.addEdges(getPosition(relname2, index2));
												}
											}
										}
									}
								}
							}
							if (allquant || rich) {
								for (Atom headatom2 : ic.getHead()) {
									if (headatom2 instanceof RelationalAtom) {
										RelationalAtom exatom = (RelationalAtom) headatom2;
										ListIterator<Term> li3 = exatom.getTerms().listIterator();
										String relname3 = exatom.getName();
										while (li3.hasNext()) {
											int index3 = li3.nextIndex();
											Term headterm2 = li3.next();
											if (headterm2.isVariable()) {
												Variable exterm = (Variable) headterm2;
												if (exterm.getVariableType() == VariableType.EXISTS) {
													getPosition(relname1, index1)
															.addSpecialedges(getPosition(relname3, index3));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	ArrayList<Position> finished = new ArrayList<Position>();

	/**
	 * Find a cycle of nodes with at least one special edge. Algorithm is based on
	 * recursive depth first search.
	 * 
	 * @return {@code true} if a cycle through a special edge has been found,
	 *         {@code false} otherwise
	 */
	private boolean searchSpecialCycle() {
		for (Position p : positions) {
			if (searchSpecialCycle(p, -1, new ArrayList<Position>()))
				return true;
		}
		return false;
	}

	/**
	 * Recursive part of the method.
	 * 
	 * @param v            the starting position
	 * @param durchSpecial the most recent index of "visited" with a special edge
	 * @param visited      list of already visited nodes
	 * @return {@code true} if a cycle through a special edge has been found,
	 *         {@code false} otherwise
	 */
	private boolean searchSpecialCycle(Position v, int durchSpezial, ArrayList<Position> visited) {
		if (finished.contains(v)) {
			return false;
		}
		if (visited.contains(v)) {
			if (visited.indexOf(v) > durchSpezial) {
				return false;
			} else {
				return true;
			}
		}
		visited.add(v);
		for (Position w2 : v.getSpecialedges()) {
			if (searchSpecialCycle(w2, visited.indexOf(v), visited)) {
				return true;
			}

		}
		for (Position w1 : v.getEdges()) {
			if (searchSpecialCycle(w1, durchSpezial, visited)) {
				return true;
			}
		}
		finished.add(v);
		return false;
	}

	/**
	 * Based on the boolean arguments, three different termination tests can be
	 * performed: WA, RA and SC.
	 * 
	 * @param constraints the integrity constraints to be tested
	 * @param safety      {@code true} if safety should be tested instead of WA
	 * @param rich        {@code true} if rich acyclicity should be tested instead
	 *                    of WA (don't combine with safety!)
	 * 
	 * @return {@code true} if integrity constraints are not weakly acyclic/ richly
	 *         acyclic/ safe, {@code false} otherwise
	 */
	public boolean checkWeakAcyclicity(Set<Constraint> constraints, boolean safety, boolean rich) {
		this.drawEdges(constraints, rich);
		if (safety) {
			this.affectInit(constraints);
			this.removeUnaffectedEdges();
		}
		return this.searchSpecialCycle();
	}

	/**
	 * This tests a set of integrity constraints for weak acyclicity. Standard chase
	 * termination is guaranteed if this test returns {@code false}.
	 * 
	 * @param constraints the integrity constraints to be tested
	 * @return {@code true} if integrity constraints are not weakly acyclic,
	 *         {@code false} otherwise
	 */
	public boolean checkWeakAcyclicity(Set<Constraint> constraints) {
		this.drawEdges(constraints, false);
		return this.searchSpecialCycle();
	}

	/**
	 * This tests a set of integrity constraints for rich acyclicity. Naive
	 * oblivious chase termination is guaranteed if this test returns {@code false}.
	 * 
	 * @param constraints the integrity constraints to be tested
	 * @return {@code true} if integrity constraints are not richly acyclic,
	 *         {@code false} otherwise
	 */
	public boolean checkRichAcyclicity(Set<Constraint> constraints) {
		this.drawEdges(constraints, true);
		return this.searchSpecialCycle();
	}

	/**
	 * This tests a set of integrity constraints for safety. Standard chase
	 * termination is guaranteed if this test returns {@code false}.
	 * 
	 * @param constraints the integrity constraints to be tested
	 * @return {@code true} if integrity constraints are not safe, {@code false}
	 *         otherwise
	 */
	public boolean checkSafety(Set<Constraint> constraints) {
		this.drawEdges(constraints, false);
		this.affectInit(constraints);
		this.removeUnaffectedEdges();
		return this.searchSpecialCycle();
	}

}
