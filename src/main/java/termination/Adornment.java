package termination;

import java.util.HashSet;

/**
 * An Adornment is a flag belonging to a term or an atom that is propagated
 * during Adn++ (similar to the value of variables during chase). For consistent
 * atoms, the atom's adornments correspond to the adornments of the atom's
 * terms. While b-Adornments are only characterized only by their
 * {@code AdornmentType}, f-Adornments reference a skolem function and include
 * therefore the parameters of this function - a set of other adornments.
 * Additionally, f-adornments are characterized by the TGD and the existentially
 * quantified variable that led to their creation. The default
 * {@code AdornmentType} is b.
 * 
 * @author Andreas Görres
 */
public class Adornment {
	private AdornmentType type = AdornmentType.BOUNDED;
	private int constraintindex = -1;
	private String variable = null;
	private HashSet<Adornment> skolemOf = null;

	/**
	 * @return the existentially quantified variable characterizing the f-adornment,
	 *         {@code null} for b-adornments
	 */
	public String getVariable() {
		return variable;
	}

	/**
	 * @param variable the variable to be set
	 */
	public void setVariable(String variable) {
		this.variable = variable;
	}

	/**
	 * @return the adornment type (f or b)
	 */
	public AdornmentType getType() {
		return type;
	}

	/**
	 * @param type the adornment type to be set
	 */
	public void setType(AdornmentType type) {
		this.type = type;
	}

	/**
	 * @return the id (index) of the TGD originally generating the f-adornment, -1
	 *         for b-adornments
	 */
	public int getConstraintindex() {
		return constraintindex;
	}

	/**
	 * @param constraintindex the constraint index to be set
	 */
	public void setConstraintindex(int constraintindex) {
		this.constraintindex = constraintindex;
	}

	/**
	 * @return the set of adornments that are the parameters of the associated
	 *         skolem function
	 */
	public HashSet<Adornment> getSkolemOf() {
		return skolemOf;
	}

	/**
	 * @param skolemOf the adornments to be set as parameters of the skolem function
	 */
	public void setSkolemOf(HashSet<Adornment> skolemOf) {
		this.skolemOf = skolemOf;
	}

	/**
	 * constructor for b-adornments
	 */
	public Adornment() {
	}

	/**
	 * constructor
	 */
	public Adornment(AdornmentType type) {
		if (type == AdornmentType.FREE) {
			this.setType(type);
			this.setConstraintindex(0);
			this.setVariable("");
			this.setSkolemOf(new HashSet<Adornment>());
		}
	}

	/**
	 * constructor
	 */
	public Adornment(int constraintindex, String variable, HashSet<Adornment> skolemOf) {
		this.type = AdornmentType.FREE;
		this.constraintindex = constraintindex;
		this.variable = variable;
		this.skolemOf = skolemOf;
	}

	/**
	 * @return {@code true} if this is a b-adornment, {@code false} otherwise
	 */
	public boolean isB() {
		return this.type == AdornmentType.BOUNDED;
	}

	/**
	 * @return {@code true} if this is an f-adornment, {@code false} otherwise
	 */
	public boolean isF() {
		return this.type == AdornmentType.FREE;
	}

	public String toString() {
		if (this.isB()) {
			return "b";
		}

		String sko = "";
		for (Adornment a : this.getSkolemOf()) {
			sko += a.toString();
		}

		return "f^r" + this.getConstraintindex() + "_" + this.getVariable() + "(" + sko + ")";
	}

	@Override
	public boolean equals(Object a1) {
		if (a1 instanceof Adornment) {
			Adornment a = (Adornment)a1;
			if (this.isB()) {
				return a.isB();
			} else {
				return this.toString().equals(a.toString());
			}
		}
		return false;
	}
}