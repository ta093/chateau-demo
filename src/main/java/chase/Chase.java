package chase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.stream.Collectors;

import atom.EqualityAtom;
import atom.ProvenanceInformation;
import atom.RelationalAtom;
import constraints.Constraint;
import constraints.Egd;
import constraints.ScopeType;
import constraints.Tgd;
import galve.BEgd;
import homomorphism.Homomorphism;
import homomorphism.TermMapping;
import instance.Instance;
import instance.OriginTag;
import io.ChateauLogger;
import javafx.util.Pair;
import term.Null;
import term.Term;
import term.Variable;
import term.VariableType;

public class Chase {

	// fields accessed by the chase method - must be initialized beforehand or
	// passed as parameters
	private Instance instance;

	private LinkedHashSet<Constraint> integrityConstraints;

	// fields for running chase

	// nullcounter stores the number of null values per attribute
	/**
	 * @see {@link #initializeNullCounter()}
	 */
	private HashMap<String, Integer> nullCounter;

	// stores the number of provenance ids per relation
	private HashMap<String, Integer> provenanceIDCounter;

	// cache the new facts after each run of the chaseMainLoop - initialized with
	// the whole instance
	private Instance newFacts;

	// cache the new instance during each run of the chaseMainLoop - initialized
	// with the empty instance
	private Instance newInstance;

	// cache the entire new instance during a run of the chaseMainLoop - initialized
	// with the whole instance
	private Instance instanceWithNewFacts;

	// homomorphism cache of all mappings that occur in a run of the chaseMainLoop
	// between the old (I_i) and the new instance (I_i+1)
	// = homomorphisms generated by EGDs
	private Homomorphism homomorphismCacheForEGDs;

	// variable to calculate provenance informations
	public boolean calculateProvenance;

	/**
	 * constructor - mainly for the CHASE thread
	 * 
	 * @param instance
	 * @param integrityConstraints
	 */
	public Chase(Instance instance, LinkedHashSet<Constraint> integrityConstraints) {
		this.instance = instance;
		this.integrityConstraints = integrityConstraints;
	}

	/**
	 * The Standard-CHASE which CHASEs an instance with a set of integrity
	 * constraints.
	 *
	 * @param instance             the instance to CHASE
	 * @param integrityConstraints the set of integrity constraints with that the
	 *                             instance {@code I} is CHASEd with
	 * @return instance containing/implements the integrity constraints if the CHASE
	 *         succeed or an empty instance if the CHASE fails (triggered by the
	 *         equating of two different constants)
	 */
	public static Instance chase(Instance instance, LinkedHashSet<Constraint> integrityConstraints) {
		// filter source dependencies
		LinkedHashSet<Constraint> sourceConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.SOURCE)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// filter target dependencies
		LinkedHashSet<Constraint> targetConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.TARGET)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// no scopes given or at least one is null
		// This can be considered "legacy" or "default" mode. There are no source/target
		// constraints.
		if (sourceConstraints.size() == 0 || targetConstraints.size() == 0) {
			return new Chase(instance, integrityConstraints).chase();
		} else {
			// This can be considered "scope mode". There are source constraints that are
			// applied first, and target constraints that applied after all source
			// constraints have been exceeded. The CHASE first runs with all source
			// constraints. The resulting instance is then re-chased with all remaining
			// target constraints.
			Instance sourceResult = new Chase(instance, sourceConstraints).chase();
			Instance targetResult = new Chase(sourceResult, targetConstraints).chase();

			return targetResult;
		}
	}

	public Instance chase() {
		initializeNullCounter();
		initializeProvenanceCounter();

		newFacts = new Instance(instance);

		boolean success = chaseMainLoop();

		if (success) {
			if (instance.getOriginTag().equals(OriginTag.INSTANCE)) {
				ChaseUtil.removeSourceRelationsFrom(instance);
			}
		} else {
			instance = new Instance(instance.getOriginTag());
		}

		return instance;
	}

	/**
	 * Create a HashMap for counting the indexes of new generated Null values. It
	 * has the attributes of instance I as key and the counter starting at 0 as
	 * value.
	 *
	 * @return
	 */
	private void initializeNullCounter() {
		ChateauLogger.log(Level.INFO, "Starting pre-setup");

		nullCounter = new HashMap<>();

		instance.getSchema().values().forEach(attibuteTypeMap -> attibuteTypeMap.keySet()
				.forEach(attributeName -> nullCounter.put(attributeName, 0)));

		ChateauLogger.log(Level.INFO, "Finished pre-setup.");
	}

	/**
	 * Initializes the provenance ID counter used to keep track of provenance
	 * annotation IDs.
	 */
	private void initializeProvenanceCounter() {
		provenanceIDCounter = new HashMap<String, Integer>();
		for (String relation : instance.getSchema().keySet()) {
			provenanceIDCounter.put(relation, 1);
		}
	}

	/**
	 * Main loop of the CHASE method Processes the new instances and the integrity
	 * conditions as long as new facts were generated in the run before and the
	 * thread is not interrupted
	 * 
	 * @return false only if distinct constants are equated, otherwise true
	 */
	private boolean chaseMainLoop() {
		boolean success = true;

		var schema = instance.getSchema();
		OriginTag originTag = instance.getOriginTag();

		// will contain all mappings generated by egd's
		homomorphismCacheForEGDs = new Homomorphism();

		// repeat until no more new facts are generated
		while (!newFacts.getRelationalAtoms().isEmpty() && !Thread.interrupted()) {
			newInstance = new Instance(schema, originTag);

			// the union of Instance and NewFacts
			instanceWithNewFacts = new Instance(instance);

			// check every run if the CHASE has to be aborted
			boolean stepSuccess = processIntegrityConstraints();

			if (!stepSuccess) {
				success = false;
				break;
			}

			updateInstances();
		}

		return success;
	}

	/**
	 * update all caches and apply mappings from homomorphismCacheForEGDs
	 */
	private void updateInstances() {
		// update the union of I and N
		instanceWithNewFacts.getRelationalAtoms().addAll(newInstance.getRelationalAtoms());

		// apply the mappings of the egd's to instanceWithNewFacts
		newFacts = homomorphismCacheForEGDs.applyMappingsTo(instanceWithNewFacts);

		// get rid of the unchanged, old facts from I
		newFacts.getRelationalAtoms().removeAll(instance.getRelationalAtoms());

		// apply the mappings of the egd's to I
		instance = homomorphismCacheForEGDs.applyMappingsTo(instance);

		// update I with the new facts
		instance.getRelationalAtoms().addAll(newFacts.getRelationalAtoms());

		// remove duplicate atoms (with isOrigin and !isOrigin)
		var duplicateOriginNewAtoms = new LinkedHashSet<RelationalAtom>();
		instance.getRelationalAtoms().stream()
				.filter(atom -> atom.isOrigin()
						&& instance.getRelationalAtoms().contains(new RelationalAtom(atom.getName(), atom.getTerms())))
				.forEach(atom -> duplicateOriginNewAtoms.add(new RelationalAtom(atom.getName(), atom.getTerms())));

		instance.getRelationalAtoms().removeAll(duplicateOriginNewAtoms);

		// adjust the cached head of the query from which the instance was created
		if (instance.getOriginTag().equals(OriginTag.QUERY)) {
			var mappedHeadAtoms = new LinkedHashSet<RelationalAtom>();
			instance.getHeadAtoms()
					.forEach(atom -> mappedHeadAtoms.add(homomorphismCacheForEGDs.applyMappingsTo(atom)));
			instance.setHeadAtoms(mappedHeadAtoms);
		}
	}

	/**
	 * generate trigger and execute chase step by active trigger
	 * 
	 * @return false only if distinct constants are equated, otherwise true
	 */
	private boolean processIntegrityConstraints() {
		boolean success = true;

		// filter source dependencies
		LinkedHashSet<Constraint> sourceConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.SOURCE)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// filter target dependencies
		LinkedHashSet<Constraint> targetConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.TARGET)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// no scopes given or at least one is null
		if (sourceConstraints.size() == 0 || targetConstraints.size() == 0) {
			for (Constraint constraint : integrityConstraints) {
				boolean stepSuccess = processIntegrityConstraint(constraint);

				if (!stepSuccess) {
					success = false;
					break;
				}
			}
		} else {
			// process source dependencies first...
			for (Constraint constraint : sourceConstraints) {
				boolean stepSuccess = processIntegrityConstraint(constraint);

				if (!stepSuccess) {
					success = false;
					break;
				}
			}

			// ...then process target dependencies
			for (Constraint constraint : targetConstraints) {
				boolean stepSuccess = processIntegrityConstraint(constraint);

				if (!stepSuccess) {
					success = false;
					break;
				}
			}
		}

		return success;
	}

	/**
	 * Processes a single integrity constraint.
	 * 
	 * @param constraint The {@link Constraint} to process
	 * @return true if operation was successful, else false
	 */
	private boolean processIntegrityConstraint(Constraint constraint) {
		ChateauLogger.log(Level.INFO, "Checking integrity constraint " + constraint.toString());

		// for each constraint of integrityConstraints update the union set of Instance
		// and NewFacts
		// for new generated atoms in NewFacts by tgd's
		instanceWithNewFacts.getRelationalAtoms().addAll(newInstance.getRelationalAtoms());

		// check every trigger if the CHASE has to be aborted
		boolean stepSuccess = processTriggers(constraint);

		return stepSuccess;
	}

	/**
	 * process all triggers for an integrity constraint
	 * 
	 * @param constraint
	 * @param triggers
	 * @return false only if distinct constants are equated, otherwise true
	 */
	private boolean processTriggers(Constraint constraint) {
		int resultCode = 0;

		ExecutionLoop: while (true) {
			// if it stays 0 until the end, no active triggers exist anymore
			int resultCodeSum = 0;

			ChateauLogger.log(Level.INFO, "Recalculating triggers for new instance...");
			Instance oldInstance = homomorphismCacheForEGDs.applyMappingsTo(instance);

			Instance mergedInstance = Instance.merge(oldInstance, newInstance);

			var triggers = ChaseUtil.generateTriggers(mergedInstance, constraint);

			TriggerLoop: for (Homomorphism trigger : triggers) {
				if (ChaseUtil.constraintIntersectsNewFacts(constraint, trigger, newFacts)) {
					resultCode = processTrigger(constraint, trigger);
					resultCodeSum += resultCode;

					if (resultCode == 2) {
						return false;
					} else if (resultCode == 1) {
						break TriggerLoop;
					}
				}
			}

			// no active triggers remaining
			if (resultCodeSum == 0) {
				break ExecutionLoop;
			}
		}

		// 0 or 1: everything okay, 2 or 3: begd error
		return resultCode < 2;
	}

	/**
	 * process one of the triggers for an integrity constraint
	 * 
	 * @param constraint
	 * @param homomorphism
	 * @return 0 - no new tuples, 1 - new tuple, 2 - BEgd problem
	 */
	private int processTrigger(Constraint constraint, Homomorphism homomorphism) {
		Instance mergedInstance = Instance.merge(instanceWithNewFacts, newInstance);

		if (!homomorphism.isTriggerFor(mergedInstance, constraint)) {
			// not a trigger
			return 0;
		}

		ChateauLogger.log(Level.INFO, "Identified trigger " + homomorphism);

		if (homomorphism.isActiveTriggerFor(mergedInstance, constraint)) {
			ChateauLogger.log(Level.INFO, "Identified active Trigger " + homomorphism);

			if (constraint instanceof Tgd) {
				Tgd tgd = (Tgd) constraint;
				applyTgd(tgd, homomorphism);
				return 1; // "new tuple" flag
			} else if (constraint instanceof BEgd) {
				BEgd bEgd = (BEgd) constraint;
				applyBEgd(bEgd, homomorphism);
			} else {
				Egd egd = (Egd) constraint;

				// check for EGD if the CHASE has to be aborted
				if (!applyEgd(egd, homomorphism)) {
					return 2;
				}
			}
		}

		return 0;
	}

	/**
	 * adds for the homomorphism (= active trigger) the head of the TGD adjusted by
	 * the homomorphism to newInstance
	 * 
	 * @param tgd
	 * @param homomorphism
	 */
	private void applyTgd(Tgd tgd, Homomorphism homomorphism) {
		ChateauLogger.log(Level.INFO, "Applying homomorphism to tgd...");

		Homomorphism tempHomomorphism = new Homomorphism(homomorphism);

		for (RelationalAtom relationalHeadAtom : tgd.getHead()) {
			// apply standard mappings
			ChateauLogger.log(Level.INFO, "Applying Mappings: " + homomorphism.getTermMappings());
			addTermMappingsForExistentialVariable(tempHomomorphism, relationalHeadAtom);
			RelationalAtom newAtom = tempHomomorphism.applyMappingsTo(relationalHeadAtom);

			// apply functional mappings / evaluate functions
			newAtom = tempHomomorphism.applyComparisons(tgd, newAtom);

			// add provenance (optional)
			if (calculateProvenance) {
				calculateProvenance(tempHomomorphism, relationalHeadAtom, newAtom);
			} else {
				newInstance.addRelationalAtom(newAtom);
			}

			ChateauLogger.log(Level.INFO, "Added new relational atom: " + newAtom.toString());
		}
	}

	/**
	 * for each mapping, get the provenance id of the atom for which the mapping was
	 * created and save it as why provenance. if the atom is a duplicate, then add
	 * the why provenance to the existing why provenance of the existing duplicate.
	 * 
	 * @param tempHomomorphism
	 * @param relationalHeadAtom
	 * @param newAtom
	 */
	private void calculateProvenance(Homomorphism tempHomomorphism, RelationalAtom relationalHeadAtom,
			RelationalAtom newAtom) {
		String newAtomName = newAtom.getRelationName();
		String provenanceId = newAtomName + "_" + provenanceIDCounter.get(newAtomName);

		ChaseUtil.incrementCounter(provenanceIDCounter, newAtomName);

		// where
		LinkedHashSet<String> whereTupleProvenance = new LinkedHashSet<>();
		LinkedHashSet<String> whereRelationsProvenance = new LinkedHashSet<>();

		// why
		ArrayList<String> whyProvenance = new ArrayList<>();

		for (Term headTerm : relationalHeadAtom.getTerms()) { // each term in tgd head
			for (TermMapping mapping : tempHomomorphism.getTermMappings()) { // each mapping
				// null values do not have an origin atom
				if (mapping.getTargetTermRelationPosition() != null) {
					if (mapping.getSource().equals(headTerm)) {
						int termIndex = relationalHeadAtom.getTerms().indexOf(headTerm);
						if (mapping.getTarget().equals(newAtom.getTerms().get(termIndex))) {
							whereTupleProvenance
									.add(mapping.getTargetTermRelationPosition().getKey().getProvInfo().getId());
							whereRelationsProvenance
									.add(mapping.getTargetTermRelationPosition().getKey().getRelationName());
						}
					}
				}
			}
		}

		// ensures that no id's are added to the why provenance for identical
		// combinations of mapping source and target
		LinkedHashSet<Pair<RelationalAtom, RelationalAtom>> howProvenanceHelper = new LinkedHashSet<>();

		tempHomomorphism.getTermMappings().stream().
		// null values do not have an origin atom
				filter(mapping -> mapping.getTargetTermRelationPosition() != null)
				.forEach(mapping -> howProvenanceHelper
						.add(new Pair<RelationalAtom, RelationalAtom>(mapping.getSourceTermRelationPosition().getKey(),
								mapping.getTargetTermRelationPosition().getKey())));
		howProvenanceHelper.stream().forEach(pair -> whyProvenance.add(pair.getValue().getProvInfo().getId()));

		if (newInstance.getRelationalAtoms().contains(newAtom)) {
			newInstance.getRelationalAtoms().stream().filter(atom -> atom.equals(newAtom)).forEach(atom -> {
				atom.getProvInfo().getWhyProvenance().add(whyProvenance);
				atom.getProvInfo().getWhereTupleProvenance().addAll(whereTupleProvenance);
				atom.getProvInfo().getWhereRelationsProvenance().addAll(whereRelationsProvenance);
			});
		} else {
			ArrayList<ArrayList<String>> provInfoWhyProv = new ArrayList<>();
			provInfoWhyProv.add(whyProvenance);
			ProvenanceInformation provInfo = new ProvenanceInformation(
					provenanceId, whereTupleProvenance, whereRelationsProvenance, provInfoWhyProv);

			newAtom.setProvInfo(provInfo);
			newInstance.addRelationalAtom(newAtom);
		}
	}

	/**
	 * adds mappings from the existential variables to new zero values or new
	 * existential variables
	 * 
	 * @param homomorphism
	 * @param relationalAtom
	 */
	private void addTermMappingsForExistentialVariable(Homomorphism homomorphism, RelationalAtom relationalAtom) {
		ArrayList<String> attributes = instance.getAttributes(relationalAtom.getRelationName());
		for (Term term : relationalAtom.getTerms()) {
			if (term.isVariable()) {
				Variable oldVariable = (Variable) term;

				if (oldVariable.getVariableType() == VariableType.EXISTS) {
					// get the matching attribute for the variable
					String attributeForVariable = attributes.get(relationalAtom.getTerms().indexOf(term));
					// replace in nullCounter the value for an attribute by its by 1
					// incremented value (counter goes up for that attribute)
					updateNullCounter(attributeForVariable);

					switch (instance.getOriginTag()) {
						case INSTANCE:
							addNewNullMappingToHomomorphism(homomorphism, oldVariable, attributeForVariable);
							break;

						case QUERY:
							addNewExistentialVariableMappingToHomomorphism(homomorphism, oldVariable,
									attributeForVariable);
							break;
					}
				}
			}
		}
	}

	/**
	 * update the nullCounter as long as nullCheck fails
	 * 
	 * @param left
	 */
	private void updateNullCounter(String attribute) {
		do {
			ChaseUtil.incrementCounter(nullCounter, attribute);
		} while (!ChaseUtil.nullCheck(instance, nullCounter, attribute));
	}

	/**
	 * adds mappings from the existential variables to new zero values
	 * 
	 * @param homomorphism
	 * @param variable
	 */
	private void addNewNullMappingToHomomorphism(Homomorphism homomorphism, Variable variable, String attribute) {
		// new Null created with the attribute of the variable and the new
		// counter number for the attribute
		Null newNull = ChaseUtil.createNewNull(attribute, nullCounter);
		TermMapping newMapping = ChaseUtil.addTermMapping(variable, newNull, homomorphism);
		String message = String.format("Added new Mapping %s to homomorphism", newMapping.toString());
		ChateauLogger.log(Level.INFO, message);
	}

	/**
	 * adds mappings from the existential variables to new existential variables
	 * 
	 * @param homomorphism
	 * @param variable
	 */
	private void addNewExistentialVariableMappingToHomomorphism(Homomorphism homomorphism, Variable variable,
			String attribute) {
		// new Variable created with the attribute of the variable and the
		// new counter number for the index
		Variable newVariable = ChaseUtil.createNewExistsVariable(attribute, nullCounter);
		TermMapping newMapping = ChaseUtil.addTermMapping(variable, newVariable, homomorphism);
		String message = String.format("Added new Mapping %s to homomorphism", newMapping.toString());
		ChateauLogger.log(Level.INFO, message);
	}

	/**
	 * creates mapping for the EGD head and executes the mapping
	 * 
	 * @param egd
	 * @param homomorphism
	 * @return
	 */
	private boolean applyEgd(Egd egd, Homomorphism homomorphism) {
		boolean success = true;

		ChateauLogger.log(Level.INFO, "Applying homomorphism to egd...");

		for (EqualityAtom equalityHeadAtom : egd.getHead()) {

			// temporary terms that are the result of applying the mappings of homomorphism
			// to the
			// equality atoms of the egd
			Term leftTerm = equalityHeadAtom.getTerm1();
			Term rightTerm = equalityHeadAtom.getTerm2();

			for (TermMapping mapping : homomorphism.getTermMappings()) {
				if (mapping.getSource().equals(leftTerm)) {
					ChateauLogger.log(Level.INFO, "Apply mapping " + mapping.toString());
					leftTerm = mapping.apply(leftTerm);
				}

				if (mapping.getSource().equals(rightTerm)) {
					ChateauLogger.log(Level.INFO, "Apply mapping " + mapping.toString());
					rightTerm = mapping.apply(rightTerm);
				}

			}

			boolean stepSuccess = processEqualityMapping(leftTerm, rightTerm);

			if (!stepSuccess) {
				success = false;
				break;
			}
		}

		return success;
	}

	/**
	 * test EGD conditions and execute it
	 * 
	 * @param leftTerm
	 * @param rightTerm
	 * @return
	 */
	private boolean processEqualityMapping(Term leftTerm, Term rightTerm) {
		boolean success = true;

		// if both terms of the equality atom are Constants the CHASE fails
		// testing for inequality isn't necessary here because of the test
		// 'homomorphism.isActiveFor(instanceWithNewFacts)' before
		// (the terms are considered as unequal already)
		if (ChaseUtil.bothTermTypesAreConst(leftTerm, rightTerm)) {
			if (!ChaseUtil.bothTermValuesAreEqual(leftTerm, rightTerm)) {
				printErrorMessage(leftTerm, rightTerm);
				success = false;
			}
		} else {
			if (ChaseUtil.shouldExecuteMapping(leftTerm, rightTerm)) {
				var maxToMinMapping = ChaseUtil.buildMaxToMinMapping(leftTerm, rightTerm);
				homomorphismCacheForEGDs.getTermMappings().add(maxToMinMapping);
				homomorphismCacheForEGDs = homomorphismCacheForEGDs.composeWith(homomorphismCacheForEGDs);
			}
		}

		instanceWithNewFacts = homomorphismCacheForEGDs.applyMappingsTo(instanceWithNewFacts);

		ChateauLogger.log(Level.INFO, "Done.");
		return success;
	}

	/**
	 * error message when EGD condition is violated
	 * 
	 * @param firstTerm
	 * @param secondTerm
	 */
	private void printErrorMessage(Term firstTerm, Term secondTerm) {
		String originType = instance.getOriginTag().getTypeName();
		String firstTermString = firstTerm.toString();
		String secondTermString = secondTerm.toString();

		String format = "The CHASE stopped because of a violated egd. An empty %s will be returned. "
				+ "The two terms in question are %s and %s.";

		String errorString = String.format(format, originType, firstTermString, secondTermString);

		System.out.println(errorString);

		ChateauLogger.log(Level.WARNING, errorString);
	}

	/**
	 * implements the customized chase step for bEgds for the galve technique
	 * 
	 * @param bEgd
	 * @param homomorphism
	 */
	private void applyBEgd(BEgd bEgd, Homomorphism homomorphism) {

		ChateauLogger.log(Level.INFO, "Applying homomorphism to egd...");

		for (EqualityAtom equalityHeadAtom : bEgd.getHead()) {

			// temporary terms that are the result of applying the mappings of homomorphism
			// to the
			// equality atoms of the egd
			Term leftTerm = equalityHeadAtom.getTerm1();
			Term rightTerm = equalityHeadAtom.getTerm2();

			// keep the applied mappings to be able to read them out afterwards
			var leftAppliedMappings = new LinkedHashSet<TermMapping>();
			var rightAppliedMappings = new LinkedHashSet<TermMapping>();

			for (TermMapping mapping : homomorphism.getTermMappings()) {
				if (mapping.getSource().equals(leftTerm)) {
					ChateauLogger.log(Level.INFO, "Apply mapping " + mapping.toString());
					leftTerm = mapping.apply(leftTerm);
					leftAppliedMappings.add(mapping);
				}

				if (mapping.getSource().equals(rightTerm)) {
					ChateauLogger.log(Level.INFO, "Apply mapping " + mapping.toString());
					rightTerm = mapping.apply(rightTerm);
					rightAppliedMappings.add(mapping);
				}
			}

			// get the atoms that created the mapping
			TermMapping leftAppliedMapping = leftAppliedMappings.stream().findFirst().get();
			TermMapping rightAppliedMapping = rightAppliedMappings.stream().findFirst().get();

			RelationalAtom leftAtom = homomorphismCacheForEGDs
					.applyMappingsTo(leftAppliedMapping.getTargetTermRelationPosition().getKey());
			RelationalAtom rightAtom = homomorphismCacheForEGDs
					.applyMappingsTo(rightAppliedMapping.getTargetTermRelationPosition().getKey());

			Integer leftTermPosition = leftAppliedMapping.getTargetTermRelationPosition().getValue();
			Integer rightTermPosition = rightAppliedMapping.getTargetTermRelationPosition().getValue();

			// if both terms of the equality atom are constants
			if (ChaseUtil.bothTermTypesAreConst(leftTerm, rightTerm)) {

				if (!ChaseUtil.bothTermValuesAreEqual(leftTerm, rightTerm)) {

					// left is origin & right is not origin
					if (leftAtom.isOrigin() && !rightAtom.isOrigin()) {
						// replace constant from new atom with constant from original atom (in instance
						// & instanceWithNewFacts)
						for (RelationalAtom atom : instanceWithNewFacts.getRelationalAtoms()) {
							if (atom.equals(rightAtom)) {
								atom.getTerms().remove(atom.getTerms().get(rightTermPosition));
								atom.getTerms().add(rightTermPosition, leftTerm);
							}
						}

						for (RelationalAtom atom : instance.getRelationalAtoms()) {
							if (atom.equals(rightAtom)) {
								atom.getTerms().remove(atom.getTerms().get(rightTermPosition));
								atom.getTerms().add(rightTermPosition, leftTerm);
							}
						}
					} else if (rightAtom.isOrigin() && !leftAtom.isOrigin()) {
						// right is origin & left is not origin;
						// replace constant from new atom with constant from original atom (in instance
						// & instanceWithNewFacts)
						for (RelationalAtom atom : instanceWithNewFacts.getRelationalAtoms()) {
							if (atom.equals(leftAtom)) {
								atom.getTerms().remove(atom.getTerms().get(leftTermPosition));
								atom.getTerms().add(leftTermPosition, rightTerm);
							}
						}

						for (RelationalAtom atom : instance.getRelationalAtoms()) {
							if (atom.equals(leftAtom)) {
								atom.getTerms().remove(atom.getTerms().get(leftTermPosition));
								atom.getTerms().add(leftTermPosition, rightTerm);
							}
						}
					} else if (!rightAtom.isOrigin() && !leftAtom.isOrigin()) {
						// left is not origin & right is not origin;
						// replace both constants with a new null value (in instance &
						// instanceWithNewFacts)
						var attribute = new ArrayList<String>(
								instanceWithNewFacts.getSchema().get(leftAtom.getRelationName()).keySet())
								.get(leftTermPosition);
						Null newNull = ChaseUtil.createNewNull(attribute, nullCounter);

						for (RelationalAtom atom : instanceWithNewFacts.getRelationalAtoms()) {
							if (atom.equals(leftAtom)) {
								atom.getTerms().remove(atom.getTerms().get(leftTermPosition));
								atom.getTerms().add(leftTermPosition, newNull);
							}
						}

						for (RelationalAtom atom : instanceWithNewFacts.getRelationalAtoms()) {
							if (atom.equals(rightAtom)) {
								atom.getTerms().remove(atom.getTerms().get(rightTermPosition));
								atom.getTerms().add(rightTermPosition, newNull);
							}
						}

						for (RelationalAtom atom : instance.getRelationalAtoms()) {
							if (atom.equals(leftAtom)) {
								atom.getTerms().remove(atom.getTerms().get(leftTermPosition));
								atom.getTerms().add(leftTermPosition, newNull);
							}
						}

						for (RelationalAtom atom : instance.getRelationalAtoms()) {
							if (atom.equals(rightAtom)) {
								atom.getTerms().remove(atom.getTerms().get(rightTermPosition));
								atom.getTerms().add(rightTermPosition, newNull);
							}
						}
					} else {
						// left is origin & right is origin
						printErrorMessage(leftTerm, rightTerm);
					}
				}
			} else {
				if (ChaseUtil.shouldExecuteMapping(leftTerm, rightTerm)) {
					TermMapping maxToMinMapping = ChaseUtil.buildMaxToMinMapping(leftTerm, rightTerm);

					// give the possibility to replace single constants from new tuples
					// with other constants from original tuples with bEgds
					if (leftTerm.equals(maxToMinMapping.getSource())) {
						maxToMinMapping.setSourceTermRelationPosition(new Pair<>(leftAtom, leftTermPosition));
						maxToMinMapping.setTargetTermRelationPosition(new Pair<>(rightAtom, rightTermPosition));
					} else {
						maxToMinMapping.setSourceTermRelationPosition(new Pair<>(rightAtom, rightTermPosition));
						maxToMinMapping.setTargetTermRelationPosition(new Pair<>(leftAtom, leftTermPosition));
					}

					// mapping from null to constant
					if (maxToMinMapping.getSource().isNull()
							&& maxToMinMapping.getTarget().isConstant()) {
						Term mappingNull = maxToMinMapping.getSource();
						Term mappingConstant = maxToMinMapping.getTarget();

						// homomorphismCacheForEGDs already has a mapping from the null value to a
						// constant from an original tuple
						if (homomorphismCacheForEGDs.getTermMappings().stream()
								.anyMatch(mapping -> mapping.getSource().equals(mappingNull)
										&& mapping.getTarget().isConstant()
										&& mapping.getTargetTermRelationPosition().getKey().isOrigin())) {
						} else if (homomorphismCacheForEGDs.getTermMappings().stream()
								.anyMatch(mapping -> mapping.getSource().equals(mappingNull)
										&& mapping.getTarget().isConstant()
										&& !mapping.getTargetTermRelationPosition().getKey().isOrigin())) {
							// homomorphismCacheForEGDs already has a mapping from the null value to a
							// constant from a new tuple
							var mappingToRemove = new LinkedHashSet<TermMapping>();
							homomorphismCacheForEGDs.getTermMappings().stream()
									.filter(mapping -> mapping.getSource().equals(mappingNull)
											&& mapping.getTarget().isConstant()
											&& !mapping.getTargetTermRelationPosition().getKey().isOrigin())
									.forEach(mapping -> mappingToRemove.add(mapping));

							// new mapping maps to a constant from an original tuple
							// left term is the constant and left atom is original or right term is the
							// constant and right atom is original
							if (leftTerm.isConstant() && leftAtom.isOrigin()
									|| rightTerm.isConstant() && rightAtom.isOrigin()) {
								// replaced the previous mapping
								homomorphismCacheForEGDs.getTermMappings().removeAll(mappingToRemove);
								homomorphismCacheForEGDs.addMapping(maxToMinMapping);
							} else if (leftTerm.isConstant() && !leftAtom.isOrigin()
									&& !leftTerm.equals(mappingConstant)
									// new mapping maps to ANOTHER constant from a new tuple
									|| rightTerm.isConstant() && !rightAtom.isOrigin()
											&& !rightTerm.equals(mappingConstant)) {
								// delete the previous mapping without replacement
								homomorphismCacheForEGDs.getTermMappings().removeAll(mappingToRemove);
							}

							// such a mapping does not yet exist
						} else {
							homomorphismCacheForEGDs.addMapping(maxToMinMapping);
							homomorphismCacheForEGDs = homomorphismCacheForEGDs.composeWith(homomorphismCacheForEGDs);
						}
					}
					// mapping from null to null
					else {
						homomorphismCacheForEGDs.addMapping(maxToMinMapping);
						homomorphismCacheForEGDs = homomorphismCacheForEGDs.composeWith(homomorphismCacheForEGDs);
					}
				}
			}

			instanceWithNewFacts = homomorphismCacheForEGDs.applyMappingsTo(instanceWithNewFacts);

			ChateauLogger.log(Level.INFO, "Done.");
		}
	}
}
