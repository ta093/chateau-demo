package chase;

import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import constraints.Constraint;
import constraints.ScopeType;
import instance.Instance;

public class ChaseThread extends Thread {
	private Instance instance;
	private LinkedHashSet<Constraint> integrityConstraints;
	private Instance output;
	private boolean calculateProvenance;

	public ChaseThread(Instance instance, LinkedHashSet<Constraint> integrityConstraints,
			boolean calculateProvenance) {
		this.instance = instance;
		this.integrityConstraints = integrityConstraints;
		this.calculateProvenance = calculateProvenance;
	}

	@Override
	public void run() {
		// filter source dependencies
		LinkedHashSet<Constraint> sourceConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.SOURCE)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// filter target dependencies
		LinkedHashSet<Constraint> targetConstraints = integrityConstraints
				.stream()
				.filter(constraint -> constraint.getScopeType() == ScopeType.TARGET)
				.collect(Collectors.toCollection(LinkedHashSet::new));

		// no scopes given or at least one is null
		// This can be considered "legacy" or "default" mode. There are no source/target
		// constraints.
		if (sourceConstraints.size() == 0 || targetConstraints.size() == 0) {
			Chase standardChase = new Chase(instance, integrityConstraints);
			standardChase.calculateProvenance = calculateProvenance;
			output = standardChase.chase();
		} else {
			// This can be considered "scope mode". There are source constraints that are
			// applied first, and target constraints that applied after all source
			// constraints have been exceeded. The CHASE first runs with all source
			// constraints. The resulting instance is then re-chased with all remaining
			// target constraints.
			Chase sourceChase = new Chase(instance, sourceConstraints);
			sourceChase.calculateProvenance = calculateProvenance;

			Chase targetChase = new Chase(sourceChase.chase(), targetConstraints);
			targetChase.calculateProvenance = calculateProvenance;

			output = targetChase.chase();
		}
	}

	public Instance getOutput() {
		return output;
	}
}
