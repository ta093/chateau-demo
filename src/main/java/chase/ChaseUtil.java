package chase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;

import atom.RelationalAtom;
import comparisons.Comparison;
import constraints.Constraint;
import constraints.Tgd;
import functions.Function;
import homomorphism.Homomorphism;
import homomorphism.TermMapping;
import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import io.ChateauLogger;
import term.ConstType;
import term.Constant;
import term.Null;
import term.Term;
import term.Variable;
import term.VariableType;

final class ChaseUtil {

	private ChaseUtil() {
	}

	static Variable createNewExistsVariable(String attribte, Map<String, Integer> nullCounter) {
		Integer index = nullCounter.get(attribte);

		return new Variable(VariableType.EXISTS, attribte, index);
	}

	static Null createNewNull(String attribte, Map<String, Integer> nullCounter) {
		Integer index = nullCounter.get(attribte);

		return new Null(attribte, index);
	}

	/**
	 * test EGD conditions (excluding constants)
	 *
	 * @param firstTerm
	 * @param secondTerm
	 * @return
	 */
	static boolean shouldExecuteMapping(Term firstTerm, Term secondTerm) {
		boolean executeMapping = false;

		if (ChaseUtil.bothTermTypesAreVariable(firstTerm, secondTerm)) {
			Variable eq1Variable = (Variable) firstTerm;
			Variable eq2Variable = (Variable) secondTerm;

			if (ChaseUtil.anyVariableTypeIsExists(eq1Variable, eq2Variable)) {
				executeMapping = true;
			}
		} else {
			executeMapping = true;
		}

		return executeMapping;
	}

	/**
	 * create mapping from the term with the smaller rank to the term with the
	 * larger rank if the rank is equal, create mapping according to homomorphism
	 * rules constant = 0, zero = 1, variable = 2
	 *
	 * @param firstTerm
	 * @param secondTerm
	 * @return
	 */
	static TermMapping buildMaxToMinMapping(Term firstTerm, Term secondTerm) {
		Term smallerTerm = ChaseUtil.getSmallerTerm(firstTerm, secondTerm);
		Term biggerTerm = ChaseUtil.getBiggerTerm(firstTerm, secondTerm);

		return new TermMapping(biggerTerm, smallerTerm);
	}

	static boolean anyVariableTypeIsExists(Variable firstVariable, Variable secondVariable) {
		return firstVariable.getVariableType() == VariableType.EXISTS
				|| secondVariable.getVariableType() == VariableType.EXISTS;
	}

	static boolean bothTermTypesAreVariable(Term firstTerm, Term secondTerm) {
		return firstTerm.isVariable() && secondTerm.isVariable();
	}

	static boolean bothTermValuesAreEqual(Term firstTerm, Term secondTerm) {
		return firstTerm.equals(secondTerm);
	}

	static boolean bothTermTypesAreConst(Term firstTerm, Term secondTerm) {
		return firstTerm.isConstant() && secondTerm.isConstant();
	}

	static void incrementCounter(Map<String, Integer> counter, String key) {
		Integer currentValue = counter.get(key);
		counter.replace(key, currentValue + 1);
	}

	/**
	 * checks if trigger uses the new facts, otherwise it is not active, saves
	 * active trigger, check for triggers that do not meet the condition
	 *
	 * @param constraint
	 * @param homomorphism
	 * @param newFacts
	 * @return
	 */
	static boolean constraintIntersectsNewFacts(Constraint constraint, Homomorphism homomorphism,
			Instance newFacts) {
		// flag for the intersection of constraint.getBody() and newFacts not being
		// empty
		// true, if at least one of the RelationalAtoms of constraint.getBody() has a
		// homomorphism to newFacts with trigger homomorphism
		boolean constraintIntersectsNewFacts = false;

		// for every constraintAtom in contraint.getBody() ...
		BodyAtomsLoop: for (RelationalAtom bodyAtom : constraint.getBody()) {
			// ... apply the mappings of homomorphism to constraintAtom ...
			// temporary atom to not alter constraintAtom
			RelationalAtom mappedBodyAtom = new RelationalAtom(bodyAtom);

			// apply all mappings of homomorphism to the temporary atom
			for (TermMapping mapping : homomorphism.getTermMappings()) {
				mappedBodyAtom = mapping.apply(mappedBodyAtom);
			}

			// ... and for every instanceAtom in newFacts ...
			InstanceAtomsLoop: for (RelationalAtom relaionalInstanceAtom : newFacts.getRelationalAtoms()) {
				// ... test if the mapped constraintAtom equals the instance atom
				// flag becomes true and we can break out of the loop because we need only one
				// satisfied instance atom in total
				if (mappedBodyAtom.equals(relaionalInstanceAtom)) {
					constraintIntersectsNewFacts = true;
					break InstanceAtomsLoop;
				}
			}

			// break out of the outer loop because we don't need further iterations if the
			// flag is true
			if (constraintIntersectsNewFacts) {
				break BodyAtomsLoop;
			}
		}

		return constraintIntersectsNewFacts;
	}

	/**
	 * remove source relations from final instance (to output s-t tgds correctly)
	 */
	static void removeSourceRelationsFrom(Instance instance) {
		var schemaTags = instance.getSchemaTags();

		if (schemaTags != null && !schemaTags.isEmpty()) {
			Instance instanceCopy = new Instance(instance);

			instanceCopy.getRelationalAtoms().stream()
					.filter(relationalAtom -> schemaTags.get(relationalAtom.getName()) != SchemaTag.TARGET)
					.forEach(relationalAtom -> instance.removeRelationalAtom(relationalAtom));
		}
	}

	/**
	 * Adds a new term mapping consisting of 2 variables to a homomorphism.
	 *
	 * @param oldVariable  The old (subsumed) variable
	 * @param newVariable  The new (subsuming) variable
	 * @param homomorphism The homomorphism the subsumtion is applied to
	 * @return The new {@link TermMapping}
	 */
	static TermMapping addTermMapping(Variable oldVariable, Variable newVariable, Homomorphism homomorphism) {
		Variable sourceTerm = oldVariable.copy();
		Variable targetTerm = newVariable.copy();

		TermMapping newMapping = new TermMapping(sourceTerm, targetTerm);

		homomorphism.addMapping(newMapping);

		return newMapping;
	}

	/**
	 * Adds a new term mapping consisting of a variable and a null value to a
	 * homomorphism.
	 *
	 * @param oldVariable  The old (subsumed) variable
	 * @param newVariable  The new (subsuming) null value
	 * @param homomorphism The homomorphism the subsumtion is applied to
	 * @return The new {@link TermMapping}
	 */
	static TermMapping addTermMapping(Variable oldVariable, Null newNull, Homomorphism homomorphism) {
		Variable sourceTerm = oldVariable.copy();
		Null targetTerm = newNull.copy();

		TermMapping newMapping = new TermMapping(sourceTerm, targetTerm);

		homomorphism.addMapping(newMapping);

		return newMapping;
	}

	/**
	 * Applies comparisons to triggers.
	 * 
	 * @param triggers    A set of triggers ({@link Homomorphism}s)
	 * @param comparisons A set of {@link Comparison}s
	 */
	private static void compareTriggers(LinkedHashSet<Homomorphism> triggers, Constraint constraint) {
		LinkedHashSet<Homomorphism> triggersToRemove = new LinkedHashSet<>();

		// each trigger
		for (Homomorphism trigger : triggers) {
			LinkedHashSet<TermMapping> mappings = trigger.getTermMappings();

			// each mapping
			for (TermMapping leftMapping : mappings) {
				// null validation
				if (leftMapping.getSource().getName() == null)
					continue;

				if (((Tgd) constraint).getBodyComparisons() != null) {
					for (Comparison<?> comparison : ((Tgd) constraint).getBodyComparisons()) {
						// variable check
						if (leftMapping.getSource().equals(comparison.getLeftSide())) {
							switch (comparison.getType()) {
								case CONSTANT:
									Constant mappingTarget = (Constant) leftMapping.getTarget();
									if (mappingTarget.getConstType() == ConstType.STRING) {
										throw new UnsupportedOperationException(
												"Comparisons with strings are not supported");
									}

									double mappingValue = Double.parseDouble(mappingTarget.getValue().toString());

									// actual comparison
									if (!comparison.compare(mappingValue)) {
										ChateauLogger.log(
												Level.INFO, "Removing trigger " + trigger + " because comparison "
														+ comparison.toString()
														+ " isn't satisfied.");

										triggersToRemove.add(trigger);
									}

									break;

								case VARIABLE:
									Variable rightSide = (Variable) comparison.getRightSide();

									for (TermMapping rightMapping : mappings) {
										if (rightMapping.getSource().equals(rightSide)) {
											Constant leftTarget = (Constant) leftMapping.getTarget();
											Constant rightTarget = (Constant) rightMapping.getTarget();

											if (leftTarget.getConstType() == ConstType.STRING
													|| rightTarget.getConstType() == ConstType.STRING) {
												throw new UnsupportedOperationException(
														"Comparisons with strings are not supported");
											}

											double leftValue = Double.parseDouble(leftTarget.getValue().toString());
											double rightValue = Double.parseDouble(rightTarget.getValue().toString());

											// actual comparison
											if (!comparison.compare(leftValue, rightValue)) {
												ChateauLogger.log(
														Level.INFO,
														"Removing trigger " + trigger + " because comparison "
																+ comparison.toString()
																+ " isn't satisfied.");

												triggersToRemove.add(trigger);
											}
										}
									}

									break;

								case FUNCTION:
									Function<?> function = (Function<?>) comparison.getRightSide();

									Constant compConstant = (Constant) leftMapping.getTarget();
									if (compConstant.getConstType() == ConstType.STRING) {
										throw new UnsupportedOperationException(
												"Comparisons with strings are not supported");
									}

									double leftDouble = Double.parseDouble(compConstant.getValue().toString());

									Constant functionResult = function.toConstant(mappings);
									double result = Double.parseDouble(functionResult.getValue().toString());

									// actual comparison
									if (!comparison.compare(leftDouble, result)) {
										ChateauLogger.log(
												Level.INFO, "Removing trigger " + trigger + " because comparison "
														+ comparison.toString()
														+ " isn't satisfied.");

										triggersToRemove.add(trigger);
									}

									break;

								default:
									break;
							}
						}
					}
				}
			}
		}

		// remove triggers that don't satisfy comp. constraint
		for (Homomorphism trigger : triggersToRemove) {
			triggers.remove(trigger);
		}
	}

	/**
	 * Generates all homomorphisms that are triggers for the given instance and
	 * integrity constraint.
	 *
	 * @param instance   the instance to that the integrity constraints are mapped
	 *                   to by the triggers
	 * @param constraint the integrity constraint that are mapped to the instance by
	 *                   the triggers
	 * @return all possible triggers for {@code instance} and {@code constraint}
	 */
	static LinkedHashSet<Homomorphism> generateTriggers(Instance instance, Constraint constraint) {
		LinkedHashSet<Homomorphism> triggers = new LinkedHashSet<>();

		// necessary subschema defined by the atoms of the integrity constraints
		// saves searching through the entire instance
		LinkedHashSet<String> constraintSchemas = new LinkedHashSet<>();

		constraint.getBody().forEach(constraintAtom -> constraintSchemas.add(constraintAtom.getName()));

		// get the instance atoms filtered by the schemas appearing in the constraint
		// body
		LinkedHashSet<RelationalAtom> instanceAtoms = instance.getRelationalAtomsBySchema(constraintSchemas);

		ChaseUtil.generateTriggersRec(instanceAtoms, constraint.getBody(), new Homomorphism(), triggers);

		// remove triggers that don't satisfy comparisons
		if (constraint instanceof Tgd) {
			compareTriggers(triggers, constraint);
		}

		// System.out.println("Generated triggers:");
		// System.out.println(triggers);

		return triggers;
	}

	/**
	 * This is the recursive function part of {@link generateTriggers
	 * generateTriggers}. Every recursive call removes one atom from the constraint
	 * body and generates mappings for that chosen atom to the object atoms. For
	 * every object atom it is tested if a generation of mappings from the chosen
	 * constraint atom is possible via
	 * {@link homomorphism.Homomorphism#applyMappingsTo(RelationalAtom)} and
	 * {@link atom.RelationalAtom#hasHomomorphismTo(RelationalAtom)}. If that's the
	 * case, the current homomorphism gets copied for the next recursion step and
	 * the mappings are generated and added to that copy. The next recursion call
	 * will have the same object atoms, the current constraint atom removed from the
	 * constraint body, the copied homomorphism instance and the trigger list, that
	 * gets changed by adding the homomorphism at the beginning of a recursive call
	 * when all constraint atoms are removed, i. e. mappings have been generated
	 * from every constraint atom to one corresponding set of object atoms.
	 *
	 * @param objectAtoms     the target relational atoms for that triggers have to
	 *                        be generated with given {@code constraintAtoms}
	 * @param constraintAtoms the source relational atoms for that triggers have to
	 *                        be generated with given {@code objectAtoms}
	 * @param homomorphism    the current trigger to be
	 * @param triggers        the trigger set that will contain all the generated
	 *                        triggers
	 */
	static void generateTriggersRec(LinkedHashSet<RelationalAtom> objectAtoms,
			LinkedHashSet<RelationalAtom> constraintAtoms,
			Homomorphism homomorphism, LinkedHashSet<Homomorphism> triggers) {
		// recursion termination
		// all constraint atoms have been removed from the constraint body
		if (constraintAtoms.isEmpty()) {
			triggers.add(homomorphism);
			return;
		}

		// get one atom of the constraint body
		RelationalAtom constraintAtom = constraintAtoms.iterator().next();

		// iterate over all instance atoms
		MainLoop: for (RelationalAtom objectAtom : objectAtoms) {
			// test hasHomomorphismTo only if applyMappingsTo maps the constraintAtom,
			// otherwise the variables from the constraints are also checked and
			// e.g. an existence quantified variable from the constraints is not mapped to a
			// null value
			boolean hasHomomorphismTo = hasHomomorphismToForGenerateTrigger(homomorphism, constraintAtom, objectAtom);

			// homomorphism could and should be formed (constraint is not negated)
			if (hasHomomorphismTo) {
				boolean isValidMapping = true;
				ArrayList<Term> sourceTerms = constraintAtom.getTerms();
				ArrayList<Term> targetTerms = objectAtom.getTerms();

				// trigger wants to map a constant to another different constant --> stop
				for (int i = 0; i < sourceTerms.size(); i++) {
					if (sourceTerms.get(i).isConstant() && targetTerms.get(i).isConstant()
							&& !sourceTerms.get(i).equals(targetTerms.get(i))) {
						isValidMapping = false;
					}

				}
				// Terms may only be mapped once in a trigger
				// for the current constraint atom try to find a homomorphism to the current
				// object atom while applying the mappings generated so far for the current
				// trigger
				for (TermMapping termMapping : homomorphism.getTermMappings()) {
					for (int i = 0; i < sourceTerms.size(); i++) {
						if (termMapping.getSource().equals(sourceTerms.get(i))) {
							if (!termMapping.getTarget().equals(targetTerms.get(i))) {
								isValidMapping = false;
							}
						}
					}
				}

				if (isValidMapping) {
					// create a copy of the current trigger so that it doesn't get changed when the
					// next constraint atom is chosen for mappings generation
					Homomorphism hNew = new Homomorphism(homomorphism);

					// abort the recursion if two different mappings with the same source terms
					// and different target terms are to be applied
					if (!hNew.generateMappingsFor(homomorphism.applyMappingsTo(constraintAtom), objectAtom)) {
						continue MainLoop;
					}

					// remove the current constraint atom from the constraint body
					LinkedHashSet<RelationalAtom> bBodyTail = new LinkedHashSet<RelationalAtom>(constraintAtoms);
					bBodyTail.remove(constraintAtom);

					// recursive call with the same objectAtoms, constraint body with the current
					// atom removed, the copied trigger and the trigger list from the beginning
					generateTriggersRec(objectAtoms, bBodyTail, hNew, triggers);
				}
			} else {
				// homomorphism could not and should not be formed (constraint is negated)
				// create a copy of the current trigger so that it doesn't get changed when the
				// next constraint atom is chosen for mappings generation
				Homomorphism hNew = new Homomorphism(homomorphism);

				// remove the current constraint atom from the constraint body
				LinkedHashSet<RelationalAtom> bBodyTail = new LinkedHashSet<RelationalAtom>(constraintAtoms);
				bBodyTail.remove(constraintAtom);

				if (objectAtoms.stream().anyMatch(innerObjectAtom -> hasHomomorphismToForGenerateTrigger(homomorphism,
						constraintAtom, innerObjectAtom))) {
					continue MainLoop;
				}
				// recursive call with the same objectAtoms, constraint body with the current
				// atom removed, the copied trigger and the trigger list from the beginning
				generateTriggersRec(objectAtoms, bBodyTail, hNew, triggers);
			}
		}
	}

	/**
	 * test hasHomomorphismTo only if applyMappingsTo maps the constraintAtom,
	 * otherwise the variables from the constraints are also checked and
	 * e.g. an existence quantified variable from the constraints is not mapped to a
	 * null value
	 * 
	 * @param homomorphism
	 * @param constraintAtom
	 * @param objectAtom
	 * @return
	 */
	private static boolean hasHomomorphismToForGenerateTrigger(Homomorphism homomorphism, RelationalAtom constraintAtom,
			RelationalAtom objectAtom) {
		boolean hasHomomorphismTo = false;

		// check if relation names and size/length match
		if (constraintAtom.getRelationName().equals(objectAtom.getRelationName())
				&& constraintAtom.getTerms().size() == objectAtom.getTerms().size()) {
			hasHomomorphismTo = true;

			TermMappingLoop: for (TermMapping termMapping : homomorphism.getTermMappings()) {
				for (int i = 0; i < constraintAtom.getTerms().size(); i++) {
					// get constraint term and object term at position i
					Term constraintTerm = constraintAtom.getTerms().get(i);
					Term objectTerm = objectAtom.getTerms().get(i);

					if (termMapping.getSource().equals(constraintTerm)) {
						hasHomomorphismTo = termMapping.apply(constraintTerm).hasHomomorphismTo(objectTerm);

						if (!hasHomomorphismTo) {
							break TermMappingLoop;
						}
					}

				}
			}
		}

		return hasHomomorphismTo;
	}

	/**
	 * smaller term is the one with the smaller rank, or if the rank is equal, the
	 * one that is mapped, according to the homomophism rule. constant = 0, zero =
	 * 1, variable = 2
	 *
	 * @param term1
	 * @param term2
	 * @return
	 */
	static Term getSmallerTerm(Term term1, Term term2) {
		if (term1.getRank() < term2.getRank()) {
			return term1;
		} else if (term2.getRank() < term1.getRank()) {
			return term2;
		} else {
			if (term1.isNull() && term2.isNull()) {
				Null null1 = (Null) term1;
				Null null2 = (Null) term2;
				return null1.getIndex() < null2.getIndex() ? term1 : term2;
			} else {
				Variable variable1 = (Variable) term1;
				Variable variable2 = (Variable) term2;

				if (variable1.getVariableType() == VariableType.FOR_ALL
						&& variable2.getVariableType() == VariableType.EXISTS) {
					return term1;
				} else if (variable1.getVariableType() == VariableType.EXISTS
						&& variable2.getVariableType() == VariableType.FOR_ALL) {
					return term2;
				} else {
					return variable1.getIndex() < variable2.getIndex() ? term1 : term2;
				}
			}
		}
	}

	/**
	 * bigger term is the one with the bigger rank, or if the rank is equal, the one
	 * that is mapped to, according to the homomophism rules constant = 0, zero = 1,
	 * variable = 2
	 *
	 * @param term1
	 * @param term2
	 * @return
	 */
	static Term getBiggerTerm(Term term1, Term term2) {
		if (term1.getRank() < term2.getRank()) {
			return term2;
		} else if (term2.getRank() < term1.getRank()) {
			return term1;
		} else {
			if (term1.isNull() && term2.isNull()) {
				Null null1 = (Null) term1;
				Null null2 = (Null) term2;

				return null1.getIndex() < null2.getIndex() ? term2 : term1;
			} else {
				Variable variable1 = (Variable) term1;
				Variable variable2 = (Variable) term2;

				if (variable1.getVariableType() == VariableType.FOR_ALL
						&& variable2.getVariableType() == VariableType.EXISTS) {
					return term2;
				} else if (variable1.getVariableType() == VariableType.EXISTS
						&& variable2.getVariableType() == VariableType.FOR_ALL) {
					return term1;
				} else {
					return variable1.getIndex() < variable2.getIndex() ? term2 : term1;
				}
			}
		}
	}

	/**
	 * Checks whether the new null value or #E already exists within the instance
	 *
	 * @param instance
	 * @param nullCounter
	 * @return
	 */
	static boolean nullCheck(Instance instance, HashMap<String, Integer> nullCounter, String attribute) {
		int index = nullCounter.get(attribute);

		// relation together with the position of the attribute in the relation
		var relationAttPosition = new HashMap<String, Integer>();

		instance.getSchema().forEach((relationName, attributeTypeMap) -> {
			var attributes = new ArrayList<String>(attributeTypeMap.keySet());
			attributes.forEach(attributeInSchema -> {
				if (attributeInSchema.equals(attribute)) {
					relationAttPosition.put(relationName, attributes.indexOf(attributeInSchema));
				}
			});
		});

		for (var pairRelationAttPos : relationAttPosition.entrySet()) {
			var relationalAtoms = instance.getRelationalAtomsBySchema(pairRelationAttPos.getKey());

			for (RelationalAtom relationalAtom : relationalAtoms) {
				Integer attrPosIndex = pairRelationAttPos.getValue();
				Term term = relationalAtom.getTerms().get(attrPosIndex);

				if (instance.getOriginTag() == OriginTag.INSTANCE) {
					if (term.isNull()) {
						var nullTerm = (Null) term;

						if (nullTerm.getIndex() == index) {
							return false;
						}
					}
				} else {
					if (term.isVariable()) {
						Variable variable = (Variable) term;

						if (variable.getVariableType() == VariableType.EXISTS) {
							if (variable.getIndex() == index) {
								return false;
							}
						}
					}
				}
			}
		}

		return true;
	}

}
