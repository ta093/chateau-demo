package term;

import termination.Adornment;

/**
 * The class defines terms that can either be Variables, Null values or
 * Constants. In the case of Constants a term is represented by a basic data
 * type. Variables and Nulls are represented and output by strings following the
 * pattern
 * <ul>
 * <li>{@code #V_indexName_index} or {@code #E_indexName_index} for
 * Variables</li>
 * <li>{@code #N_indexName_index} for Nulls.</li>
 * </ul>
 * The underscores are included in the strings. The {@code indexName} represents
 * the name of the attribute for that the Variable or Null stands for. The
 * {@code index} is the number of the corresponding Variable or Null.
 * 
 * @author Martin Jurklies
 */

public abstract class Term {

	/**
	 * adornments of terms and atoms are used for the constraint-rewriting
	 * termination check
	 */
	protected Adornment adornment = new Adornment();

	public Term() {}

	public Term(Adornment adornment) {
		this.adornment = adornment;
	}

	
	public boolean isNull() {
		return false;
	}

	public boolean isVariable() {
		return false;
	}

	public boolean isConstant() {
		return false;
	}

	public abstract Term copy();

	public abstract int getRank();

	public abstract String getName();
	
	/**
	 * checks if a homomorphism can be created from a term to another term (according to homomorphism rules)
	 * @param other
	 * @return
	 */
	public abstract boolean hasHomomorphismTo(Term other);

	@Override
	public abstract boolean equals(Object other);

	@Override
	public abstract int hashCode();

	@Override
	public abstract String toString();

	/**
	 * @return the Adornment
	 */
	public Adornment getAdornment() {
		return adornment;
	}


	/**
	 * @param adornment the Adornment to set
	 */
	public void setAdornment(Adornment adornment) {
		this.adornment = adornment;
	}

}
