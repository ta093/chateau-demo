package term;

import java.util.Objects;

import termination.Adornment;

/**
 * The class represents Null values in database instances. It consists of a name
 * of the Null value and an index which indicates how many Null values with that
 * name are used so far.
 * 
 * @author Martin Jurklies
 */
public class Null extends Term {
	
	private String name;
	
	private int index;

	/**
	 * constructor
	 */
	public Null(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public Null(String name, int index, Adornment adornment) {
		super(adornment);
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return this.name;
	}
	
	@Override
	public final boolean isNull() {
		return true;
	}

	@Override
	public Null copy() {
		return new Null(name, index, adornment);
	}

	@Override
	public int getRank() {
		return 1;
	}

	@Override
	public boolean hasHomomorphismTo(Term other) {
		return !other.isVariable();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		
		if (other instanceof Null) {
			var that = (Null) other;
			
			return Objects.equals(this.name, that.name) && this.index == that.index;
		}
		
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + index;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {		
		return "#N_" + name + "_" + index;
	}

	/**
	 * @return the indexName
	 */
	public String getIndexName() {
		return this.name;
	}

	/**
	 * @param indexName the indexName to set
	 */
	public void setIndexName(String indexName) {
		this.name = indexName;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return this.index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
