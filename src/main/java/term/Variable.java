package term;

import java.util.Objects;

import termination.Adornment;

/**
 * The class represents Variables in integrity constraints and queries. It
 * consists of a Variable-type which indicates whether the Variable is given or
 * existentially quantified, a name of the Variable and an index which indicates
 * how many Variables with that type and name are used so far.
 * 
 * @author Martin Jurklies
 */
public class Variable extends Term {

	private VariableType variableType;

	private String name;

	private int index;

	/**
	 * constructor
	 */
	public Variable(VariableType variableType, String name, int index) {
		this.variableType = Objects.requireNonNull(variableType);
		this.name = name;
		this.index = index;
	}

	public Variable(VariableType variableType, String name, int index, Adornment adornment) {
		super(adornment);
		this.variableType = Objects.requireNonNull(variableType);
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public final boolean isVariable() {
		return true;
	}

	@Override
	public Variable copy() {
		return new Variable(variableType, name, index, adornment);
	}

	@Override
	public int getRank() {
		return 2;
	}

	@Override
	public boolean hasHomomorphismTo(Term other) {
		// "this" is an existential quantified Variable (#E)
		if (this.variableType == VariableType.EXISTS) {
			return !other.isNull();
		} else {
			// "other" is a Variable
			if (other.isVariable()) {
				return this.equals(other);
			}

			// "other" is a Null
			else if (other.isNull()) {
				return false;
			}

			// "other" is a Constant
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + index;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((variableType == null) ? 0 : variableType.hashCode());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (other instanceof Variable) {
			Variable that = (Variable) other;

			return this.variableType == that.variableType && Objects.equals(this.name, that.name)
					&& this.index == that.index;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "#" + variableType.getToken() + "_" + name + "_" + index;
	}

	/**
	 * @return the variableType
	 */
	public VariableType getVariableType() {
		return this.variableType;
	}

	/**
	 * @return the indexName
	 */
	public String getIndexName() {
		return this.name;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return this.index;
	}

}
