package term;

public enum VariableType {

	FOR_ALL("V", "for all"),

	EXISTS("E", "existential quantified");

	private final String token;

	private final String description;

	private VariableType(String token, String type) {
		this.token = token;
		this.description = type;
	}

	@Override
	public String toString() {
		return token;
	}

	public String getToken() {
		return token;
	}

	public String getDescription() {
		return description;
	}
}
