package term;

import java.util.Objects;

import termination.Adornment;

public class Constant extends Term {
	private ConstType constType;
	private String name;
	private Object value;

	private Constant(ConstType constType, String name, Object value) {
		this.constType = constType;
		this.name = name;
		this.value = value;
	}

	private Constant(ConstType constType, String name, Object value, Adornment adornment) {
		super(adornment);
		this.constType = constType;
		this.name = name;
		this.value = value;
	}

	public static Constant fromInteger(String name, int value) {
		return new Constant(ConstType.INT, name, value);
	}

	public static Constant fromDouble(String name, double value) {
		return new Constant(ConstType.DOUBLE, name, value);
	}

	public static Constant fromString(String name, String value) {
		return new Constant(ConstType.STRING, name, value);
	}

	// Add more to support additional data types

	@Override
	public final boolean isConstant() {
		return true;
	}

	@Override
	public Constant copy() {
		return new Constant(constType, name, value, adornment);
	}

	@Override
	public int getRank() {
		return 0;
	}

	@Override
	public boolean hasHomomorphismTo(Term other) {
		return this.equals(other);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (other instanceof Constant) {
			var that = (Constant) other;

			return this.constType == that.constType && Objects.equals(this.value, that.value);
		}

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + (constType == null ? 0 : constType.hashCode());
		result = prime * result + (value == null ? 0 : value.hashCode());

		return result;
	}

	@Override
	public String toString() {
		if (constType == ConstType.STRING) {
			return "\"" + value.toString() + "\"";
		}

		return value.toString();
	}

	public ConstType getConstType() {
		return constType;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object newValue) {
		this.value = newValue;
	}

	public String getName() {
		return name;
	}
}
