package term;

public enum ConstType {
	
	INT("int"),
	
	DOUBLE("double"),
	
	STRING("string");

	private final String typeName;

	private ConstType(String type) {
		this.typeName = type;
	}

	public String getTypeName() {
		return typeName;
	}
}
