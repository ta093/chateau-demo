package comparisons;

import term.Variable;

public abstract class Comparison<Any> {

    private final Variable leftSide;
    private final Any rightSide;
    private final ComparisonType type;

    protected Comparison(ComparisonType type, Variable leftSide, Any rightSide) {
        this.leftSide = leftSide;
        this.rightSide = rightSide;
        this.type = type;
    }
    
    public Variable getLeftSide() {
        return leftSide;
    }

    /**
     * Gets the value to compare to.
     * 
     * @return The comparison value
     */
    public Any getRightSide() {
        return rightSide;
    }

    public ComparisonType getType() {
        return type;
    }

    /**
     * Method that actually compares values. Has to be implemented by subclasses.
     * 
     * @return Result of comparison (either true or false)
     */
    public abstract boolean compare(double value);

    public abstract boolean compare(double left, double right);

    @Override
    public abstract String toString();

}
