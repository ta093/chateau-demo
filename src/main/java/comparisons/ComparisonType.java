package comparisons;

public enum ComparisonType {

    CONSTANT, VARIABLE, FUNCTION

}