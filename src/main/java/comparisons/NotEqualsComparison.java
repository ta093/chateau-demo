package comparisons;

import term.Variable;

public class NotEqualsComparison<Any> extends Comparison<Any> {

    public NotEqualsComparison(ComparisonType type, Variable leftSide, Any rightSide) {
        super(type, leftSide, rightSide);
    }

    @Override
    public boolean compare(double value) {
        return !getRightSide().equals(value);
    }

    @Override
    public boolean compare(double left, double right) {
        return left != right;
    }

    @Override
    public String toString() {
        String left = getLeftSide().toString();
        String right = getRightSide().toString();

        return String.format("%s != %s", left, right);
    }
    
}
