package comparisons;

import term.Variable;

public class GreaterOrEqualComparison<Any> extends Comparison<Any> {

    public GreaterOrEqualComparison(ComparisonType type, Variable leftSide, Any rightSide) {
        super(type, leftSide, rightSide);
    }

    @Override
    public boolean compare(double value) {
        if (getRightSide() instanceof Double) {
            return value >= (double) getRightSide();
        }

        return false;
    }
    
    @Override
    public boolean compare(double left, double right) {
        return left >= right;
    }

    @Override
    public String toString() {
        String left = getLeftSide().toString();
        String right = getRightSide().toString();
        
        return String.format("%s >= %s", left, right);
    }
    
}
