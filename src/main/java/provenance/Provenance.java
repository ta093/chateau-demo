package provenance;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.logging.Level;

import chase.Chase;
import constraints.Constraint;
import constraints.Tgd;
import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import io.ChateauLogger;

/*
 * Diese Klasse implementiert die Funktionalitäten für die Evolution einer Datenbank
 * author: Florian Rose
 */
public class Provenance {

	private Instance source;
	private LinkedHashSet<Constraint> sigma;

	/**
	 * constructor
	 * 
	 * @param source instance which is being involved
	 * @param set    of integrity constraints which describe the schema evolution
	 */
	public Provenance(Instance source, LinkedHashSet<Constraint> sigma) {
		this.setSource(source);
		this.setSigma(sigma);
	}

	public Instance getSource() {
		return source;
	}

	public void setSource(Instance source) {
		this.source = source;
	}

	public LinkedHashSet<Constraint> getSigma() {
		return sigma;
	}

	public void setSigma(LinkedHashSet<Constraint> sigma) {
		this.sigma = sigma;
	}

	/*
	 * Evolves Source Schema @param
	 */
	public Instance getTargetInstance() {
		ChateauLogger.log(Level.INFO, "Evolving Schema...");
		ChateauLogger.log(Level.INFO, "Chasing Instance with Constraints");
		Instance i = Chase.chase(this.getSource(), this.getSigma());

		ChateauLogger.log(Level.INFO, "Generating Target Relations...");
		LinkedHashSet<String> targetRelations = new LinkedHashSet<String>();
		HashMap<String, SchemaTag> relations = i.getSchemaTags();
		
		for (String rel : relations.keySet()) {
			if (relations.get(rel).equals(SchemaTag.TARGET)) {
				targetRelations.add(rel);
			}
		}
		
		ChateauLogger.log(Level.INFO, "Generating Target Schema...");
		var targetSchema = new HashMap<String, LinkedHashMap<String, String>>();
		for (String rel : targetRelations) {
			targetSchema.put(rel, i.getSchema().get(rel));
		}

		HashMap<String, SchemaTag> targetSchemaTags = new HashMap<String, SchemaTag>();
		for (String rel : targetRelations) {
			targetSchemaTags.put(rel, SchemaTag.SOURCE);
		}

		ChateauLogger.log(Level.INFO, "Generating Target Instance ");
		return new Instance(i.getRelationalAtomsBySchema(targetRelations), targetSchema, OriginTag.INSTANCE,
				targetSchemaTags);
	}

	// Backchase
	public Provenance invert() {
		LinkedHashSet<Constraint> sigmaInv = new LinkedHashSet<Constraint>();
		for (Constraint c : this.getSigma()) {
			// C is a TGD
			if (c instanceof Tgd) {
				sigmaInv.add(((Tgd) c).invert());
			}
		}

		HashMap<String, SchemaTag> oldTags = this.getSource().getSchemaTags();
		HashMap<String, SchemaTag> newTags = new HashMap<String, SchemaTag>();
		for (String rel : oldTags.keySet()) {
			if (oldTags.get(rel).equals(SchemaTag.TARGET)) {
				newTags.put(rel, SchemaTag.SOURCE);
			} else {
				newTags.put(rel, SchemaTag.TARGET);
			}
		}
		Instance i = new Instance(this.getTargetInstance().getRelationalAtoms(),
				this.getSource().getSchema(), OriginTag.INSTANCE, newTags);
		return new Provenance(i, sigmaInv);
	}
}
