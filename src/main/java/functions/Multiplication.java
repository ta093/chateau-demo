package functions;

import term.Constant;
import term.Variable;

public class Multiplication<Any> extends Function<Any> {

    public Multiplication(Variable left, Any right) {
        super(left, right);
    }

    @Override
    public String getName() {
        return "Addition";
    }

    @Override
    public String toString() {
        return left + " * " + right;
    }

    @Override
    protected double eval(Constant left, Constant right) {
        double leftValue = Double.parseDouble(left.getValue().toString());
        double rightValue = Double.parseDouble(right.getValue().toString());

        double result = leftValue * rightValue;

        return result;
    }

}
