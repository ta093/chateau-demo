package functions;

import java.util.HashMap;
import java.util.LinkedHashSet;

import homomorphism.TermMapping;
import term.ConstType;
import term.Constant;
import term.Term;
import term.Variable;

public abstract class Function<Any> {

    protected Variable left;
    protected Any right;

    protected Function(Variable left, Any right) {
        if (!(right instanceof Double || right instanceof Variable || right instanceof Function<?>)) {
            throw new UnsupportedOperationException(
                    "Unsupported function type: " + right.getClass().getCanonicalName());
        }

        this.left = left;
        this.right = right;
    }

    /**
     * Gets the name of the function.
     * 
     * @return The name
     */
    public abstract String getName();

    /**
     * Evaluates the function by providing two constant values.
     * 
     * @param left  Left value, as constant
     * @param right Right value, as constant
     * @return The result, as double
     */
    protected abstract double eval(Constant left, Constant right);

    @Override
    public abstract String toString();

    /**
     * Gets the variable the function is applied on.
     * 
     * @return The variable
     */
    public Variable getVariable() {
        return this.left;
    }

    /**
     * Sets the variable the function is applied on.
     * 
     * @param variable The variable
     */
    public void setVariable(Variable variable) {
        this.left = variable;
    }

    /**
     * Evaluates the function by providing a set of mappings.
     * 
     * @param mappings A set of {@link TermMapping}s
     * @return A {@link Constant} that holds the result
     */
    public Constant toConstant(LinkedHashSet<TermMapping> mappings) {
        // convert set of term mappings to (Term -> Term) map
        HashMap<Term, Term> mappingsMap = new HashMap<>();
        mappings.forEach(mapping -> mappingsMap.put(mapping.getSource(), mapping.getTarget()));

        // type check
        if (!mappingsMap.get(left).isConstant()) {
            throw new IllegalArgumentException(
                    "Right side of left variable mapping not constant: " + mappingsMap.get(left));
        }

        // get left value of function
        Constant leftTerm = (Constant) mappingsMap.get(left);

        // type check
        if (leftTerm.getConstType() == ConstType.STRING) {
            throw new UnsupportedOperationException("Strings are not compatible with functions.");
        }

        double result;

        if (right instanceof Variable) {
            // type check
            if (!mappingsMap.get(right).isConstant()) {
                throw new IllegalArgumentException(
                        "Right side of right variable mapping not constant: " + mappingsMap.get(left));
            }

            Constant rightTerm = (Constant) mappingsMap.get(right);
            result = eval(leftTerm, rightTerm);
        } else if (right instanceof Double) {
            Constant rightTerm = Constant.fromDouble("right", (double) right);
            result = eval(leftTerm, rightTerm);
        } else { // function
            result = eval(leftTerm, ((Function<?>) right).toConstant(mappings));
        }

        // check if result is integer or floating-point number
        if (result % 1 == 0) {
            return Constant.fromInteger(left.getName(), (int) result);
        } else {
            return Constant.fromDouble(left.getName(), result);
        }
    }

}
