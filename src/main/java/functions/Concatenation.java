package functions;

import homomorphism.TermMapping;
import term.ConstType;
import term.Constant;
import term.Term;
import term.Variable;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class Concatenation<Any> extends Function<Any>{

    public Concatenation(Variable left, Any right) {
        super(left, right);
    }

    @Override
    public String getName() {
        return "Concatenation";
    }

    // not used here
    @Override
    protected double eval(Constant left, Constant right) {
        return 0;
    }

    @Override
    public String toString() {
        return left + " + " + right;
    }

    protected String concatenate(Constant left, Constant right) {
        var leftValue = left.getValue().toString();
        var rightValue = right.getValue().toString();

        return leftValue + rightValue;
    }

    @Override
    public Constant toConstant(LinkedHashSet<TermMapping> mappings) {

        // convert set of term mappings to (Term -> Term) map
        HashMap<Term, Term> mappingsMap = new HashMap<>();
        mappings.forEach(mapping -> mappingsMap.put(mapping.getSource(), mapping.getTarget()));

        // type check
        if (!mappingsMap.get(left).isConstant()) {
            throw new IllegalArgumentException(
                    "Right side of left variable mapping not constant: " + mappingsMap.get(left));
        }

        // get left value of function
        Constant leftTerm = (Constant) mappingsMap.get(left);

        // type check
        if (leftTerm.getConstType() == ConstType.STRING) {
            throw new UnsupportedOperationException("Strings are not compatible with functions.");
        }

        String result;

        if (right instanceof Variable) {
            // type check
            if (!mappingsMap.get(right).isConstant()) {
                throw new IllegalArgumentException(
                        "Right side of right variable mapping not constant: " + mappingsMap.get(left));
            }

            Constant rightTerm = (Constant) mappingsMap.get(right);
            result = concatenate(leftTerm, rightTerm);
        } else if (right instanceof String) {
            Constant rightTerm = Constant.fromString("right", (String) right);
            result = concatenate(leftTerm, rightTerm);
        } else { // function
            result = concatenate(leftTerm, ((Function<?>) right).toConstant(mappings));
        }

        return Constant.fromString(left.getName(), result);
    }

}
