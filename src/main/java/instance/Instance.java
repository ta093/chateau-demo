package instance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import atom.Atom;
import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Egd;
import constraints.Constraint;
import constraints.STTgd;
import constraints.Tgd;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * The class Instance represents database instances which consist of a set of
 * {@link atom.RelationalAtom}, a set of Strings representing the attributes of
 * the whole instance and a map with a relation name as key and its containing
 * attributes as value.
 *
 * @author Martin Jurklies
 */
public class Instance {

	private LinkedHashSet<RelationalAtom> relationalAtoms;
	// schema contains for a relation name a map of attributes to types
	private HashMap<String, LinkedHashMap<String, String>> schema;
	// tag for Source and Target in schema
	private HashMap<String, SchemaTag> schemaTags;
	// tag differentiate between instances and queries
	private OriginTag originTag;
	// cache of the head atoms of a query
	private LinkedHashSet<RelationalAtom> headAtoms;
	// relations with key attributes
	private HashMap<String, LinkedHashSet<String>> relationKeyAttributes;

	/**
	 * empty constructor
	 */
	public Instance(OriginTag originTag) {
		this(null, originTag);
	}

	/**
	 * constructor
	 *
	 * @param attributes the attributes for the new instance
	 * @param schema     the schema for the new instance
	 */
	public Instance(HashMap<String, LinkedHashMap<String, String>> schema, OriginTag originTag) {
		this(null, schema, originTag);
	}

	/**
	 * constructor
	 *
	 * @param relationalAtoms the relational atoms for the new instance
	 * @param attributes      the attributes for the new instance
	 * @param schema          the schema for the new instance
	 */
	public Instance(LinkedHashSet<RelationalAtom> relationalAtoms,
			HashMap<String, LinkedHashMap<String, String>> schema,
			OriginTag originTag) {
		this(relationalAtoms, schema, originTag, null);
	}

	/**
	 * constructor
	 *
	 * @param relationalAtoms the relational atoms for the new instance
	 * @param attributes      the attributes for the new instance
	 * @param schema          the schema for the new instance
	 * @param schemaTags      the schemaTags for the new instance
	 */
	public Instance(LinkedHashSet<RelationalAtom> relationalAtoms,
			HashMap<String, LinkedHashMap<String, String>> schema,
			OriginTag originTag, HashMap<String, SchemaTag> schemaTags) {
		this(relationalAtoms, schema, originTag, schemaTags, null);
	}

	/**
	 * constructor
	 *
	 * @param relationalAtoms the relational atoms for the new instance
	 * @param attributes      the attributes for the new instance
	 * @param schema          the schema for the new instance
	 * @param schemaTags      the schemaTags for the new instance
	 * @param headAtoms       the headAtoms for the new Instance
	 */
	public Instance(LinkedHashSet<RelationalAtom> relationalAtoms,
			HashMap<String, LinkedHashMap<String, String>> schema,
			OriginTag originTag, HashMap<String, SchemaTag> schemaTags, LinkedHashSet<RelationalAtom> headAtoms) {
		if (relationalAtoms == null) {
			relationalAtoms = new LinkedHashSet<RelationalAtom>();
		}

		if (schema == null) {
			schema = new HashMap<String, LinkedHashMap<String, String>>();
		}

		if (headAtoms == null) {
			headAtoms = new LinkedHashSet<RelationalAtom>();
		}

		this.relationalAtoms = new LinkedHashSet<RelationalAtom>();
		relationalAtoms.forEach(atom -> this.relationalAtoms.add(atom.copy()));
		this.schema = new HashMap<String, LinkedHashMap<String, String>>(schema);
		this.originTag = originTag;
		this.schemaTags = schemaTags;
		this.headAtoms = headAtoms;
	}

	/**
	 * copy constructor
	 *
	 * @param instance the instance to copy
	 */
	public Instance(Instance instance) {
		this(instance.relationalAtoms, instance.schema, instance.originTag, instance.schemaTags, instance.headAtoms);
	}

	/**
	 * @return the relational atoms
	 */
	public LinkedHashSet<RelationalAtom> getRelationalAtoms() {
		return this.relationalAtoms;
	}

	/**
	 * @param schema the schema to filter the relational atoms to be output
	 * @return the relational atoms filtered by {@code schema}
	 */
	public LinkedHashSet<RelationalAtom> getRelationalAtomsBySchema(LinkedHashSet<String> relationNames) {
		LinkedHashSet<RelationalAtom> result = new LinkedHashSet<RelationalAtom>();

		for (RelationalAtom atom : this.relationalAtoms) {
			if (relationNames.contains(atom.getName())) {
				result.add(atom);
			}
		}

		return result;
	}

	/**
	 * @param relationName the relation name to filter the relational atoms to be
	 *                     output
	 * @return the relational atoms filtered by {@code relationName}
	 */
	public LinkedHashSet<RelationalAtom> getRelationalAtomsBySchema(String relationName) {
		var result = new LinkedHashSet<RelationalAtom>();

		for (RelationalAtom atom : this.relationalAtoms) {
			if (relationName.equals(atom.getName())) {
				result.add(atom);
			}
		}

		return result;
	}

	/**
	 * Adds a relational atom to the instance.
	 *
	 * @param atom the relational atom to be added
	 * @return {@code true} if the new atom has been added, {@code false} otherwise
	 */
	public boolean addRelationalAtom(RelationalAtom atom) {
		if (this.schema.containsKey(atom.getName())) {
			return this.relationalAtoms.add(atom);
		}

		return false;
	}

	/**
	 * Removes a relational atom from the instance.
	 *
	 * @param atom The relational atom to be removed
	 * @return {@code true} if the new atom has been removed, {@code false}
	 *         otherwise
	 * @author Nic Scharlau
	 */
	public boolean removeRelationalAtom(RelationalAtom atom) {
		return this.relationalAtoms.remove(atom);
	}

	/**
	 * ...
	 */
	public void clearRelationalAtoms() {
		this.relationalAtoms.clear();
	}

	/**
	 * @return the schema
	 */
	public HashMap<String, LinkedHashMap<String, String>> getSchema() {
		return this.schema;
	}

	/**
	 *
	 * @return the schema tags
	 */
	public HashMap<String, SchemaTag> getSchemaTags() {
		return this.schemaTags;
	}

	/**
	 *
	 * @return the origin tag
	 */
	public OriginTag getOriginTag() {
		return originTag;
	}

	/**
	 * @return the attributes for relation name
	 */
	public ArrayList<String> getAttributes(String relationName) {
		return new ArrayList<String>(schema.get(relationName).keySet());

	}

	/**
	 *
	 * @return the headAtoms
	 */
	public LinkedHashSet<RelationalAtom> getHeadAtoms() {
		return headAtoms;
	}

	/**
	 *
	 * @param headAtoms
	 */
	public void setHeadAtoms(LinkedHashSet<RelationalAtom> headAtoms) {
		this.headAtoms = headAtoms;
	}

	/**
	 *
	 * @return relationKeyAttributes
	 */
	public HashMap<String, LinkedHashSet<String>> getRelationKeyAttributes() {
		return relationKeyAttributes;
	}

	/**
	 *
	 * @param relationKeyAttributes
	 */
	public void setRelationKeyAttributes(HashMap<String, LinkedHashSet<String>> relationKeyAttributes) {
		this.relationKeyAttributes = relationKeyAttributes;
	}

	/**
	 * Checks if this instance is homomorphically contained in another given
	 * instance.
	 *
	 * @param other the other instance to where a homomorphism should exist
	 * @return {@code true} if there is a homomorphism from this instance to the
	 *         other, {@code false} otherwise
	 */
	public boolean isHomomorphicallyContainedIn(Instance other) {
		for (RelationalAtom atomThis : this.relationalAtoms) {
			boolean containmentFound = false;

			for (RelationalAtom atomOther : other.getRelationalAtoms()) {
				containmentFound = containmentFound || atomThis.hasHomomorphismTo(atomOther);

				if (containmentFound) {
					break;
				}
			}

			if (!containmentFound) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks if two instances are equivalent with respect to homomorphisms to each
	 * other.
	 *
	 * @param other the other instance to where this instance should be
	 *              homomorphically equivalent
	 * @return {@code true} if there is a homomorphism from this instance to the
	 *         other and vice versa, {@code false} otheriwse
	 */
	public boolean isHomomorphicallyEquivalent(Instance other) {
		return this.isHomomorphicallyContainedIn(other) && other.isHomomorphicallyContainedIn(this);
	}

	/**
	 * Create a query from the instance; Body = all atoms, Head = headAtoms or all
	 * all-quantified
	 * variables
	 *
	 * @return Query
	 */
	public STTgd getQuery() {
		var queryBody = new LinkedHashSet<RelationalAtom>();
		var queryHead = new LinkedHashSet<RelationalAtom>();
		var queryHeadTerms = new ArrayList<Term>();
		var extractHeadAtom = false;

		// if the query was defined without headatoms, all all-quantified variables are
		// used
		if (this.getHeadAtoms().isEmpty()) {
			extractHeadAtom = true;
		}

		for (RelationalAtom atom : this.getRelationalAtoms()) {
			queryBody.add(atom);
			if (extractHeadAtom) {
				for (Term term : atom.getTerms()) {
					if (term.isVariable()) {
						var variable = (Variable) term;

						if (variable.getVariableType() == VariableType.FOR_ALL) {
							if (!queryHeadTerms.contains(term)) {
								queryHeadTerms.add(term);
							}
						}
					}
				}

			}
		}

		if (extractHeadAtom) {
			queryHead.add(new RelationalAtom("queryHead", queryHeadTerms));
		} else {
			queryHead.addAll(this.getHeadAtoms());
		}

		return new STTgd(queryBody, queryHead);
	}

	/**
	 * Check whether the query meets the tabeleau restrictions
	 * = an all quantified variable may only be used for one attribute
	 *
	 * @return
	 */
	public boolean queryCheck() {
		STTgd query = this.getQuery();
		LinkedHashSet<RelationalAtom> queryBody = query.getBody();

		// HashMap to assign the attributes their unique all-quantified variable
		HashMap<String, String> attributVmap = new HashMap<String, String>();
		for (RelationalAtom bodyAtom : queryBody) {
			int i = 0;
			var attributes = new ArrayList<String>(this.schema.get(bodyAtom.getName()).keySet());
			for (Term bodyTerm : bodyAtom.getTerms()) {
				if (bodyTerm.isVariable()) {
					var variable = (Variable) bodyTerm;

					if (variable.getVariableType() == VariableType.FOR_ALL) {
						if (attributVmap.containsKey(attributes.get(i))) {
							if (!attributVmap.get(attributes.get(i))
									.equals(variable.toString())) {
								return false;
							}
						} else {
							attributVmap.put(attributes.get(i), variable.toString());
						}
					}
				}
				i++;
			}
		}

		return true;
	}

	/**
	 * Checks all constrainsChecks
	 *
	 * @param constraints
	 * @return constraintsCheck
	 */
	public boolean checkConstraints(LinkedHashSet<Constraint> constraints) {
		// check if a constraint is defined incorrectly
		boolean constraintCheck = true;

		// Negation check incompatible with comparisons and functions;
		// leads to "false positives" even if negation don't exist at all
		// if (!checkNegations(constraints)) {
		// constraintCheck = false;
		// }

		if (!checkSttgds(constraints)) {
			constraintCheck = false;
		}

		if (!checkTgds(constraints)) {
			constraintCheck = false;
		}

		if (!checkEgds(constraints)) {
			constraintCheck = false;
		}

		return constraintCheck;
	}

	/***
	 * Checks if the Egds are defined correctly
	 * does the egd equate the same types?
	 * checks if attributes of the same type are set equal
	 * 
	 * @param constraints
	 * @return constrainCheck
	 */
	public boolean checkEgds(LinkedHashSet<Constraint> constraints) {
		boolean constraintCheck = true;
		// run through all constraints, even if the first one returns false, to log all
		// failures
		for (Constraint constraint : constraints) {
			if (constraint instanceof Egd) {
				for (Atom headAtom : constraint.getHead()) {
					var term1 = ((EqualityAtom) headAtom).getTerm1();
					var tempAtom1 = constraint.getBody().stream().filter(atom -> atom.getTerms().contains(term1))
							.findFirst().get();
					String type1 = new ArrayList<String>(this.schema.get(tempAtom1.getName()).values())
							.get(tempAtom1.getTerms().indexOf(term1));

					var term2 = ((EqualityAtom) headAtom).getTerm2();
					var tempAtom2 = constraint.getBody().stream().filter(atom -> atom.getTerms().contains(term2))
							.findFirst().get();
					String type2 = new ArrayList<String>(this.schema.get(tempAtom2.getName()).values())
							.get(tempAtom2.getTerms().indexOf(term2));

					if (!type1.equals(type2)) {
						constraintCheck = false;
					}
				}
			}
		}

		return constraintCheck;
	}

	/***
	 * Checks if the Tgds are defined correctly.
	 * does the tgd insert tuples with correct types?
	 * for each all-quantified variable in the tgd header,
	 * check if its attribute has the same type as the same all-quantified variable
	 * in the body
	 * 
	 * @param constraints
	 * @return constraintCheck
	 */
	public boolean checkTgds(LinkedHashSet<Constraint> constraints) {
		boolean constraintCheck = true;
		// run through all constraints, even if the first one returns false, to log all
		// failures
		for (Constraint constraint : constraints) {
			if (constraint instanceof Tgd) {

				for (Atom headAtom : constraint.getHead()) {
					var headAtomTypes = new ArrayList<String>(
							this.schema.get(((RelationalAtom) headAtom).getName()).values());
					int i = 0;
					for (Term headTerm : ((RelationalAtom) headAtom).getTerms()) {
						if (headTerm.isVariable()) {
							if (((Variable) headTerm).getVariableType().equals(VariableType.FOR_ALL)) {
								var headTermType = headAtomTypes.get(i);
								for (RelationalAtom bodyAtom : constraint.getBody()) {
									var bodyAtomTypes = new ArrayList<String>(
											this.schema.get(bodyAtom.getName()).values());
									int j = 0;
									for (Term bodyTerm : bodyAtom.getTerms()) {
										if (bodyTerm.equals(headTerm)) {
											var bodyTermType = bodyAtomTypes.get(j);
											if (!bodyTermType.equals(headTermType)) {
												constraintCheck = false;
											}
										}

										j++;
									}
								}
							}
						}

						i++;
					}
				}
			}
		}

		return constraintCheck;
	}

	/***
	 * Checks if the STTgds are defined correctly
	 * Maps the sttgd from the correct schema to the correct schema?
	 * 
	 * @param constraints
	 * @return constraintCheck
	 */
	public boolean checkSttgds(LinkedHashSet<Constraint> constraints) {
		boolean constraintCheck = true;

		// run through all constraints, even if the first one returns false, to log all
		// failures
		for (Constraint constraint : constraints) {
			if (constraint instanceof STTgd) {
				// if there are s-t tgds, then schemaTags have to be defined as well
				if (!this.schemaTags.isEmpty()) {
					STTgd sttgd = (STTgd) constraint;

					for (RelationalAtom bodyAtom : sttgd.getBody()) {
						// false if a bodyAtom of the s-t tgd appears in target
						if (schemaTags.get(bodyAtom.getName()) == SchemaTag.TARGET) {
							constraintCheck = false;
						}
					}
					// false if a headAtom of the s-t tgd appears in source
					for (RelationalAtom headAtom : sttgd.getHead()) {
						if (schemaTags.get(headAtom.getName()) == SchemaTag.SOURCE) {
							constraintCheck = false;
						}
					}
				} else {
					constraintCheck = false;
				}
			}
		}

		return constraintCheck;
	}

	/***
	 * Check if the negations are used correctly.
	 * aren't negations only in sttgd bodys?
	 * do negations exist in the head?
	 * are the variables of the negated atoms needed in the head?
	 * 
	 * @param constraints
	 * @return constraintCheck
	 */
	public boolean checkNegations(LinkedHashSet<Constraint> constraints) {
		boolean constraintCheck = true;
		// run through all constraints, even if the first one returns false, to log all
		// failures
		for (Constraint constraint : constraints) {
			// aren't negations only in sttgd bodys?
			for (RelationalAtom bodyAtom : constraint.getBody()) {
				if (bodyAtom.isNegated() && !(constraint instanceof Tgd)) {
					constraintCheck = false;
				}
			}

			if (constraint instanceof Tgd) {
				for (Atom headAtom : constraint.getHead()) {
					if (((RelationalAtom) headAtom).isNegated()) {
						constraintCheck = false;
					}
				}
			}
		}

		return constraintCheck;
	}

	/**
	 * Converts the schema into a readable version.
	 * 
	 * @param schema The schema that shall be converted to a string
	 * @return Schema as string, one relation per row
	 */
	public String schemaToString() {
		String schemaString = "";

		// for each relation
		for (String relationName : schema.keySet()) {
			schemaString += relationName + "(";
			// for each attribute
			for (String attribute : schema.get(relationName).keySet()) {
				schemaString += attribute + ": " + schema.get(relationName).get(attribute).toUpperCase();
				schemaString += ", ";
			}

			// remove trailing separator
			schemaString = schemaString.substring(0, schemaString.length() - 2);
			schemaString += ")," + System.lineSeparator();
		}

		// remove trailing separator
		schemaString = schemaString.substring(0, schemaString.length() - 3);
		schemaString += System.lineSeparator();

		return schemaString;
	}

	/**
	 * Merges two instances.
	 * 
	 * @param i1 "Left" instance
	 * @param i2 "Right" instance
	 * @return A new instance containing each elements of both given instances
	 */
	public static Instance merge(Instance i1, Instance i2) {
		// create new "merged" instance
		Instance i = new Instance(i1.originTag);

		// put schema attributes of both instances into new instance
		i.getSchema().putAll(i1.getSchema());
		i.getSchema().putAll(i2.getSchema());

		// add i1 tuples
		for (RelationalAtom atom : i1.getRelationalAtoms()) {
			i.addRelationalAtom(atom);
		}

		// add i2 tuples
		for (RelationalAtom atom : i2.getRelationalAtoms()) {
			i.addRelationalAtom(atom);
		}

		// "merged"/combined instance
		return i;
	}

	/**
	 * Checks whether a {@link RelationalAtom} is contained.
	 * 
	 * @param atom The atom to check
	 * @return true if the atom is part of the instance, otherwise false
	 */
	public boolean contains(RelationalAtom atom) {
		for (RelationalAtom instanceAtom : this.getRelationalAtoms()) {
			if (instanceAtom.equals(atom)) {
				return true;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		// seperators that are appended between terms and atoms respectively to avoid a
		// comma printed after the final term of an atom or after the final atom
		String sepTerm = "";
		String sepAtom = "";

		// convert HashSet to ArrayList and sort it afterwards
		List<RelationalAtom> atoms = new ArrayList<RelationalAtom>(this.relationalAtoms);
		Collections.sort(atoms);

		// output of all atoms seperated by comma and \n
		for (RelationalAtom atom : atoms) {
			// in the first iteration this is empty; will be a comma until the end
			builder.append(sepAtom);

			// bullet point
			// builder.append("- ");

			// relation name and opening bracket
			builder.append(atom.getName());
			builder.append("(");

			// temporary variables to avoid evaluation of access in every loop iteration
			ArrayList<Term> terms = atom.getTerms();
			int termSize = terms.size();

			// output of all terms in current atom seperated by comma
			for (int i = 0; i < termSize; i++) {
				// in the first iteration this is empty; will be a comma until the end
				builder.append(sepTerm);

				// gets the i-th term of the current atom and returns its value to output
				builder.append(terms.get(i).toString());
				sepTerm = ", ";
			}

			// closing bracket of the current atom
			// seperators changed for next iteration
			builder.append(")");
			sepTerm = "";
			sepAtom = "\n";
		}

		builder.append("\n");

		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + ((relationalAtoms == null) ? 0 : relationalAtoms.hashCode());
		result = prime * result + ((schema == null) ? 0 : schema.hashCode());

		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Instance)) {
			return false;
		}

		Instance other = (Instance) obj;

		if (relationalAtoms == null) {
			if (other.relationalAtoms != null) {
				return false;
			}
		}

		else if (!relationalAtoms.equals(other.relationalAtoms)) {
			return false;
		}

		if (schema == null) {
			if (other.schema != null) {
				return false;
			}
		}

		else if (!schema.equals(other.schema)) {
			return false;
		}

		return true;
	}

}
