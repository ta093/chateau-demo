package instance;

/**
 * a schemaTag can be specified in the input file 
 * to distinguish between source schemas 
 * and target schemas for s-t tgds 
 *
 */
public enum SchemaTag {
	
	SOURCE("S", "source"),
	
	TARGET("T", "target");

	private final String token;
	
	private final String typeName;

	private SchemaTag(String token, String type) {
		this.token = token;
		this.typeName = type;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	@Override
	public String toString() {
		return token;
	}
}
