package instance;

/**
 * a OriginTag allows you to distinguish between instances 
 * that were instances in the first place 
 * and instances that originated from queries
 *
 */
public enum OriginTag {
	
	INSTANCE("i", "instance"),
	
	QUERY("q", "query");

	private final String token;
	
	private final String typeName;

	private OriginTag(String token, String type) {
		this.token = token;
		this.typeName = type;
	}
	
	public String getToken() {
		return token;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	@Override
	public String toString() {
		return token;
	}
}
