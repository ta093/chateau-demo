/**
 * @author Nic Scharlau
 * 
 *         This package provides an api in order to use ChaTEAU as a library
 *         with other Java projects.
 */

package api;