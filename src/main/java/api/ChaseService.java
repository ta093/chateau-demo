package api;

import java.util.LinkedHashSet;

import chase.Chase;
import constraints.Constraint;
import instance.Instance;
import instance.OriginTag;
import io.SingleInput;

public class ChaseService {

    /**
     * Runs the CHASE taking an instance and a set of integrity constraints, then
     * returns the new instance with embedded dependencies.
     * 
     * @param instance             The instance the CHASE shall be applied on.
     * @param integrityConstraints The integrity constraints that shall be 'chased'
     *                             into the instance.
     * @return The new instance including the given constraints.
     */
    public static Instance runChase(Instance instance, LinkedHashSet<Constraint> integrityConstraints) {
        return Chase.chase(instance, integrityConstraints);
    }

    /**
     * Doesn't do anything useful for now. Just serves as a placeholder to implement
     * the interface.
     * 
     * @return A new, empty instance.
     */
    public static Instance newInstance() {
        return new Instance(OriginTag.INSTANCE);
    }

    public static void runTests(Instance instance, LinkedHashSet<Constraint> integrityConstraints)
            throws TerminationCheckFailure {
        SingleInput input = new SingleInput(instance, integrityConstraints);
        TesterService.runChecks(input);
    }
}
