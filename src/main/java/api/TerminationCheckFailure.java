package api;

public class TerminationCheckFailure extends Exception {
    public TerminationCheckFailure(String failedTestsMessage) {
        super(failedTestsMessage);
    }

    private static final long serialVersionUID = 1L;

}
