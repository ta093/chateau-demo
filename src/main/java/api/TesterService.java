package api;

import java.util.LinkedHashSet;

import constraints.Constraint;
import constraints.STTgd;
import io.SingleInput;
import termination.ConstraintRewriting;
import termination.SfS;
import termination.Tester;

public class TesterService {
    public static void runChecks(SingleInput input) throws TerminationCheckFailure{
        Tester tester = new Tester();

        // copy the constraints so that the termination test can modify them
        var terminationConstraintsCopy = new LinkedHashSet<Constraint>();
        input.getConstraints().forEach(constraint -> {
            // remove sttgds from constraints, as they are not relevant to termination
            // and the negations allowed in them were not tested in the termination test
            if(!(constraint instanceof STTgd)) {
                terminationConstraintsCopy.add(constraint.copy());
            }});

        String result;
        StringBuilder failedCheckResult = new StringBuilder();
        boolean testedPositive;

        // test for rich acyclicity
        System.out.println("Testing for rich acyclicity...");

        testedPositive = tester.checkRichAcyclicity(terminationConstraintsCopy);

        if (testedPositive) {
            result = "tgds are not richly acyclic, Standard CHASE might not terminate.\n";
            failedCheckResult.append(result);
        } else {
            result = "tgds are richly acyclic, Standard CHASE will definitely terminate.\n";
        }

        System.out.println(result);

        // test for weak acyclicity

        System.out.println("Testing for weak acyclicity...");

        testedPositive = tester.checkWeakAcyclicity(terminationConstraintsCopy);

        if (testedPositive) {
            result = "tgds are not weakly acyclic, Standard CHASE might not terminate.\n";
            failedCheckResult.append(result);
        } else {
            result = "tgds are weakly acyclic, Standard CHASE will definitely terminate.\n";
        }

        System.out.println(result);

        // test for safety
        System.out.println("Testing for safety...");

        testedPositive = tester.checkSafety(terminationConstraintsCopy);

        if (testedPositive) {
            result = "tgds are not safe, Standard CHASE might not terminate.\n";
            failedCheckResult.append(result);
        } else {
            result = "tgds are safe, Standard CHASE will definitely terminate.\n";
        }

        System.out.println(result);

        // check for acyclicity
        System.out.println("Testing for acyclicity...");

        testedPositive = new ConstraintRewriting().prepareConstraintAdn(terminationConstraintsCopy);

        if (testedPositive) {
            result = "Constraint rewriting shows that tgds are not acyclic. Standard CHASE probably won't terminate.\n";
            failedCheckResult.append(result);
        } else {
            result = "Constraint rewriting shows that tgds are acyclic. Standard CHASE will definitely terminate.\n";
        }

        System.out.println(result);

        // check for acyclicity / egd rewriting
        System.out.println("Testing for acyclicity with egd rewriting...");

        testedPositive = new ConstraintRewriting().prepareAdn(SfS.doSfS((terminationConstraintsCopy)));

        if (testedPositive) {
            result = "Constraint rewriting shows that tgds/egds are not acyclic. Standard CHASE probably won't terminate.\n";
            failedCheckResult.append(result);
        } else {
            result = "Constraint rewriting shows that tgds/egds are acyclic. Standard CHASE will definitely terminate.\n";
        }

        System.out.println(result);

        // check constraints
        System.out.println("Checking constraints...");

        boolean success = input.getInstance().checkConstraints(input.getConstraints());
        if (success) {
            result = "Constraints are defined correctly.\n";
        } else {
            result = "At least one constraint is defined incorrectly.\n";
            failedCheckResult.append(result);
        }

        System.out.println(result);
        
        String failedChecksMessage = failedCheckResult.toString();
        if(!failedChecksMessage.isEmpty()) {
            throw new TerminationCheckFailure(failedChecksMessage);
        }
    }
}
