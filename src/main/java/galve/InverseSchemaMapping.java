package galve;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import constraints.STTgd;

public class InverseSchemaMapping{
	// schema contains for a relation name a map of attributes to types
	private HashMap<String, LinkedHashMap<String, String>> sourceSchema;
	private HashMap<String, LinkedHashMap<String, String>> targetSchema;
	private HashMap<String,LinkedHashSet<STTgd>> disjunctiveSets;

	public InverseSchemaMapping(HashMap<String, LinkedHashMap<String, String>> sourceSchema, 
			HashMap<String, LinkedHashMap<String, String>> targetSchema,
			HashMap<String,LinkedHashSet<STTgd>> disjunctiveSets) {
		this.sourceSchema = sourceSchema;
		this.targetSchema = targetSchema;
		this.disjunctiveSets = disjunctiveSets;
	}
	
	public HashMap<String,LinkedHashSet<STTgd>> getDisjunctiveSets() {
		return disjunctiveSets;
	}

	public HashMap<String, LinkedHashMap<String, String>> getSourceSchema() {
		return sourceSchema;
	}

	public HashMap<String, LinkedHashMap<String, String>> getTargetSchema() {
		return targetSchema;
	}	
}