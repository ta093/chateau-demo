package galve;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import constraints.Constraint;

public class SchemaMapping{
	// schema contains for a relation name a map of attributes to types
	private HashMap<String, LinkedHashMap<String, String>> sourceSchema;
	private HashMap<String, LinkedHashMap<String, String>> targetSchema;
	private LinkedHashSet<Constraint> integrityConstraints;
	
	public SchemaMapping(HashMap<String, LinkedHashMap<String, String>> sourceSchema, 
			HashMap<String, LinkedHashMap<String, String>> targetSchema,
			LinkedHashSet<Constraint> integrityConstraints) {
		this.sourceSchema = sourceSchema;
		this.targetSchema = targetSchema;
		this.integrityConstraints = integrityConstraints;
	}

	public HashMap<String, LinkedHashMap<String, String>> getTargetSchema() {
		return targetSchema;
	}

	public HashMap<String, LinkedHashMap<String, String>> getSourceSchema() {
		return sourceSchema;
	}

	public LinkedHashSet<Constraint> getIntegrityConstraints() {
		return integrityConstraints;
	}		
}