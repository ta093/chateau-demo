package galve;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

import org.jdom2.JDOMException;

import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import io.InputReader;
import io.SingleInput;
/**
 * implementation of galve technique according to master thesis:
 *  "Datenintegration durch inverse Schemaabbildung: 
 *  		Erweiterung der Rostocker GaLVE-Technik"
 *  
 *  console program accepts any number of file paths and calculates the schema mapping of the input.
 *  the global schemas of the input schema mappings should be the same.
 *  for the calculation of the begds it is important to specify key attributes.
 *  the galve procedure will start for the first input
 *  
 *  the galve techniques described in the master thesis are implemented in GalveTechniques.java.
 *  GalveUtil.java contains outsourced functionality of galve techniques.
 *  the chases with begds is implemented in the applyBEgd method in Chase.java.  
 *  
 * @author Jakob Zimmer
 *
 */
public class MainGaLVE {
    public static void main(String[] args) {

    	// read input files
    	InputReader reader = new InputReader();
    	var inputs = new ArrayList<SingleInput>();
    	        
    	int i = 1;
        for (String arg : args) {            

            File file = new File(arg);
            if (file.exists() && !file.isDirectory()) {
            	System.out.println("Read Input_"+ i++ + " from File: " + file.getAbsolutePath() + "\n");	
            	SingleInput input;
                try {
                    input = reader.readFile(file);
                } catch (JDOMException | IOException e) {
                    e.printStackTrace();
                    return;
                }
                inputs.add(input);
            }            
        }
        if(inputs.size() < 1) {
        	System.out.println("-Empty input...");
        }else {
        	// get key attributes from first instance
        	var relationKeyAttributes = inputs.stream().findFirst().get().getInstance().getRelationKeyAttributes();        	
        	
        	// data integration
        	System.out.println("***Start data integration...***\n");
            var globalInstance = GalveTechniques.dataIntegration(inputs);
            
            System.out.println("-Global instance after integration:");
            System.out.println(globalInstance);
            
            System.out.println("-Generated global scheme:");
            globalInstance.getSchema().entrySet().
            	forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue()));
            
            // calculate inverse
            System.out.println("\n\n******Start GaLVE for first input...******\n");
            System.out.println("***Start calculation inverse mapping...***");
            
            var source = inputs.get(0).getInstance();
            var constaints = inputs.get(0).getConstraints();
            
            var sourceSchema = GalveUtil.getSchemaForSchemaTag(source, SchemaTag.SOURCE);
            var targetSchema = GalveUtil.getSchemaForSchemaTag(source, SchemaTag.TARGET);
            
            var schemaMapping = new SchemaMapping(sourceSchema, targetSchema, constaints);
            
            var inverseMapping = GalveTechniques.invert(schemaMapping);
            
            System.out.println("\n-Disjunctive sets in inverse mapping:");
            inverseMapping.getDisjunctiveSets().entrySet().
            	forEach(entry -> System.out.println("\n{" + entry.getKey() + "} = " + entry.getValue()));            
                        
            // optimize
            System.out.println("\n\n***Start calculation optimized inverse mapping...***\n");
            GalveTechniques.optimizeDisjunctiveSets(inverseMapping.getDisjunctiveSets());
            
            System.out.println("-Disjunctive sets after optimization:");
            inverseMapping.getDisjunctiveSets().entrySet().
            	forEach(entry -> System.out.println("\n{" + entry.getKey() + "} = " + entry.getValue()));            
    
            // extend
            System.out.println("\n\n***Start calculation extended inverse mapping...***");
            GalveTechniques.extend(inverseMapping);
            
            System.out.println("\n-Disjunctive sets after extension:");
            inverseMapping.getDisjunctiveSets().entrySet().
        		forEach(entry -> System.out.println("\n{" + entry.getKey() + "} = " + entry.getValue()));        
            
            System.out.println("\n-Extended source schema:");
            var extendedSchema = inverseMapping.getTargetSchema();
            extendedSchema.entrySet().
            	forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue()));
            
            // run adapted disjunctive chase with extended inverse schema mapping
            System.out.println("\n***Run disjunctive chase with extended inverse schema mapping...***");
            var unionSchema = new HashMap<String, LinkedHashMap<String, String>>();
            unionSchema.putAll(extendedSchema);
            unionSchema.putAll(targetSchema);
            
            var resultInstance = new Instance(globalInstance.getRelationalAtoms(), unionSchema, globalInstance.getOriginTag(), globalInstance.getSchemaTags());
            var instanceSet = GalveTechniques.runAdaptedDisjunvtiveChase(resultInstance, inverseMapping);
            
            System.out.println("\n-Result instance set from adapted disjunctive chase:");
            instanceSet.forEach(instance -> System.out.println(instance));
            
            // safe instance
            System.out.println("\n\n***Calculate safe instance from instance set...***");
            var safeInstance = new Instance(GalveTechniques.safeInstance(instanceSet).getRelationalAtoms(),extendedSchema, OriginTag.INSTANCE);               
            
            System.out.println("\n-Save instance:");
            System.out.println(safeInstance);                        
            
            // extend source with safe instance            
            var newNullHashSet = new HashSet<Instance>();
            newNullHashSet.add(source);
            newNullHashSet.add(safeInstance);
            GalveUtil.makeDisjunctNulls(newNullHashSet);
            
            safeInstance.getRelationalAtoms().forEach(atom -> atom.setOrigin(false));
            source.getRelationalAtoms().forEach(atom -> atom.setOrigin(true));
            safeInstance.getRelationalAtoms().forEach(atom -> source.getRelationalAtoms().add(atom));           
            
            System.out.println("-Extended source instance:");
            System.out.println(source);
            
            // calculate follow-up constrains
            System.out.println("\n***Calculate follow-up Tgds and bEgds...***");
            var followUpTgds = GalveTechniques.followUpTgds(source.getSchema(), sourceSchema, relationKeyAttributes);            
            var followUpBEgds = GalveTechniques.followUpBEgds(source.getSchema(), sourceSchema, relationKeyAttributes);
            
            System.out.println("\n-Follow-up constrains:");
            System.out.println(followUpTgds);
            System.out.println(followUpBEgds);
            
            // run chase with follow-up constrains
            System.out.println("\n-Run chase with follow-up Tgds...");
            
            // adjust schema of the source instance
            sourceSchema.entrySet().forEach(entry -> source.getSchema().put(entry.getKey(), entry.getValue()));
            source.getSchemaTags().clear();
            
            var singleInputForTgdResult = new SingleInput(source, followUpTgds);
            var resultForTgds = GalveUtil.runChase(singleInputForTgdResult);
            
            System.out.println("\n-Result from follow-up Tgds...");
            System.out.println(resultForTgds);

            System.out.println("\n-Run chase with follow-up bEgds...");

            // run chase as many times as it takes to equate all constants 
            var result = new Instance(OriginTag.INSTANCE);
            var tempResult = new Instance(OriginTag.INSTANCE);
            do {
            	tempResult = new Instance(result);
            	var singleInputForBEgdResult = new SingleInput(resultForTgds, followUpBEgds);
                result = GalveUtil.runChase(singleInputForBEgdResult);                                                     	
            }while(tempResult.equals(result));
            
            System.out.println("\n******Galve result:******");
            System.out.println(result);

        }                
    }	
}