package galve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;

import atom.RelationalAtom;
import chase.Chase;
import constraints.STTgd;
import homomorphism.Homomorphism;
import homomorphism.TermMapping;
import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import io.SingleInput;
import term.Null;
import term.Term;
import term.Variable;
import term.VariableType;

/**
 * outsourced functionality for implementation of galve technique according to master thesis:
 *  "Datenintegration durch inverse Schemaabbildung: 
 *  		Erweiterung der Rostocker GaLVE-Technik"
 *  
 * @author Jakob Zimmer
 *
 */
final class GalveUtil{		
    
	/**
	 * run standard chateau chase
	 * @param input
	 * @return chase result instance
	 */
    static Instance runChase(SingleInput input) {        
    	
        return Chase.chase(input.getInstance(), input.getConstraints());
    }
    
	/**
	 * Create a HashMap for counting the indexes of new generated Null values.
	 * It has the attributes of instance I as key and the counter starting at 0 as value.
	 * 
	 * @return nullcounter
	 */
	static HashMap<String, Integer> initializeNullCounter(Instance instance) {				
	
		var nullCounter = new HashMap<String, Integer>();
		for(LinkedHashMap<String,String> attributeTypeMap : instance.getSchema().values()) {
			for(String attributeName : attributeTypeMap.keySet()) {
				nullCounter.put(attributeName, 0);
			}
		}
		
		return nullCounter;
	}
	
	/**
	 * update the nullCounter as long as nullCheck fails
	 * @param left
	 */
	static void updateNullCounter(Instance instance, HashMap<String, Integer> nullCounter, String attribute) {
		do {
			incrementCounter(nullCounter, attribute);
		} while (!nullCheck(instance, nullCounter, attribute));
	}
	
	/**
	 * increment nullCounter for an attribute
	 * @param counter
	 * @param key
	 */
	static void incrementCounter(Map<String, Integer> counter, String key) {
		var currentValue = counter.get(key);

		counter.replace(key, currentValue + 1);
	}
	
	/**
	 * Checks whether the new null value or #E already exists within the instance
	 * 
	 * @param instance
	 * @param nullCounter
	 * @return boolean for nullCheck
	 */
	static boolean nullCheck(Instance instance, HashMap<String, Integer> nullCounter, String attribute) {
		int index = nullCounter.get(attribute);

		// relation together with the position of the attribute in the relation
		var relationAttributePositionMap = new HashMap<String, Integer>();

		for(Entry<String, LinkedHashMap<String, String>> relationSchema : instance.getSchema().entrySet()) {
			var attributes = new ArrayList<String>(relationSchema.getValue().keySet());
			for(String attributeInSchema : attributes) {
				if (attributeInSchema.equals(attribute)) {
					relationAttributePositionMap.put(relationSchema.getKey(), attributes.indexOf(attributeInSchema));
				}
			}			
		}

		for (var relationAttributePosition : relationAttributePositionMap.entrySet()) {
			var relationAtoms = instance.getRelationalAtomsBySchema(relationAttributePosition.getKey());

			for (var relationalAtom : relationAtoms) {
				var attrPosIndex = relationAttributePosition.getValue();
				var term = relationalAtom.getTerms().get(attrPosIndex);

				if (instance.getOriginTag() == OriginTag.INSTANCE) {
					if (term.isNull()) {
						var nullTerm = (Null) term;

						if (nullTerm.getIndex() == index) {
							return false;
						}
					}
				} else {
					if (term.isVariable()) {
						var variable = (Variable) term;

						if (variable.getVariableType() == VariableType.EXISTS) {
							if (variable.getIndex() == index) {
								return false;
							}
						}
					}
				}
			}
		}

		return true;
	}
	
	/**
	 * calculates a subschema with respect to a schemaTag
	 * @param instance
	 * @param schemaTag
	 * @return schema for schemaTag
	 */
	static HashMap<String, LinkedHashMap<String, String>> getSchemaForSchemaTag(Instance instance, SchemaTag schemaTag){
		// get relations for schemaTag
		var relations = new ArrayList<String>(); 
        instance.getSchemaTags().entrySet().stream().
        		filter(schemaTagEntry -> schemaTagEntry.getValue().equals(schemaTag)).
        		forEach(schemaTagEntry -> relations.add(schemaTagEntry.getKey()));
        // fill schema with relations
        var schema = new HashMap<String, LinkedHashMap<String, String>>(); 
		instance.getSchema().entrySet().stream().
	            filter(schemaEntry -> relations.contains(schemaEntry.getKey())).
	            forEach(schemaEntry -> schema.put(schemaEntry.getKey(), schemaEntry.getValue()));     
		return schema;
	}
	
	/**
	 * average of the relations for which there are atoms in the heads of the sttgds
	 * only relations that appear in each head are returned 
	 * @param disjunctiveSet
	 * @return head relations intersection for sttgds
	 */
	static ArrayList<String> headRelationsIntersection(LinkedHashSet<STTgd> disjunctiveSet) {
	
		var headRealations = new ArrayList<String>();
		var realationsCounterMap = new HashMap<String,Integer>();

		for(STTgd sttgd : disjunctiveSet) {
			for(RelationalAtom atom : sttgd.getHead()) {
				if(realationsCounterMap.containsKey(atom.getRelationName())) {
					realationsCounterMap.put(atom.getRelationName(), realationsCounterMap.get(atom.getRelationName()) + 1);
				}else {
					realationsCounterMap.put(atom.getRelationName(), 1);
				}
			}
		}
		
		// add to result if there is a common relation in any head
		realationsCounterMap.entrySet().stream().
			filter(realtionCounter -> realtionCounter.getValue() == disjunctiveSet.size()).
			forEach(realtionCounter -> headRealations.add(realtionCounter.getKey()));
				
		return headRealations;
	}
	
	/**
	 * checks if there is another disjunctive set whose average of the head atoms has an atom 
	 * from the union of the head relations of the given disjunctive set
	 * 
	 * disjunctive sets whose average is not empty find themselves and result in true
	 * @param disjunctiveSet
	 * @param disjunctiveSets
	 * @return exists disjunktive set with common relation in there intersection
	 */
	static boolean existsDisjunktiveSetWithRelationInIntersection(LinkedHashSet<STTgd> disjunctiveSet, HashMap<String,LinkedHashSet<STTgd>> disjunctiveSets) {
		
		var headRealtionsUnion = new ArrayList<String>();
		
		// union of the head relations
		disjunctiveSet.forEach(sttgd -> sttgd.getHead().
				forEach(atom -> headRealtionsUnion.add(atom.getRelationName())));
		
		var result = disjunctiveSets.entrySet().stream().
				anyMatch(otherDisjunctiveSet ->	headRelationsIntersection(otherDisjunctiveSet.getValue()).stream().
						anyMatch(relation -> headRealtionsUnion.contains(relation)));
		
		return result;
	}
	
	/**
	 * invert a sttgd by swapping the body and head and adjusting the all quantification and existence quantification
	 * @param sttgd
	 * @return inverted sttgd
	 */
	static STTgd invert(STTgd sttgd) {
		
		var newBody = new LinkedHashSet<RelationalAtom>();
		var newHead = new LinkedHashSet<RelationalAtom>();	
		
		var bodyHomomorphismus = new Homomorphism();
		var headHomomorphismus = new Homomorphism();
		
		// create homomorphism that maps all existence-quantified variables to all-quantified ones
		sttgd.getHead().forEach(atom -> {
			atom.getTerms().stream().
			filter(term -> term.isVariable() && ((Variable)term).getVariableType().equals(VariableType.EXISTS)).
			forEach(term -> bodyHomomorphismus.addMapping(new TermMapping(term, 
									new Variable(VariableType.FOR_ALL,((Variable)term).getIndexName(),((Variable)term).getIndex()))));
			newBody.add(bodyHomomorphismus.applyMappingsTo(atom));
		});

		// create a homomorphism that maps all all-quantified variables to existence-quantified variables 
		// if the new body has no matching all-quantified variable
		sttgd.getBody().forEach(atom -> {	
			atom.getTerms().stream().
				filter(term -> term.isVariable() && !newBody.stream().anyMatch(newBodyAtom -> newBodyAtom.getTerms().contains(term))).
				forEach(term -> headHomomorphismus.addMapping( new TermMapping(term, 
									new Variable(VariableType.EXISTS,((Variable)term).getIndexName(),((Variable)term).getIndex()))));
			newHead.add(headHomomorphismus.applyMappingsTo(atom));
		});	
		
		return new STTgd(newBody, newHead);
	}
	
	/**
	 * copy the given schema
	 * @param schema
	 * @return copy for a schema
	 */
	static HashMap<String, LinkedHashMap<String, String>> copySchema(HashMap<String, LinkedHashMap<String, String>> schema) {
		
		var schemaCopy = new HashMap<String, LinkedHashMap<String, String>>();
		
		for(Entry<String, LinkedHashMap<String, String>> schemaEntry : schema.entrySet()) {
			var schemaValueCopy = new LinkedHashMap<String, String>();
			schemaEntry.getValue().entrySet().forEach(value -> schemaValueCopy.put(value.getKey(), value.getValue()));
			schemaCopy.put(schemaEntry.getKey(),schemaValueCopy);	
		}
		
		return schemaCopy;
	}
	
	/**
	 * copy the given disjunctive set
	 * @param disjunctiveSets
	 * @return copy for a disjunctive set
	 */
	static HashMap<String,LinkedHashSet<STTgd>> copyDisjunktiveSets(HashMap<String,LinkedHashSet<STTgd>> disjunctiveSets){
		
		var disjunctiveSetsCopy = new HashMap<String,LinkedHashSet<STTgd>>();			
		
		for(Entry<String,LinkedHashSet<STTgd>> disjunctiveSet : disjunctiveSets.entrySet()) {
			var sttgdsCopy = new LinkedHashSet<STTgd>();
			disjunctiveSet.getValue().forEach(sttgd -> sttgdsCopy.add(new STTgd(sttgd.copyBody(),sttgd.copyHead())));
			disjunctiveSetsCopy.put(disjunctiveSet.getKey(), sttgdsCopy);
		}

		return disjunctiveSetsCopy;
	}
	
	/**
	 * apply all term mappings to an atom
	 * @param atom
	 * @param termMappings
	 * @return mapped atom
	 */
	static RelationalAtom applyTermMappings(RelationalAtom atom, HashSet<TermMapping> termMappings) {
		
		var tempAtom = atom.copy();
		var newAtom = atom.copy();
		
		do {
			tempAtom = newAtom;
			
			for(TermMapping mapping : termMappings) {
				newAtom = mapping.apply(newAtom);
			}
			
		}while(!tempAtom.equals(newAtom));
		
		return newAtom;
	}
	
	/**
	 * apply all term mappings to all atoms of the instance
	 * @param instance
	 * @param termMappings
	 * @return mapped instance
	 */
	static Instance applyTermMappings(Instance instance, HashSet<TermMapping> termMappings) {
		
		var newInstance = new Instance(null,instance.getSchema(),instance.getOriginTag());
			
		for(RelationalAtom atom : instance.getRelationalAtoms()) {			
			newInstance.getRelationalAtoms().add(applyTermMappings(atom, termMappings));
		}						
		
		return newInstance;
	}
	
	/**
	 * test whether a homomorphism can be formed between the atoms given the existing homomorphism 
	 * @param homomorphism
	 * @param sourceAtom
	 * @param targetAtom
	 * @return boolean for extensibility of the homomorphism
	 */
	static boolean homomorphismIsExpandable(HashSet<TermMapping> homomorphism, RelationalAtom sourceAtom, RelationalAtom targetAtom) {
		var homomorphismIsExpandable = false;
		var sourceTerms = sourceAtom.getTerms();
		var targetTerms = targetAtom.getTerms();

		// test if the two atoms have the same relation name and same size
		if (sourceAtom.getName().equals(targetAtom.getName()) && sourceTerms.size() == targetTerms.size()) {
			for (int i = 0; i < sourceTerms.size(); i++) {
				var sourceTerm = sourceTerms.get(i);
				var targetTerm = targetTerms.get(i);					
								
				// test whether a homomorphism can be formed between the terms 
				// and whether a mapping to another term already exists in the homomorphism, 
				// which cannot be mapped to the new target term
				if(sourceTerm.hasHomomorphismTo(targetTerm)
					&&!homomorphism.stream().anyMatch(mapping -> 
						mapping.getSource().equals(sourceTerm)
						&& !mapping.getTarget().hasHomomorphismTo(targetTerm)
						&& !targetTerm.hasHomomorphismTo(mapping.getTarget()))) {
						
						homomorphismIsExpandable = true;
					}else {						
						homomorphismIsExpandable = false;
						break;
					}	
				}	
		}
		
		return homomorphismIsExpandable;
	}
	
	/**
	 * add a new term mappings
	 * @param homomorphism
	 * @param sourceAtom
	 * @param targetAtom
	 */
	static void addTermMapping(HashSet<TermMapping> homomorphism, RelationalAtom sourceAtom, RelationalAtom targetAtom) {
		
		var sourceTerms = sourceAtom.getTerms();
		var targetTerms = targetAtom.getTerms();
		
		// new mapping for each term in the atoms
		for (int i = 0; i < sourceTerms.size(); i++) {			
			var sourceTerm = sourceTerms.get(i);
			var targetTerm = targetTerms.get(i);
		
			// do not add trivial mappings and check if a homomorphism can be formed
			if(!sourceTerm.equals(targetTerm) && sourceTerm.hasHomomorphismTo(targetTerm)) {
				var newTermMapping = new TermMapping(sourceTerm, targetTerm);

				// add mapping if it does not exist yet
				if(!homomorphism.stream().anyMatch(mapping -> mapping.getSource().equals(newTermMapping.getSource())
						&& mapping.getTarget().equals(newTermMapping.getTarget()))) {
					homomorphism.add(newTermMapping);
				}				
			}			
		}				
	}
	
	/**
	 * returns the null values that are mapped to different values by the same homomorphism
	 * @param homomorphism
	 * @param sourceAtom
	 * @param targetAtom
	 * @param instance
	 * @return conflicting nulls
	 */
	static HashSet<Null> getConflictingNulls(HashSet<TermMapping> homomorphism) {		
		
		var conflictingNulls = new HashSet<Null>();
		
		// search conflicting null values that are mapped by the same homomorphism
		// to different constants
		homomorphism.stream().
		filter(outerMapping -> outerMapping.getSource().isNull() 
				&& homomorphism.stream().anyMatch(innerMapping -> 
						innerMapping.getSource().equals(outerMapping.getSource())
						&& !innerMapping.getTarget().hasHomomorphismTo(outerMapping.getTarget())
						&& !outerMapping.getTarget().hasHomomorphismTo(innerMapping.getTarget()))).									
		forEach(outerMapping -> conflictingNulls.add((Null) outerMapping.getSource()));
				
		return conflictingNulls;
	}
	/**
	 * returns true if atoms containing null values 
	 * that are mapped to different values by the same homomorphism 
	 * @param homomorphism
	 * @param sourceAtom
	 * @param targetAtom
	 * @param instance
	 * @return atom has conflicting nulls
	 */
	static boolean hasConflictingNulls(RelationalAtom atom, HashSet<TermMapping> homomorphism) {		
		
		var conflictingNulls = getConflictingNulls(homomorphism);		
		
		return conflictingNulls.stream().anyMatch(conflictingNull -> atom.getTerms().contains(conflictingNull));
	}
	
	/**
	 * add a new term mappings for two atoms without mapping the conflicting null values 
	 * that are mapped to different values by the same homomorphism
	 * @param homomorphism
	 * @param sourceAtom
	 * @param targetAtom
	 * @param instance
	 * @return
	 */
	static void addTermMappingWithoutConflictingNulls(HashSet<TermMapping> homomorphism, RelationalAtom sourceAtom, RelationalAtom targetAtom) {		
		
		var conflictingNulls = getConflictingNulls(homomorphism);		
		
		var conflictingNullsIndices = new ArrayList<Integer>();
		
		conflictingNulls.forEach(conflictingNull -> conflictingNullsIndices.add(sourceAtom.getTerms().indexOf(conflictingNull)));						
		
		var sourceAtomCopy = sourceAtom.copy();
		var targetAtomCopy = targetAtom.copy();
		
		
		for(Integer index : conflictingNullsIndices) {
			sourceAtomCopy.getTerms().remove(sourceAtom.getTerms().get(index));
			targetAtomCopy.getTerms().remove(targetAtom.getTerms().get(index));
		}
		
		addTermMapping(homomorphism, sourceAtom, targetAtom);
	}
	
	
	/**
	 * change the null values in each instance so that there is no overlap of null values among instances
	 * @param instances
	 */
	static void makeDisjunctNulls(HashSet<Instance> instances) {
		
		// a single instance does not need to be changed
		if(instances.size()>1) {
			var firstInstance = instances.stream().findFirst().get();

	        var nullcounter = GalveUtil.initializeNullCounter(firstInstance);	        			
			
	        // the seen instances are checked for the presence of null values 
	        var seenInstances = new HashSet<Instance>();
	        seenInstances.add(firstInstance);
	        
	        for(Instance instance : instances) {
	        	// first instance does not need to be changed
	        	if(!seenInstances.contains(instance)) {

	        		for(RelationalAtom atom : instance.getRelationalAtoms()) {
			        	for(Term term : atom.getTerms()) {
			        		if(term.isNull()){
			        			// is the zero value contained in a seen instance 
			        			if(seenInstances.stream().anyMatch(seenInstance -> 
			        				seenInstance.getRelationalAtoms().stream().anyMatch(seenAtom -> 
			        					seenAtom.getTerms().stream().anyMatch(seenTerm -> 
			        						seenTerm.equals(term))))) {
			        				// update null counter or all seen instances
			        				String attribute = instance.getAttributes(atom.getRelationName()) 
				    						.get(atom.getTerms().indexOf(term));
			        				seenInstances.forEach(seenInstance -> GalveUtil.updateNullCounter(seenInstance, nullcounter, attribute));
			        				
			        				var termCopy = term.copy();
			        				
			        				// also adjust all other equal null values to the new index
				    				instance.getRelationalAtoms().
				    					forEach(otherAtom -> otherAtom.getTerms().stream().
			    								filter(otherTerm -> otherTerm.equals(termCopy)).
			    								forEach(otherTerm ->((Null) otherTerm).setIndex(nullcounter.get(attribute))));
			        			}			    				
			                }
			        	}
			        }
					seenInstances.add(instance);
	        	}								
			}	
		}				
	}
	
	/**
	 * generates a new relation name for a given schema by adding a + to it
	 * @param oldName
	 * @param schema
	 * @return new name
	 */
	static String generateNewName(String oldName, HashMap<String, LinkedHashMap<String, String>> schema) {
		do {
			oldName = oldName.concat("+");
		}while(schema.containsKey(oldName));
		
		return oldName;
	}	
}