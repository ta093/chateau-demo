package galve;

import java.util.LinkedHashSet;

import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Egd;

public class BEgd extends Egd {

	public BEgd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<EqualityAtom> head) {
		super(body, head);
	}

}