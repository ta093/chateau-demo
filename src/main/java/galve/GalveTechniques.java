package galve;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;

import atom.Atom;
import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Constraint;
import constraints.STTgd;
import constraints.Tgd;
import homomorphism.TermMapping;
import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import io.SingleInput;
import term.Variable;
import term.VariableType;

/**
 * implementation of galve technique according to master thesis:
 *  "Datenintegration durch inverse Schemaabbildung: 
 *  		Erweiterung der Rostocker GaLVE-Technik"
 *  
 * @author Jakob Zimmer
 *
 */
public final class GalveTechniques{
	
	/**
	 * executes the schema mappings
	 * combines the results with disjoint null values
	 * @param inputs
	 * @return global instance
	 */
	static Instance dataIntegration(ArrayList<SingleInput> inputs) {
		
		var globalInstances = new HashSet<Instance>();
		var globalInstance = new Instance(OriginTag.INSTANCE);
				
		for (SingleInput input : inputs) {
			if(!input.getInstance().getOriginTag().equals(OriginTag.INSTANCE)) {
				System.out.println("GaLVE is defined only for instances. Input is skipped.\n");
			}
			if(!input.getConstraints().stream().allMatch(constraint -> constraint instanceof STTgd)) {
				System.out.println("GaLVE is defined only for S-T TGDs. Input is skipped.\n");
			}

            System.out.println("-Input_" + (inputs.indexOf(input) + 1) + ":");
            System.out.println(input.getInstance());
            
            System.out.println("-Constraints_" + (inputs.indexOf(input) + 1) + ":");
            System.out.println(input.getConstraints());
             
            // fill global scheme	
            globalInstance.getSchema().putAll(GalveUtil.getSchemaForSchemaTag(input.getInstance(), SchemaTag.TARGET));
//            System.out.println("\n-Generated global scheme for input (last relation win):");
//            System.out.println(globalInstance.getSchema());
                        
            var resultInstance = GalveUtil.runChase(input);
            
            System.out.println("\n-Output" + (inputs.indexOf(input) + 1) + ":");
            System.out.println(resultInstance);
            
            globalInstances.add(resultInstance);            			                       
		}                             
		// make disjunct nulls		
		GalveUtil.makeDisjunctNulls(globalInstances);
		
		globalInstances.forEach(instance -> globalInstance.getRelationalAtoms().addAll(instance.getRelationalAtoms()));
                               
        return globalInstance;
	}
	
	/**
	 * calculate adapted strong maximum extended recovery 
	 * @param schemaMapping
	 * @return inverse schema mapping
	 */
	static InverseSchemaMapping invert(SchemaMapping schemaMapping) {		
		
		// normalize sttgds
		var normalizedSttgds = new LinkedHashSet<Constraint>(); 
				
		for(Constraint constraint : schemaMapping.getIntegrityConstraints()) {
			for(Atom headAtom : constraint.getHead()) {
				var newHead = new LinkedHashSet<RelationalAtom>();
				newHead.add((RelationalAtom) headAtom.copy());
				normalizedSttgds.add(new STTgd(((STTgd)constraint).copyBody(),newHead));
			}
		}
				
		// invert the sttgds and collect them by relations in the bodies in disjunctive sets
		var disjunctiveSets = new HashMap<String,LinkedHashSet<STTgd>>();
		
		for(Constraint constraint : normalizedSttgds) {
			var headRelationName = ((RelationalAtom)constraint.getHead().stream().findAny().get()).getName();			
			if(disjunctiveSets.containsKey(headRelationName)) {
				disjunctiveSets.get(headRelationName).add(GalveUtil.invert((STTgd)constraint));
			}else {		
				var sttgds = new LinkedHashSet<STTgd>();
				sttgds.add(GalveUtil.invert((STTgd)constraint));
				
				disjunctiveSets.put(headRelationName, sttgds);
			}
		}				
		
		return new InverseSchemaMapping(schemaMapping.getTargetSchema(), schemaMapping.getSourceSchema(), disjunctiveSets);
	}
	
	/**
	 * delete the disjunctive sets that will not contribute to the result of the safe instance
	 * (delete disjunctive sets that split the chase tree in the disjunctive chase 
	 * 	and have no overlap with other disjunctive sets)
	 * @param disjunctiveSets
	 */
	static void optimizeDisjunctiveSets(HashMap<String,LinkedHashSet<STTgd>> disjunctiveSets) {
		var toRemove = new LinkedHashSet<String>();
		
		for(Entry<String,LinkedHashSet<STTgd>> disjunctiveSet : disjunctiveSets.entrySet()) {
			if(GalveUtil.headRelationsIntersection(disjunctiveSet.getValue()).isEmpty()) {
				if(!GalveUtil.existsDisjunktiveSetWithRelationInIntersection(disjunctiveSet.getValue(), disjunctiveSets)) {
					toRemove.add(disjunctiveSet.getKey());
				}
			}	
		}
		
		toRemove.forEach(relationName -> disjunctiveSets.remove(relationName));
	}	
	
	/**
	 * extend the schema mapping so that it maps into new relations 
	 * and transfers additional interesting attributes from the global schema
	 * @param inverse
	 */
	static void extend(InverseSchemaMapping inverse) {
		
		// generate disjunctive sets that map to new relations 
		// extend scheme of the source
		var newDisjunctiveSets = new HashMap<String,LinkedHashSet<STTgd>>();
		
		// copy the schema to not disturb the new name generation when the schema is extended
		var originalTargetSchema = GalveUtil.copySchema(inverse.getTargetSchema());
		
		for(Entry<String,LinkedHashSet<STTgd>> disjunctiveSet : inverse.getDisjunctiveSets().entrySet()) {
			var newDisjunctiveSet = new LinkedHashSet<STTgd>();
			
			for(STTgd sttgd : disjunctiveSet.getValue()) {
				
				var newSttgd = new STTgd(sttgd.getBody(), new LinkedHashSet<RelationalAtom>());
			
				for(RelationalAtom atom : sttgd.getHead()) {
				
					var oldRelationName = atom.getRelationName();
					var newRelationName = GalveUtil.generateNewName(oldRelationName, originalTargetSchema);
					
					newSttgd.getHead().add(new RelationalAtom(newRelationName, atom.getTerms()));
					
					var newAttributeTypeMap = new LinkedHashMap<String, String>();
				
					for(Entry<String, String> entry : inverse.getTargetSchema().get(oldRelationName).entrySet()) {
						newAttributeTypeMap.put(entry.getKey(), entry.getValue());
					}
					inverse.getTargetSchema().put(newRelationName, newAttributeTypeMap);	
				}				
				newDisjunctiveSet.add(newSttgd);
			}		
			newDisjunctiveSets.put(disjunctiveSet.getKey(), newDisjunctiveSet);
		}
		
		inverse.getDisjunctiveSets().clear();
		inverse.getDisjunctiveSets().putAll(newDisjunctiveSets);
		
		// search for interesting attributes
		// these are the attributes that appear in a body but not in a head
		var allAttibutes = new HashSet<String>();
		var interestingAttibutes = new HashSet<String>();
		
		// collect all attributes that occur in the relations of the body atoms of the disjunctive sets 
		for(Entry<String,LinkedHashSet<STTgd>> disjunctiveSet : inverse.getDisjunctiveSets().entrySet()) {
			for(Entry<String, String> attributeTypeMap : inverse.getSourceSchema().get(disjunctiveSet.getKey()).entrySet()) {
				allAttibutes.add(attributeTypeMap.getKey());
			}
		}

		for(String attribute : allAttibutes) {
			
			var interesting = true;
			
			for (Entry<String, LinkedHashSet<STTgd>> disjunctiveSet : inverse.getDisjunctiveSets().entrySet()) {
			
				if(inverse.getSourceSchema().get(disjunctiveSet.getKey()).containsKey(attribute)) {
					var schemaAttributeList = new ArrayList<String>();
					inverse.getSourceSchema().get(disjunctiveSet.getKey()).keySet().
						forEach(schemaAttribute -> schemaAttributeList.add(schemaAttribute));
					
					var termPosition = schemaAttributeList.indexOf(attribute);	
					
					interesting = disjunctiveSet.getValue().stream().
							anyMatch(sttgd -> {
								var bodyTerm = sttgd.getBody().stream().findFirst().get().getTerms().get(termPosition);
								// only variable can be interesting
								if(!bodyTerm.isVariable()) {
									return false;
								}
								// if the variable also appears in the head, then it is not interesting
								if(sttgd.getHead().stream().anyMatch(headAtom -> 
									headAtom.getTerms().contains(bodyTerm))) {
									return false;								
								}else {
									// else still interesting
									return true;
								}								
							});
					if(!interesting) {
						break;
					}							
				}
			}
			if(interesting) {
				interestingAttibutes.add(attribute);				
			}		
		}						
		
		// expand mappings of the disjunctive sets and 
		// the schema of the source with corresponding interesting attributes
		for(String attribute : interestingAttibutes) {
			for(Entry<String, LinkedHashSet<STTgd>> disjunctiveSet : inverse.getDisjunctiveSets().entrySet()) {
				// expand only the disjunctive sets with the corresponding attribute in the body
				if(inverse.getSourceSchema().get(disjunctiveSet.getKey()).containsKey(attribute)) {
					
					var schemaAttributeList = new ArrayList<String>();
					inverse.getSourceSchema().get(disjunctiveSet.getKey()).keySet().
						forEach(schemaAttribute -> schemaAttributeList.add(schemaAttribute));
					
					var termPosition = schemaAttributeList.indexOf(attribute);	
					
					for(STTgd sttgd : disjunctiveSet.getValue()) {
						
						// it is enough to find the first term, the terms are the same by construction for each sttgd
						var bodyAtom = sttgd.getBody().stream().findFirst().get();
						var bodyTerm = bodyAtom.getTerms().get(termPosition);
						
						for(RelationalAtom atom : sttgd.getHead()) {							
							
							// attribute is new for the relation of the atom
							if(!inverse.getTargetSchema().get(atom.getRelationName()).containsKey(attribute)) {								
								// add interesting attribute
								atom.getTerms().add(bodyTerm);
								
								// change all corresponding atoms in the heads of the sttgds in the disjunctive sets
								for(Entry<String, LinkedHashSet<STTgd>> innerDisjunctiveSet : inverse.getDisjunctiveSets().entrySet()) {
									for(STTgd innerSttgd : innerDisjunctiveSet.getValue()) {
										for(RelationalAtom innerAtom : innerSttgd.getHead()) {											
											// atoms with the same name are corresponding atoms that must be changed
											if(innerAtom.getRelationName().equals(atom.getRelationName())) {
												// atom is in a disjunctive set that does not have the attribute in the body atom
												if(!inverse.getSourceSchema().get(innerDisjunctiveSet.getKey()).containsKey(attribute)) {

													// if the atom is a variable, add a new existence quantified variable
													if(bodyTerm.isVariable()) {
														var bodyTermCopy = new Variable(VariableType.EXISTS, ((Variable) bodyTerm).getIndexName(), ((Variable) bodyTerm).getIndex());
														if(!innerAtom.getTerms().contains(bodyTermCopy)) {
															innerAtom.getTerms().add(bodyTermCopy);
														}
													}else {
														// this block is not needed anymore, because constants can't result in interesting attributes
														// if this is changed: 
														// with a constant as bodyTerm a NEW existence quantified variable for the head must be created and added
														var existingIndices = new ArrayList<Integer>();
														innerSttgd.getHead().forEach(otherAtom -> otherAtom.getTerms().stream().
																filter(otherTerm -> otherTerm.isVariable() && ((Variable)otherTerm).getIndexName().equals(attribute)).
																forEach(otherTerm -> existingIndices.add(((Variable)otherTerm).getIndex())));
														var newIndex = 1;
														if(!existingIndices.isEmpty()) {
															Collections.sort(existingIndices, Collections.reverseOrder()); 
															newIndex = existingIndices.stream().findFirst().get() + 1 ;	
														}
														
														var newVariabel = new Variable(VariableType.EXISTS, attribute, newIndex);
														innerAtom.getTerms().add(newVariabel);
													}
												}else {
													// also change atoms that have already been adjusted in other disjunctive sets
													// this case occurs when two sttgds in a disjunctive set have the same atoms in the head
													var innerBodyAtom = innerSttgd.getBody().stream().findFirst().get();
													var innerBodyTerm = innerBodyAtom.getTerms().get(termPosition);
													
													if(bodyAtom.getRelationName().equals(innerBodyAtom.getRelationName())
															&& atom.getRelationName().equals(innerAtom.getRelationName()) 
															&& !innerAtom.getTerms().contains(innerBodyTerm)) {
														innerAtom.getTerms().add(innerBodyTerm);	
													}
												}
											}											
										}	
									}									
								}							
								// adjust the schema
								var type = inverse.getSourceSchema().get(disjunctiveSet.getKey()).get(attribute);
								inverse.getTargetSchema().get(atom.getRelationName()).put(attribute, type);
							}else {
								// attribute already exists in the relation of the atom
								// replace the existing variables in the sttgd head with variables/constants from the body								
								inverse.getTargetSchema().entrySet().stream().
									filter(schemaEntry -> schemaEntry.getKey().equals(atom.getRelationName())).
									forEach(schemaEntry -> {
										var attributes = new ArrayList<String>(schemaEntry .getValue().keySet());
										atom.getTerms().remove(attributes.indexOf(attribute));
										atom.getTerms().add(attributes.indexOf(attribute) , bodyTerm);										
									});																
							}								
						}	
					}					
				}
			}
		}						
	}
	
	/**
	 * This implementation of the disjunctive CHASE does not compute the complete CHASE tree.
	 * The individual STTgds in the disjunctive sets are completely chased in, 
	 * instead of executing one trigger for them at a time. 
	 * For the optimized disjunctive sets the result should be the same.
	 * @param targetInstance
	 * @param inverse
	 * @return result instances
	 */
	static LinkedHashSet<Instance> runAdaptedDisjunvtiveChase(Instance targetInstance, InverseSchemaMapping inverse){
		var resultInstances = new LinkedHashSet<Instance>();
		
		// start recursive executions of the chase
		resultInstances = runAdaptedDisjunctiveChaseRec(targetInstance, inverse);
		
		// remove the atoms of the global database and their relations from the schema
		for(Instance instance : resultInstances) {
			for(String key : inverse.getSourceSchema().keySet()) {
				instance.getRelationalAtoms().removeAll(instance.getRelationalAtomsBySchema(key));
				instance.getSchema().remove(key);	
			}
		}
		
		return resultInstances;
	}
	
	/**
	 * recursive calculation of the disjunctive chase
	 * one sttgd of the disjunctive sets is applied to each instance
	 * multiple sttgds in a disjunctive set create multiple instances
	 * @param instance
	 * @param inverse
	 * @return intermediate instances
	 */
	static LinkedHashSet<Instance> runAdaptedDisjunctiveChaseRec(Instance instance,  InverseSchemaMapping inverse){
		
		var chaseResultInstances = new LinkedHashSet<Instance>();
		var resultInstances = new LinkedHashSet<Instance>();

		// termination condition of the recursion
		if(inverse.getDisjunctiveSets().isEmpty()) {
			resultInstances.add(instance);
			return resultInstances;
		}
					
		// select the first disjunctive set and start the chase for each sttgd individually
		var disjunctiveSet = inverse.getDisjunctiveSets().entrySet().stream().findFirst().get();
		var disjunctiveSetRelation = disjunctiveSet.getKey();
		var sttgds = disjunctiveSet.getValue();
		
		for (STTgd sttgd : sttgds) {
			var constraints = new LinkedHashSet<Constraint>();
			// tgd instead sttgd to preserve the source tuples in chateau chase
			constraints.add(new Tgd(sttgd.getBody(), sttgd.getHead()));
			// run chase and add chase result
			chaseResultInstances.add(GalveUtil.runChase(new SingleInput(instance, constraints)));
		}
		
		// copy the disjunctive sets to not disturb the recursion
		var disjunctiveSetsCopy = GalveUtil.copyDisjunktiveSets(inverse.getDisjunctiveSets());
		
		// remove the used disjunctive set 
		disjunctiveSetsCopy.remove(disjunctiveSetRelation);
		
		var inverseCopy = new InverseSchemaMapping(inverse.getSourceSchema(),inverse.getSourceSchema(), disjunctiveSetsCopy); 		
		
		// recursion for each instance created by each sttgds
		for(Instance chaseResultInstance : chaseResultInstances) {
			resultInstances.addAll(runAdaptedDisjunctiveChaseRec(chaseResultInstance, inverseCopy));
		}
		
		return resultInstances;
	}
	
	/**
	 * calculate the safe instance as the average under a homomorphism from a set of instances 
	 * @param instances
	 * @return safe instance
	 */
	static Instance safeInstance(LinkedHashSet<Instance> instances) {
		
		var resultInstance = new Instance(OriginTag.INSTANCE);
		// homomorphism mapping the atoms of the instances to the atoms in the result			
		// homomorphism collects the mappings over all computations
		var homomorphism = new HashSet<TermMapping>();
		// the flag to not restart the calculation
		var breakWhile = true;
		
		do {
			// set the flag to not restart the calculation
			breakWhile = true;
			// stores an atom with a conflicting null value
			var toRemove = new RelationalAtom();
			// instance with an atom with conflicting null value
			var instanceForRemove = new Instance(OriginTag.INSTANCE);
			
			resultInstance = new Instance(OriginTag.INSTANCE);
			
			if(instances.size()>0) {
				var firstInstance = instances.stream().findFirst().get();
				
				// disjunctive null values are important for the average calculation
				GalveUtil.makeDisjunctNulls(instances);
				
				// search for each atom in the first instance for matching atoms in the other instances
				loop1: for(RelationalAtom outerAtom : firstInstance.getRelationalAtoms()) {																	
					
					var mappedOuterAtom = GalveUtil.applyTermMappings(outerAtom, homomorphism);				
					
					for(Instance instance : instances) {
						
						if(!instance.equals(firstInstance)) {											
							var found = false;
							
							for(RelationalAtom innerAtom : instance.getRelationalAtoms()) {
								
								// if an atom already contains conflicting null values it will be deleted 
								if(outerAtom.hasHomomorphismTo(innerAtom) 
										&& GalveUtil.hasConflictingNulls(outerAtom, homomorphism)) {
									// add atom with conflicting null value 								
									toRemove = outerAtom;
									
									// note the instance containing the atom with the conflicting null value
									instanceForRemove = firstInstance;
									
									// set the flag to restart the calculation
									breakWhile = false;

									// extend homomorphism to be able to recognize later conflicts
									GalveUtil.addTermMapping(homomorphism, outerAtom, innerAtom);
									
									// Cancel the calculation
									break loop1;
								}
								// if an atom already contains conflicting null values it will be deleted
								if(innerAtom.hasHomomorphismTo(outerAtom) && 
										GalveUtil.hasConflictingNulls(innerAtom, homomorphism)) {
									// add atom with conflicting null value 								
									toRemove = innerAtom;
									
									// note the instance containing the atom with the conflicting null value
									instanceForRemove = instance;
									
									// set the flag to restart the calculation
									breakWhile = false;

									// extend homomorphism to be able to recognize later conflicts
									GalveUtil.addTermMapping(homomorphism, innerAtom, outerAtom);
									
									// Cancel the calculation
									break loop1;
								}
							
								var mappedInnerAtom = GalveUtil.applyTermMappings(innerAtom, homomorphism);
								
								// homomorphism can be extended
								if(GalveUtil.homomorphismIsExpandable(homomorphism, outerAtom, innerAtom)) {
									// extend homomorphism
									GalveUtil.addTermMapping(homomorphism, mappedOuterAtom, mappedInnerAtom);
									
									found = true;		
								}else 
								// homomorphism can be extended
								if(GalveUtil.homomorphismIsExpandable(homomorphism, innerAtom, outerAtom)) {									
									// extend homomorphism
									GalveUtil.addTermMapping(homomorphism, mappedInnerAtom, mappedOuterAtom);							
									
									found = true;	
								}else 
								// homomorphism exists, but existing homomorphism cannot be extended
								if(outerAtom.hasHomomorphismTo(innerAtom)) {							
									// add atom with conflicting null value 								
									toRemove = outerAtom;
									
									// note the instance containing the atom with the conflicting null value
									instanceForRemove = firstInstance;
									
									// set the flag to restart the calculation
									breakWhile = false;

									// extend homomorphism to be able to recognize later conflicts
									GalveUtil.addTermMapping(homomorphism, outerAtom, innerAtom);
									// Cancel the calculation
									break loop1;
								}else 
								// homomorphism exists, but existing homomorphism cannot be extended
								if(innerAtom.hasHomomorphismTo(outerAtom)) {
									// add atom with conflicting null value
									toRemove = innerAtom;
									
									// note the instance containing the atom with the conflicting null value
									instanceForRemove = instance;
									
									// set the flag to restart the calculation
									breakWhile = false;
									
									// extend homomorphism to be able to recognize later conflicts
									GalveUtil.addTermMapping(homomorphism, innerAtom, outerAtom);
									// Cancel the calculation
									break loop1;
									}
								}
							if(!found) {
								continue loop1;
							}
						}					
					}	
					// atom was found everywhere and can be added to the result
					resultInstance.getRelationalAtoms().add(GalveUtil.applyTermMappings(outerAtom, homomorphism));				
				}			
				if(breakWhile) {
					// apply the homomorphism to all atoms already added to the result
					resultInstance = GalveUtil.applyTermMappings(resultInstance, homomorphism);	
				}else {
					// find the instance with conflicting null value and remove the corresponding atom
					for(Instance instance : instances) {
						if(instance.equals(instanceForRemove)) {
							instance.getRelationalAtoms().remove(toRemove);
						}
					}
				}				
			}	
			// remove the atom and start again
			// removing the atom from the result is not sufficient, 
			// otherwise the same atom could be deleted from other mappings as well
		}while(!breakWhile);	
				
		return resultInstance;
	}	
	
	/**
	 * calculates tgd, which copies the new tuples into the original relations
	 * @param originSchema
	 * @param extendedSchema
	 * @param relationKeyAttribtes
	 * @return tgds for extension follow-up
	 */
	static LinkedHashSet<Constraint> followUpTgds(HashMap<String, LinkedHashMap<String, String>> originSchema, 
			HashMap<String, LinkedHashMap<String, String>> extendedSchema,
			HashMap<String, LinkedHashSet<String>> relationKeyAttribtes){
		
		var followUpTgds = new LinkedHashSet<Constraint>();
		
		for(Entry<String, LinkedHashMap<String, String>> extendedSchemaEntry : extendedSchema.entrySet()) {
			// consider only extended relations
			if(!originSchema.containsKey(extendedSchemaEntry.getKey())) {								
				
				var extendedRelationName = extendedSchemaEntry.getKey();
				var extendedAttributes = new ArrayList<String>(extendedSchemaEntry.getValue().keySet());
				
				// create tgd from new extended relation to corresponding original relation
				var tgdBody = new LinkedHashSet<RelationalAtom>();
				var tgdBodyAtom = new RelationalAtom(extendedRelationName);
				
				for(String attribut : extendedAttributes) {
					tgdBodyAtom.getTerms().add(new Variable(VariableType.FOR_ALL, attribut, 1));
				}
				tgdBody.add(tgdBodyAtom);
				
				var originRelationName = extendedRelationName.substring(0, extendedRelationName.length()-1);
				var originAttributes = new ArrayList<String>(originSchema.get(originRelationName).keySet());
				
				var tgdHead = new LinkedHashSet<RelationalAtom>();
				var tgdHeadAtom = new RelationalAtom(originRelationName);
				
				for(String attribut : originAttributes) {
					tgdHeadAtom.getTerms().add(new Variable(VariableType.FOR_ALL, attribut, 1));
				}
				tgdHead.add(tgdHeadAtom);
				
				followUpTgds.add(new Tgd(tgdBody,tgdHead));				
			}
		}
		return followUpTgds;
	}	
	
	/**
	 * calculates the begds that clean the new and original tuples in the original relations
	 * @param originSchema
	 * @param extendedSchema
	 * @param relationKeyAttribtes
	 * @return begds for extension follow-up
	 */
	static LinkedHashSet<Constraint> followUpBEgds(HashMap<String, LinkedHashMap<String, String>> originSchema, 
			HashMap<String, LinkedHashMap<String, String>> extendedSchema,
			HashMap<String, LinkedHashSet<String>> relationKeyAttribtes){
		
		var followUpBEgds = new LinkedHashSet<Constraint>();
		
		for(Entry<String, LinkedHashMap<String, String>> extendedSchemaEntry : extendedSchema.entrySet()) {
			// consider only extended relations
			if(!originSchema.containsKey(extendedSchemaEntry.getKey())) {								
				
				var extendedRelationName = extendedSchemaEntry.getKey();
				var originRelationName = extendedRelationName.substring(0, extendedRelationName.length()-1);
				var originAttributes = new ArrayList<String>(originSchema.get(originRelationName).keySet());
				
				// create bEgd to equate all non-key attributes 
				var keyAttributes = relationKeyAttribtes.get(originRelationName);
				
				// there are more attributes than key attributes
				if(originAttributes.size() > keyAttributes.size()) {
					
					var bEgdBody = new LinkedHashSet<RelationalAtom>();
					var bEgdBodyAtom1 = new RelationalAtom(originRelationName);
					var bEgdBodyAtom2 = new RelationalAtom(originRelationName);
					
					var bEgdHead = new LinkedHashSet<EqualityAtom>();
					
					for(int i = 0; i < originAttributes.size(); i++) {
						var attribute = originAttributes.get(i);
						// select according to the key attributes
						if(keyAttributes.contains(attribute)) {
							bEgdBodyAtom1.getTerms().add(new Variable(VariableType.FOR_ALL, attribute, 1));
							bEgdBodyAtom2.getTerms().add(new Variable(VariableType.FOR_ALL, attribute, 1));
						}else {
							bEgdBodyAtom1.getTerms().add(new Variable(VariableType.FOR_ALL, attribute, 1));
							bEgdBodyAtom2.getTerms().add(new Variable(VariableType.FOR_ALL, attribute, 2));
							
							bEgdHead.add(new EqualityAtom(new Variable(VariableType.FOR_ALL, attribute, 1), 
														 new Variable(VariableType.FOR_ALL, attribute, 2)));
						}					
					}
					bEgdBody.add(bEgdBodyAtom1);
					bEgdBody.add(bEgdBodyAtom2);
					
					followUpBEgds.add(new BEgd(bEgdBody, bEgdHead));	
				}				
			}
		}
		return followUpBEgds;
	}	
}