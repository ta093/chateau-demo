package io;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This class can be seen as a wrapper around {@link Logger}. It reduces it to
 * only a few methods and adds methods for easier string formatting.
 * 
 * @author Nic Scharlau
 */
public class ChateauLogger {
    private static Logger logger;
    private static ArrayList<LogRecord> records;

    /**
     * Constructor
     */
    public ChateauLogger() {
        init();
    }

    /**
     * Initializes the logger.
     */
    private static void init() {
        logger = Logger.getLogger("ChaTEAU Log");
        records = new ArrayList<LogRecord>();

        logger.addHandler(new Handler() {
            public void publish(LogRecord record) {
                records.add(record);
            }

            public void flush() {
            }

            public void close() {
            }
        });
    }

    /**
     * Gets a list of all records.
     * 
     * @return An {@link ArrayList} containing {@link LogRecord}s.
     */
    public static ArrayList<LogRecord> getRecords() {
        return records;
    }

    /**
     * Logs a message.
     * 
     * @param level   The {@link Level} of the message, e.g. INFO or WARNING.
     * @param message The actual message to log.
     */
    public static void log(Level level, String message) {
        if (logger == null) {
            init();
        }

        logger.log(level, message);
    }

    /**
     * Converts a {@link LogRecord} into a String.
     * 
     * @param record The {@link LogRecord} that shall be converted to a string.
     * @return A formatted version of the log record.
     */
    public static String recordToString(LogRecord record) {
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date logDate = new Date(record.getMillis());

        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(formatter.format(logDate));
        sb.append("] [");
        sb.append(record.getLevel());
        sb.append("] ");
        sb.append(record.getMessage());

        return sb.toString();
    }
}