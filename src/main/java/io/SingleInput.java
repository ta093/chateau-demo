package io;

import java.util.LinkedHashSet;

import constraints.Constraint;
import instance.Instance;

public class SingleInput {
	private final Instance instance;
	private final LinkedHashSet<Constraint> constraints;

	/**
	 * Constructor. Initialites a new {@link SingleInput}.
	 * 
	 * @param instance    The incoming {@link Instance}.
	 * @param constraints The incoming {@link Constraint}s, as
	 *                    {@link LinkedHashSet}.
	 */
	public SingleInput(Instance instance, LinkedHashSet<Constraint> constraints) {
		this.instance = instance;
		this.constraints = constraints;
	}

	/**
	 * Gets the {@link Instance} the input is holding.
	 * 
	 * @return The {@link Instance} the input is holding.
	 */
	public Instance getInstance() {
		return this.instance;
	}

	/**
	 * Gets the {@link Constraint}s the input is holding.
	 * 
	 * @return The {@link Constraint}s the input is holding, as
	 *         {@link LinkedHashSet}.
	 */
	public LinkedHashSet<Constraint> getConstraints() {
		return this.constraints;
	}
}
