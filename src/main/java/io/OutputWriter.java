package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.logging.LogRecord;

import javax.management.Query;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import atom.EqualityAtom;
import atom.RelationalAtom;
import constraints.Egd;
import constraints.Constraint;
import constraints.STTgd;
import constraints.Tgd;
import instance.Instance;
import instance.SchemaTag;
import term.Constant;
import term.Null;
import term.Term;
import term.Variable;

public class OutputWriter {

	/*
	 * Variable that preserves the source schemas
	 */
	public boolean preserveSourceSchema = false;
	/*
	 * Variable to output provenance information
	 */
	public boolean outputProvenance = false;

	private String origin = null;

	/**
	 * constructor
	 */
	public OutputWriter() {

	}

	/**
	 * Writes a SingleInput into an XML file.
	 *
	 * @param instance The {@link Instance} that shall be written.
	 * @param file     The file to write the SingleInput to.
	 * @throws IOException If file can't be created or {@link PrintWriter} can't
	 *                     write to file
	 */
	public void writeXML(SingleInput input, File file) throws IOException {
		writeXML(input.getInstance(), null, input.getConstraints(), file);
	}

	/**
	 * Writes an instance into an XML file.
	 *
	 * @param instance The {@link Instance} that shall be written.
	 * @param file     The file to write the instance to.
	 * @throws IOException If file can't be created or {@link PrintWriter} can't
	 *                     write to file
	 */
	public void writeXML(Instance instance, File file) throws IOException {
		writeXML(instance, null, null, file);
	}

	/**
	 * Writes an instance and a query into an XML file.
	 *
	 * @param instance  The {@link Instance} that shall be written.
	 * @param @Nullable query The {@link Query} that shall be written.
	 * @param file      The file to write instance and query to.
	 * @throws IOException If file can't be created or {@link PrintWriter} can't
	 *                     write to file
	 */
	public void writeXML(Instance instance, STTgd query, File file) throws IOException {
		writeXML(instance, query, null, file);
	}

	/**
	 * Writes an instance, a query and constraints into an XML file.
	 *
	 * @param instance  The {@link Instance} that shall be written.
	 * @param @Nullable query The {@link Query} that shall be written.
	 * @param @Nullable constraints the dependencies (if it is a chase SingleInput
	 *                  that shall be written).
	 * @param file      The file to write instance and query to.
	 * @throws IOException If file can't be created or {@link PrintWriter} can't
	 *                     write to file
	 */
	public void writeXML(Instance instance, STTgd query, LinkedHashSet<Constraint> constraints, File file)
			throws IOException {
		Document doc = new Document();
		Element root = new Element("input");

		if (this.origin != null) {
			root.setAttribute("origin", this.origin);
		}

		doc.setRootElement(root);

		Element schemaElement = new Element("schema");
		Element relationsElement = new Element("relations");
		Element dependenciesElement = new Element("dependencies");

		// elements for query
		Element queryElement = new Element("query");
		Element queryBodyElement = new Element("body");
		Element queryHeadElement = new Element("head");

		// element for instance
		Element instanceElement = new Element("instance");

		// element for provenance
		Element provenanceElement = new Element("provenance");

		schemaElement.addContent(relationsElement);
		schemaElement.addContent(dependenciesElement);
		root.addContent(schemaElement);

		if (query != null) {
			queryElement.addContent(queryBodyElement);
			queryElement.addContent(queryHeadElement);
			root.addContent(queryElement);
		} else {
			root.addContent(instanceElement);
		}

		if (outputProvenance) {
			root.addContent(provenanceElement);
		}

		HashMap<String, LinkedHashMap<String, String>> schema = instance.getSchema();
		schema.forEach((relation, attributeTypeMap) -> {
			// adopt relations
			if (preserveSourceSchema
					|| (instance.getSchemaTags() != null && !instance.getSchemaTags().isEmpty()
							&& instance.getSchemaTags().containsKey(relation)
							&& instance.getSchemaTags().get(relation).equals(SchemaTag.TARGET))
					|| instance.getSchemaTags() == null || instance.getSchemaTags().isEmpty()) {
				Element relationElement = new Element("relation");
				relationElement.setAttribute("name", relation);
				if (instance.getSchemaTags().containsKey(relation) && preserveSourceSchema) {
					Attribute attributeSchemaTag = new Attribute("tag",
							instance.getSchemaTags().get(relation).getToken());
					relationElement.setAttribute(attributeSchemaTag);

				}

				attributeTypeMap.forEach((attribute, type) -> {
					Element attributeElement = new Element("attribute");
					Attribute attributeName = new Attribute("name", attribute);
					Attribute attributeType = new Attribute("type", type);
					attributeElement.setAttribute(attributeName);
					attributeElement.setAttribute(attributeType);
					relationElement.addContent(attributeElement);
				});

				relationsElement.addContent(relationElement);

				if (query != null) {
					// adopt queryBody
					LinkedHashSet<RelationalAtom> bodyAtoms = query.getBodyAtomsBySchema(relation);
					if (!bodyAtoms.isEmpty()) {
						for (RelationalAtom atom : bodyAtoms) {
							queryBodyElement.addContent(getAtomElement(atom));
							if (outputProvenance) {
								provenanceElement.addContent(getProvenanceElement(atom));
							}
						}
					}
				} else {
					// adopt atoms
					LinkedHashSet<RelationalAtom> atoms = instance.getRelationalAtomsBySchema(relation);
					if (!atoms.isEmpty()) {
						for (RelationalAtom atom : atoms) {
							instanceElement.addContent(getAtomElement(atom));
							if (outputProvenance) {
								provenanceElement.addContent(getProvenanceElement(atom));
							}
						}
					}
				}
			}
		});

		if (query != null) {
			// adopt queryHead
			LinkedHashSet<RelationalAtom> headAtoms = query.getHead();
			if (!headAtoms.isEmpty()) {
				for (RelationalAtom atom : headAtoms) {
					queryHeadElement.addContent(getAtomElement(atom));
				}
			}
		}

		if (constraints != null) {
			constraints.forEach(dep -> {
				if (dep instanceof STTgd) {
					Element constraintElement = new Element("sttgd");
					dependenciesElement.addContent(constraintElement);
					STTgd sttgd = (STTgd) dep;
					Element depBodyElement = new Element("body");
					constraintElement.addContent(depBodyElement);
					LinkedHashSet<RelationalAtom> bodyAtoms = sttgd.getBody();

					for (RelationalAtom atom : bodyAtoms) {
						depBodyElement.addContent(getAtomElement(atom, false));
					}

					Element depHeadElement = new Element("head");
					constraintElement.addContent(depHeadElement);
					LinkedHashSet<RelationalAtom> headAtoms = sttgd.getHead();

					for (RelationalAtom atom : headAtoms) {
						depHeadElement.addContent(getAtomElement(atom, false));
					}

				} else if (dep instanceof Tgd) {
					Element constraintElement = new Element("tgd");
					dependenciesElement.addContent(constraintElement);
					Tgd tgd = (Tgd) dep;

					Element depBodyElement = new Element("body");
					constraintElement.addContent(depBodyElement);
					LinkedHashSet<RelationalAtom> bodyAtoms = tgd.getBody();

					for (RelationalAtom atom : bodyAtoms) {
						depBodyElement.addContent(getAtomElement(atom, false));
					}

					Element depHeadElement = new Element("head");
					constraintElement.addContent(depHeadElement);
					LinkedHashSet<RelationalAtom> headAtoms = tgd.getHead();

					for (RelationalAtom atom : headAtoms) {
						depHeadElement.addContent(getAtomElement(atom, false));
					}
				} else if (dep instanceof Egd) {
					Element constraintElement = new Element("egd");
					dependenciesElement.addContent(constraintElement);
					Egd egd = (Egd) dep;

					Element depBodyElement = new Element("body");
					constraintElement.addContent(depBodyElement);
					LinkedHashSet<RelationalAtom> bodyAtoms = egd.getBody();

					for (RelationalAtom atom : bodyAtoms) {
						depBodyElement.addContent(getAtomElement(atom, false));
					}

					Element depHeadElement = new Element("head");
					constraintElement.addContent(depHeadElement);
					LinkedHashSet<EqualityAtom> headAtoms = egd.getHead();

					for (EqualityAtom atom : headAtoms) {
						depHeadElement.addContent(getEqualityAtomElement(atom));
					}

				}
			});
		}

		XMLOutputter fmt = new XMLOutputter();
		fmt.setFormat(Format.getPrettyFormat());

		// create file and directories if it doesn't exist yet
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}

		PrintWriter writer = new PrintWriter(file);
		fmt.output(doc, writer);
		writer.close();
	}

	/**
	 * Returns the element for an atom
	 * 
	 * @param atom
	 * @return element for atom
	 */
	private Element getAtomElement(RelationalAtom atom) {
		return getAtomElement(atom, true);
	}

	/**
	 * Returns the element for an atom
	 * 
	 * @param atom
	 * @param withAtomID
	 * @return element for atom
	 */
	private Element getAtomElement(RelationalAtom atom, boolean withAtomID) {
		Element atomElement = new Element("atom");
		atomElement.setAttribute("name", atom.getName());

		if (atom.isNegated()) {
			atomElement.setAttribute("negated", "true");
		}

		if (withAtomID) {
			if (atom.getProvInfo().getId() != null) {
				atomElement.setAttribute("id", atom.getProvInfo().getId());
			}
		}

		for (Term term : atom.getTerms()) {
			addTermToAtomElement(atomElement, term);
		}

		return atomElement;
	}

	/**
	 * Returns the element for an equality atom ("egd head atom").
	 * {@link #getAtomElement(RelationalAtom, boolean)}
	 * 
	 * @param atom
	 * @return element for atom
	 */
	private Element getEqualityAtomElement(EqualityAtom atom) {
		Element atomElement = new Element("atom");
		// need LinkedHashSet for deterministic iteration order:
		LinkedHashSet<Term> egdHead = new LinkedHashSet<>();
		egdHead.add(atom.getTerm1());
		egdHead.add(atom.getTerm2());

		for (Term term : egdHead) {
			addTermToAtomElement(atomElement, term);
		}

		return atomElement;
	}

	/**
	 * Adds a Term to an Atom Element.
	 * 
	 * @param atomElement An {@link Element} that represents an {@link Atom}
	 * @param term        The {@link Term} to add
	 */
	private void addTermToAtomElement(Element atomElement, Term term) {
		if (term.isConstant()) {
			Element constantElement = new Element("constant");

			constantElement.setAttribute("name", ((Constant) term).getName());
			constantElement.setAttribute("value", ((Constant) term).getValue().toString());

			atomElement.addContent(constantElement);
		} else if (term.isNull()) {
			Element nullElement = new Element("null");

			nullElement.setAttribute("name", ((Null) term).getIndexName());
			nullElement.setAttribute("index", Integer.toString(((Null) term).getIndex()));

			atomElement.addContent(nullElement);
		} else if (term.isVariable()) {
			Element variableElement = new Element("variable");

			variableElement.setAttribute("name", ((Variable) term).getIndexName());
			variableElement.setAttribute("type", ((Variable) term).getVariableType().getToken());
			variableElement.setAttribute("index", Integer.toString(((Variable) term).getIndex()));

			atomElement.addContent(variableElement);
		}
	}

	/**
	 * Returns the provenance element for an atom
	 * 
	 * @param atom
	 * @return provenance element for atom
	 */
	private Element getProvenanceElement(RelationalAtom atom) {
		Element atomElement = new Element("atom");
		atomElement.setAttribute("name", atom.getName());

		if (atom.getProvInfo().getId() != null) {
			atomElement.setAttribute("id", atom.getProvInfo().getId());
		}

		// where provenance
		Element whereProvenanceElement = new Element("where");
		Element whereTupleElement = new Element("tuples");
		for (var tuple : atom.getProvInfo().getWhereTupleProvenance()) {
			Element tupleElement = new Element("tuple");
			tupleElement.setAttribute("id", tuple);
			whereTupleElement.addContent(tupleElement);
		}
		whereProvenanceElement.addContent(whereTupleElement);

		Element whereRelationsElement = new Element("relations");
		for (var relation : atom.getProvInfo().getWhereRelationsProvenance()) {
			Element relationElement = new Element("relation");
			relationElement.setAttribute("name", relation);
			whereRelationsElement.addContent(relationElement);
		}
		whereProvenanceElement.addContent(whereRelationsElement);

		atomElement.addContent(whereProvenanceElement);

		// why provenance
		Element whyProvenanceElement = new Element("why");
		var whyProvenance = atom.getProvInfo().getWhyProvenance();
		var whyProvenanceSet = new LinkedHashSet<LinkedHashSet<String>>();
		whyProvenance.forEach(witnesses -> {
			var witnessesSet = new LinkedHashSet<String>();
			witnesses.forEach(witness -> witnessesSet.add(witness));
			whyProvenanceSet.add(witnessesSet);
		});
		for (var witnesses : whyProvenanceSet) {
			Element witnessesElement = new Element("witnesses");
			for (var witness : witnesses) {
				Element witnessElement = new Element("witness");
				witnessElement.setAttribute("id", witness);
				witnessesElement.addContent(witnessElement);
			}
			whyProvenanceElement.addContent(witnessesElement);
		}

		// minimal why provenance
		Element minimalWitnessesElement = new Element("minimalWhy");
		var minimalWitnessesSet = new LinkedHashSet<LinkedHashSet<String>>();
		var minimalWitnesses = new LinkedHashSet<String>();
		for (var witnesses : whyProvenanceSet) {

			if (minimalWitnesses.isEmpty()) {
				minimalWitnesses = witnesses;
			} else {
				if (witnesses.size() < minimalWitnesses.size()) {
					minimalWitnessesSet.clear();
					minimalWitnesses = witnesses;
				} else if (witnesses.size() == minimalWitnesses.size()) {
					minimalWitnessesSet.add(witnesses);
				}
			}
		}
		minimalWitnessesSet.add(minimalWitnesses);

		if (minimalWitnessesSet.size() < whyProvenanceSet.size()) {
			for (var witnesses : minimalWitnessesSet) {
				Element witnessesElement = new Element("minimalWitnesses");
				for (var witness : witnesses) {
					Element witnessElement = new Element("minimalWitness");
					witnessElement.setAttribute("id", witness);
					witnessesElement.addContent(witnessElement);
				}
				minimalWitnessesElement.addContent(witnessesElement);
			}

			whyProvenanceElement.addContent(minimalWitnessesElement);
		}

		atomElement.addContent(whyProvenanceElement);

		// how provenance
		Element howProvenanceElement = new Element("how");
		String polynom = atom.getProvInfo().getPolynom();
		howProvenanceElement.setAttribute("polynom", polynom.toString());
		atomElement.addContent(howProvenanceElement);

		return atomElement;
	}

	/**
	 * Sets the origin of the input.
	 * 
	 * @param origin The origin, e.g. "prosa".
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Gets the origin.
	 * 
	 * @return The origin, e.g. "prosa".
	 */
	public String getOrigin() {
		return this.origin;
	}

	/**
	 * Writes a ChaseLog to a file.
	 *
	 * @param log  The {@link io.ChaseLog ChaseLog} to be written.
	 * @param file The {@link java.io.File File} to be created.
	 * @throws FileNotFoundException If file can't be created
	 */
	public void writeLog(File file) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(file);

		for (LogRecord record : ChateauLogger.getRecords()) {
			writer.write(ChateauLogger.recordToString(record));
			writer.write(System.lineSeparator());
		}

		writer.close();
	}
}
