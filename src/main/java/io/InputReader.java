package io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import functions.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import atom.EqualityAtom;
import atom.ProvenanceInformation;
import atom.RelationalAtom;
import comparisons.Comparison;
import comparisons.ComparisonType;
import comparisons.EqualsComparison;
import comparisons.GreaterComparison;
import comparisons.GreaterOrEqualComparison;
import comparisons.LessComparison;
import comparisons.LessOrEqualComparison;
import comparisons.NotEqualsComparison;
import constraints.Constraint;
import constraints.Egd;
import constraints.STTgd;
import constraints.ScopeType;
import constraints.Tgd;
import instance.Instance;
import instance.OriginTag;
import instance.SchemaTag;
import term.ConstType;
import term.Constant;
import term.Null;
import term.Term;
import term.Variable;
import term.VariableType;

public class InputReader {

	private final SAXBuilder jdomBuilder;
	private HashMap<String, LinkedHashMap<String, String>> schema;
	private boolean sortTerms;

	public InputReader() {
		this.jdomBuilder = new SAXBuilder();
		this.sortTerms = false;
	}

	public SingleInput readFile(File file) throws JDOMException, IOException {
		Document doc = jdomBuilder.build(file);
		Element root = doc.getRootElement();

		// enable term sorting if file was created automatically by sql2sttgd
		this.sortTerms = root.getAttributeValue("origin") != null;

		Element schemaElement = root.getChild("schema").getChild("relations");
		Element instanceElement = root.getChild("instance");
		Element queryElement = root.getChild("query");
		Element dependenciesElement = root.getChild("schema").getChild("dependencies");

		this.schema = this.getSchema(schemaElement);
		HashMap<String, SchemaTag> schemaTags = this.getSchemaTags(schemaElement);
		LinkedHashSet<Constraint> constraints = this.getConstraints(dependenciesElement);

		if (instanceElement != null) {
			// read instance
			LinkedHashSet<RelationalAtom> instanceAtoms = this.getRelationalAtoms(instanceElement);
			Instance instance = new Instance(instanceAtoms, this.schema, OriginTag.INSTANCE, schemaTags);
			instance.setRelationKeyAttributes(getRelationKeyAttributes(schemaElement));

			return new SingleInput(instance, constraints);
		} else if (queryElement != null) {
			// read query
			STTgd query = this.getQuery(queryElement);
			var instanceAtoms = new LinkedHashSet<RelationalAtom>();

			// fill InstanceAtoms with QueryBody
			instanceAtoms.addAll(query.getBody());

			Instance instance = new Instance(instanceAtoms, schema, OriginTag.QUERY, schemaTags, query.getHead());

			// QueryCheck with Query from the Instance (= Head from every #V variable)
			if (!instance.queryCheck()) {
				System.out.println("The query does not satisfy the restriction of the tableaus:");
				System.out.println("An all quantified variable may only be used for one attribute!");
				System.out.println("The CHASE is continued. A correct result is not guaranteed!\n");
			}

			return new SingleInput(instance, constraints);
		} else {
			Instance instance = new Instance(null, this.schema, OriginTag.INSTANCE, schemaTags);
			return new SingleInput(instance, constraints);
		}
	}

	/**
	 * Reads a ByteArrayOutputStream by creating a temp file.
	 * 
	 * @param stream The {@link ByteArrayOutputStream} containing the needed data as
	 *               XML.
	 * @return A {@link SingleInput} object from that stream.
	 * @throws IOException   If temp file cannot be created
	 * @throws JDOMException If XML is mis-formatted
	 */
	public SingleInput readStream(ByteArrayOutputStream stream) throws IOException, JDOMException {
		File file = File.createTempFile("streamInput", "xml");
		FileOutputStream outputStream = new FileOutputStream(file);
		stream.writeTo(outputStream);

		return this.readFile(file);
	}

	/**
	 * Gets the relational atoms from a JDOM {@link Element}.
	 *
	 * @param element The JDOM {@link Element}.
	 * @return The relational atoms {@link RelationalAtom}s, as
	 *         {@link LinkedHashSet}.
	 */
	private LinkedHashSet<RelationalAtom> getRelationalAtoms(Element element) {
		var atoms = new LinkedHashSet<RelationalAtom>();
		List<Element> atomElements = element.getChildren("atom");

		for (Element atomElement : atomElements) {
			atoms.add(getRelationalAtom(atomElement));
		}

		return atoms;
	}

	/**
	 * Gets the relational atom from a JDOM {@link Element}.
	 * 
	 * @param atomElement The JDOM {@link Element}.
	 * param sort        If true, terms will be sorted in order to match the
	 *                    schema. Not advised for queries!
	 * @return The relational atom {@link RelationalAtom}.
	 */
	private RelationalAtom getRelationalAtom(Element atomElement) {
		String name = atomElement.getAttributeValue("name");
		String negationString = atomElement.getAttributeValue("negation");
		String idString = atomElement.getAttributeValue("id");

		boolean negation = negationString != null && negationString.equalsIgnoreCase("true");

		ArrayList<Term> terms = new ArrayList<>();

		List<Element> elements = atomElement.getChildren();
		LinkedHashMap<String, String> types = null;

		// as long as the header of queries can still be unnamed this must be checked
		// here
		// if queries are written as s-t tgds this is no longer necessary
		if (schema.containsKey(name)) {
			types = this.schema.get(name);
		}

		for (Element element : elements) {
			Term term = getTerm(element);

			// as long as the header of queries can still be unnamed this must be checked
			// if queries are written as s-t tgds this is no longer necessary
			// constants in queries are therefore also always interpreted as strings
			if (term.isConstant() && types != null) {
				ConstType type = ConstType.valueOf(types.get(term.getName()).toUpperCase());
				terms.add(changeConstType(term, type));
			} else {
				terms.add(term);
			}
		}

		if (sortTerms) {
			// sort terms in order to match schema
			terms = sortTerms(terms, types.keySet().toArray(new String[0]));
		}

		return new RelationalAtom(name, terms, negation, false, new ProvenanceInformation(idString));
	}

	/**
	 * Extracts a {@link Function} from an {@link Element}.
	 * 
	 * @param element The element that contains the function
	 * @return A new function, or null
	 */
	private Function<?> getFunction(Element element) {
		// operator / function type
		String operator = element.getAttributeValue("operator");

		// left and right XML representation
		Element leftElement = element.getChild("left").getChildren().get(0);
		Element rightElement = element.getChild("right").getChildren().get(0);

		// throw exception is left element is not a variable
		if (!leftElement.getName().equals("variable")) {
			throw new UnsupportedOperationException("Left side of function has to be a variable");
		}

		Variable left = getVariable(leftElement);

		switch (rightElement.getName()) {
			case "variable":
				Variable rightVariable = getVariable(rightElement);
				return createFunction(operator, left, rightVariable);

			case "number":
				double rightDouble = Double.parseDouble(rightElement.getAttributeValue("value"));
				return createFunction(operator, left, rightDouble);

			case "function":
				Function<?> rightFunction = getFunction(rightElement);
				return createFunction(operator, left, rightFunction);

			default:
				throw new UnsupportedOperationException(
						"Unsupported right side of function:" + rightElement.getName());
		}
	}

	/**
	 * Creates a function from a given operator name, variable, and double,
	 * variable, or function.
	 * 
	 * @param <Any>
	 * @param operator
	 * @param left
	 * @param right
	 * @return
	 */
	private <Any> Function<Any> createFunction(String operator, Variable left, Any right) {
		// check type of "right"
		if (!(right instanceof Double || right instanceof Variable || right instanceof Function<?>)) {
			throw new IllegalArgumentException(
					"Unsupported right side of function: " + right.getClass().getCanonicalName());
		}

		switch (operator) {
			case "addition":
				return new Addition<>(left, right);

			case "subtraction":
				return new Subtraction<>(left, right);

			case "multiplication":
				return new Multiplication<>(left, right);

			case "division":
				return new Division<>(left, right);

			case "concatenation":
				return new Concatenation<>(left, right);

			default:
				throw new UnsupportedOperationException("Unsupported function operator: " + operator);
		}
	}

	/**
	 * Sorts a list of terms after a specified schema.
	 * 
	 * @param terms  The list of unsorted terms
	 * @param schema The schema of a relation, in other words a list of attribute
	 *               names
	 * @return A sorted list of terms that match the given schema
	 */
	private ArrayList<Term> sortTerms(ArrayList<Term> terms, String[] schema) {
		var sortedTerms = new ArrayList<Term>();
		for (String attributeName : schema) {
			for (Term term : terms) {
				if (term.getName().equals(attributeName)) {
					sortedTerms.add(term);
				}
			}
		}

		return sortedTerms;
	}

	/**
	 * Converts an JDOM element into a term.
	 *
	 * @param element The {@link org.jdom2.Element Element} that represents a
	 *                {@link term.Term Term}.
	 * @return The new, extracted/converted term.
	 */
	private Term getTerm(Element element) {
		switch (element.getName()) {
			case "constant":
				return getConstant(element);

			case "null":
				return getNull(element);

			case "variable":
				return getVariable(element);

			default:
				throw new IllegalArgumentException();
		}
	}

	/**
	 * Parses a JDOM element to a Constant.
	 * 
	 * @param element The JDOM element to parse
	 * @return new Constant
	 */
	private Term getConstant(Element element) {
		String name = element.getAttributeValue("name");
		String value = element.getAttributeValue("value");

		return Constant.fromString(name, value);
	}

	/**
	 * Parses a JDOM element to a Null.
	 * 
	 * @param element The JDOM element to parse
	 * @return new Null
	 */
	private Null getNull(Element element) {
		String name = element.getAttributeValue("name");
		int index = getIndex(element);

		return new Null(name, index);
	}

	/**
	 * Parses a JDOM element to a Variable.
	 * 
	 * @param element The JDOM element to parse
	 * @return new Variable
	 */
	private Variable getVariable(Element element) {
		String name = element.getAttributeValue("name");
		String type = element.getAttributeValue("type");
		int index = getIndex(element);

		if (type == null) {
			type = "V";
		}

		switch (type) {
			case "V":
			default:
				return new Variable(VariableType.FOR_ALL, name, index);

			case "E":
				return new Variable(VariableType.EXISTS, name, index);
		}
	}

	/**
	 * Gets the index of an element.
	 * 
	 * @param element The JDOM element to parse
	 * @return The index (default: 1)
	 */
	private int getIndex(Element element) {
		String elementIndexString = element.getAttributeValue("index");
		int elementIndex = 1;

		if (elementIndexString != null) {
			try {
				elementIndex = Integer.parseInt(elementIndexString);
			} catch (NumberFormatException ignored) {
				// index = 1 by default
			}
		}
		return elementIndex;
	}

	/**
	 * change the type of the constant depending on the type of the attribute which
	 * the constant represents
	 * 
	 * @param constTerm
	 * @param constType
	 * @return constant with correct type
	 */
	private Term changeConstType(Term constTerm, ConstType constType) {
		switch (constType) {
			case INT:
				constTerm = (Constant.fromInteger(constTerm.getName(),
						Integer.parseInt((String) ((Constant) constTerm).getValue())));
				break;

			case DOUBLE:
				constTerm = (Constant.fromDouble(constTerm.getName(),
						Double.parseDouble((String) ((Constant) constTerm).getValue())));
				break;

			case STRING:
				constTerm = (Constant.fromString(constTerm.getName(), (String) ((Constant) constTerm).getValue()));
				break;
		}

		return constTerm;
	}

	/**
	 * Extracts a {@link STTgd} from a JDOM {@link Element}.
	 *
	 * @param queryElement ...
	 * @return The extracted {@link STTgd}.
	 */
	private STTgd getQuery(Element queryElement) {
		Element bodyElement = queryElement.getChild("body");
		Element headElement = queryElement.getChild("head");

		LinkedHashSet<RelationalAtom> queryBody = getRelationalAtoms(bodyElement);
		LinkedHashSet<RelationalAtom> queryHead = getRelationalAtoms(headElement);

		return new STTgd(queryBody, queryHead);
	}

	/**
	 * Gets the schema from a JDOM {@link Element}.
	 *
	 * @param schemaElement The JDOM {@link Element} holding the schema.
	 * @return The schema, as {@link HashMap} mapping strings to an
	 *         {@link ArrayList} of strings.
	 */
	private HashMap<String, LinkedHashMap<String, String>> getSchema(Element schemaElement) {
		var schema = new HashMap<String, LinkedHashMap<String, String>>();
		List<Element> relationElements = schemaElement.getChildren("relation");

		for (Element relationElement : relationElements) {
			String name = relationElement.getAttributeValue("name");
			var attributes = new LinkedHashMap<String, String>();

			List<Element> attributeElements = relationElement.getChildren("attribute");

			for (Element attributeElement : attributeElements) {
				attributes.put(attributeElement.getAttributeValue("name"), attributeElement.getAttributeValue("type"));
			}

			schema.put(name, attributes);
		}

		return schema;
	}

	/**
	 * Gets the keyAttributes from a JDOM {@link Element}.
	 *
	 * @param schemaElement The JDOM {@link Element} holding the schema.
	 * @return The keyAttributes, as {@link LinkedHashSet} mapping strings for key
	 *         attributes.
	 */
	private HashMap<String, LinkedHashSet<String>> getRelationKeyAttributes(Element schemaElement) {
		var relationKeyAttributes = new HashMap<String, LinkedHashSet<String>>();
		List<Element> relationElements = schemaElement.getChildren("relation");

		for (Element relationElement : relationElements) {
			String name = relationElement.getAttributeValue("name");
			var keyAttributes = new LinkedHashSet<String>();

			List<Element> attributeElements = relationElement.getChildren("attribute");

			for (Element attributeElement : attributeElements) {
				String keyAttribute = attributeElement.getAttributeValue("key");
				if (keyAttribute != null && keyAttribute.equals("true")) {
					keyAttributes.add(attributeElement.getAttributeValue("name"));
				}
			}

			relationKeyAttributes.put(name, keyAttributes);
		}

		return relationKeyAttributes;
	}

	/**
	 * Gets the schema tags out of a JDOM {@link Element}.
	 *
	 * @param schemaTagElement The JDOM {@link Element} holding the schema tags.
	 * @return Schema tags, as {@link HashMap} from strings to {@link SchemaTag}s.
	 */
	private HashMap<String, SchemaTag> getSchemaTags(Element schemaTagElement) {
		var schemaTags = new HashMap<String, SchemaTag>();
		List<Element> relationElements = schemaTagElement.getChildren("relation");

		for (Element relationElement : relationElements) {
			String name = relationElement.getAttributeValue("name");
			if (relationElement.getAttributeValue("tag") != null) {
				String tag = relationElement.getAttributeValue("tag");
				if (tag.contentEquals("S")) {
					schemaTags.put(name, SchemaTag.SOURCE);
				} else if (tag.contentEquals("T")) {
					schemaTags.put(name, SchemaTag.TARGET);
				}
			}
		}

		return schemaTags;
	}

	/**
	 * creates the integrity constraints
	 * 
	 * @param contraintElement
	 * @return integrity constraints
	 */
	private LinkedHashSet<Constraint> getConstraints(Element contraintElement) {
		var constraints = new LinkedHashSet<Constraint>();
		List<Element> elements = contraintElement.getChildren();

		for (Element element : elements) {
			Element bodyElement = element.getChild("body");
			Element headElement = element.getChild("head");

			// determine scope type (source or target dependency?)
			String scopeTypeString = element.getAttributeValue("tag");
			ScopeType scopeType = null;

			if (scopeTypeString != null) {
				if (scopeTypeString.equalsIgnoreCase("S")) {
					scopeType = ScopeType.SOURCE;
				} else if (scopeTypeString.equalsIgnoreCase("T")) {
					scopeType = ScopeType.TARGET;
				}
			}

			// tgds and s-t tgds
			if (element.getName().equals("tgd") || element.getName().equals("sttgd")) {
				LinkedHashSet<RelationalAtom> body = getRelationalAtoms(bodyElement);
				LinkedHashSet<RelationalAtom> head = getRelationalAtoms(headElement);

				// get comparisons
				LinkedHashSet<Comparison<?>> bodyComparisons = getComparisons(bodyElement);
				LinkedHashSet<Comparison<?>> headComparisons = getComparisons(headElement);

				// add tgd or s-t tgd to constraints
				if (element.getName().equals("tgd")) {
					Tgd tgd = new Tgd(body, head, bodyComparisons, headComparisons);
					tgd.setScopeType(scopeType);
					constraints.add(tgd);
				} else if (element.getName().equals("sttgd")) {
					STTgd sttgd = new STTgd(body, head, bodyComparisons, headComparisons);
					sttgd.setScopeType(scopeType);
					constraints.add(sttgd);
				}
			}

			// egds
			else if (element.getName().equals("egd")) {
				LinkedHashSet<RelationalAtom> body = getRelationalAtoms(bodyElement);
				var head = new LinkedHashSet<EqualityAtom>();

				for (Element headAtomElement : headElement.getChildren("atom")) {
					List<Element> valueElements = headAtomElement.getChildren();

					Element element1 = valueElements.get(0);
					Element element2 = valueElements.get(1);

					Term headTerm1 = getTerm(element1);
					Term headTerm2 = getTerm(element2);

					// constants also type correctly in the egd head
					// better solutions very welcome
					if (headTerm1.isConstant()) {
						if (!headTerm2.isConstant()) {
							var tempTerm = headTerm2.copy();
							var tempAtom = body.stream().filter(atom -> atom.getTerms().contains(tempTerm)).findFirst()
									.get();
							String typeString = new ArrayList<>(this.schema.get(tempAtom.getName()).values())
									.get(tempAtom.getTerms().indexOf(headTerm2));

							ConstType type = ConstType.valueOf(typeString.toUpperCase());
							headTerm1 = changeConstType(headTerm1, type);
						}
					}
					if (!headTerm1.isConstant()) {
						if (headTerm2.isConstant()) {
							var tempTerm = headTerm1.copy();
							var tempAtom = body.stream().filter(atom -> atom.getTerms().contains(tempTerm)).findFirst()
									.get();
							String typeString = new ArrayList<>(this.schema.get(tempAtom.getName()).values())
									.get(tempAtom.getTerms().indexOf(headTerm1));

							ConstType type = ConstType.valueOf(typeString.toUpperCase());
							headTerm2 = changeConstType(headTerm2, type);
						}
					}

					EqualityAtom headAtom = new EqualityAtom(headTerm1, headTerm2);
					head.add(headAtom);
				}

				Egd egd = new Egd(body, head);
				egd.setScopeType(scopeType);
				constraints.add(egd);
			}
		}

		return constraints;
	}

	/**
	 * Gets the comparisons from an {@link Element}.
	 * 
	 * @param element The JDOM element containing the comparisons
	 * @return A set of {@link Comparison comparisons}
	 */
	private LinkedHashSet<Comparison<?>> getComparisons(Element element) {
		LinkedHashSet<Comparison<?>> comparisons = new LinkedHashSet<>();

		Element rootElement = element.getChild("comparisons");
		if (rootElement == null) {
			return comparisons;
		}

		List<Element> comparisonElements = rootElement.getChildren("comparison");

		for (Element comparisonElement : comparisonElements) {
			Comparison<?> comparison = getComparison(comparisonElement);

			if (comparison != null) {
				comparisons.add(comparison);
			}
		}

		return comparisons;
	}

	private Comparison<?> getComparison(Element element) {
		// get both sides of comparison
		Element leftElement = element.getChild("left");
		Element rightElement = element.getChild("right");

		// get operator
		String operator = element.getAttributeValue("operator");

		// get left side, which has to be a variable
		Element leftChild = leftElement.getChild("variable");
		if (leftChild == null) {
			throw new IllegalArgumentException();
		}

		Variable leftSide = getVariable(leftChild);

		// get right side
		Element rightChild = rightElement.getChildren().get(0);

		switch (rightChild.getName()) {
			case "number":
				double rightDouble = Double.parseDouble(rightChild.getAttributeValue("value"));
				return createComparison(operator, leftSide, rightDouble);

			case "variable":
				Variable rightVariable = getVariable(rightChild);
				return createComparison(operator, leftSide, rightVariable);

			case "function":
				Function<?> rightFunction = getFunction(rightChild);
				return createComparison(operator, leftSide, rightFunction);

			default:
				throw new UnsupportedOperationException("Unsupported right side: " + rightChild.getName());
		}
	}

	private <Any> Comparison<Any> createComparison(String operator, Variable left, Any right) {
		ComparisonType comparisonType;

		// determine type of comparison (constant, variable, function)
		if (right instanceof Variable) {
			comparisonType = ComparisonType.VARIABLE;
		} else if (right instanceof Double) {
			comparisonType = ComparisonType.CONSTANT;
		} else if (right instanceof Function<?>) {
			comparisonType = ComparisonType.FUNCTION;
		} else {
			throw new IllegalArgumentException();
		}

		// determine operator
		switch (operator) {
			case "gt":
				return new GreaterComparison<>(comparisonType, left, right);

			case "lt":
				return new LessComparison<>(comparisonType, left, right);

			case "geq":
				return new GreaterOrEqualComparison<>(comparisonType, left, right);

			case "leq":
				return new LessOrEqualComparison<>(comparisonType, left, right);

			case "neq":
				return new NotEqualsComparison<>(comparisonType, left, right);

			case "eq":
				return new EqualsComparison<>(comparisonType, left, right);

			default:
				throw new UnsupportedOperationException("Unsupported operator: " + operator);
		}
	}
}
