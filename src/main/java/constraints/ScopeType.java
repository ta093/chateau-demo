package constraints;

public enum ScopeType {
    SOURCE, TARGET;
}
