package constraints;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import atom.RelationalAtom;
import comparisons.Comparison;
import term.Term;
import term.Variable;
import term.VariableType;
import termination.Adornment;

/**
 * The class represents tgd's and inherits from
 * {@link constraints.Constraint}.
 *
 * @author Martin Jurklies
 */
public class Tgd extends Constraint {

	protected int index;

	/**
	 * constructor
	 * 
	 * @param body
	 * @param head
	 */
	public Tgd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<RelationalAtom> head,
			LinkedHashSet<Comparison<?>> bodyComparisons, LinkedHashSet<Comparison<?>> headComparisons) {
		this.body = body;
		this.head = head;
		this.bodyComparisons = bodyComparisons;
		this.headComparisons = headComparisons;
	}

	/**
	 * Old constructor for backward compatibility
	 * 
	 * @param body
	 * @param head
	 */
	public Tgd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<RelationalAtom> head) {
		this(body, head, new LinkedHashSet<>(), new LinkedHashSet<>());
	}

	/**
	 * @return the tgd index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the tgd index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * Gets the body comparisons of the tgd.
	 * 
	 * @return The set of comparisons included in the tgd's body
	 */
	public LinkedHashSet<Comparison<?>> getBodyComparisons() {
		return this.bodyComparisons;
	}

	/**
	 * Gets the head comparisons of the tgd.
	 * 
	 * @return The set of comparisons included in the tgd's head
	 */
	public LinkedHashSet<Comparison<?>> getHeadComparisons() {
		return this.headComparisons;
	}

	/**
	 * @return the Adornments of all the universally quantified variables in the
	 *         head
	 */
	public LinkedHashSet<Adornment> getAdornmentsForSkolemization() {
		LinkedHashSet<Adornment> skolemOf = new LinkedHashSet<Adornment>();
		for (RelationalAtom a : this.getHead()) {
			for (Term t : a.getTerms()) {
				if (t.isVariable()) {
					Variable v = (Variable) t;
					if (v.getVariableType() == VariableType.FOR_ALL)
						skolemOf.add(t.getAdornment());
				}
			}
		}
		return skolemOf;
	}

	/**
	 * Existentially quantified variables (in ChaTEAU their terms) have an adornment
	 * that depends only on the adornments of the head's universally quantified
	 * variables. If the tgd is bodiless however, the adornment of other variables
	 * does not matter, it is simply b.
	 */
	public void setAdornmentsForSkolemization() {
		LinkedHashSet<Adornment> skolemOf = getAdornmentsForSkolemization();
		for (RelationalAtom a : this.getHead()) {
			for (Term t : a.getTerms()) {
				if (t.isVariable()) {
					Variable v = (Variable) t;
					if (v.getVariableType() == VariableType.EXISTS) {
						if (this.getBody().isEmpty())
							t.setAdornment(new Adornment());
						else
							t.setAdornment(new Adornment(this.getIndex(), t.toString(), skolemOf));
					}
				}
			}
		}
	}

	/**
	 * @return the tgd head
	 */
	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashSet<RelationalAtom> getHead() {
		return (LinkedHashSet<RelationalAtom>) this.head;
	}

	/**
	 * @param head the tgd head to set
	 *//*
		 * public void setHead(HashSet<RelationalAtom> head) { this.head = head; }
		 */

	/**
	 * Adds a relational atom to the head of this tgd.
	 *
	 * @param atom the relational atom to add to the head
	 * @return {@code true} if {@code atom} was added to {@code head}, {@code false}
	 *         otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean addToHead(RelationalAtom atom) {
		return ((LinkedHashSet<RelationalAtom>) this.head).add(atom);
	}

	public Tgd invert() {
		// Construct new Body from Head
		LinkedHashSet<RelationalAtom> newBody = new LinkedHashSet<RelationalAtom>();

		for (RelationalAtom a : (LinkedHashSet<RelationalAtom>) this.getHead()) {
			ArrayList<Term> terms = a.getTerms();
			ArrayList<Term> newTerms = new ArrayList<Term>();

			for (Term t : terms) {
				Term newTerm = t.copy();
				newTerms.add(newTerm);
			}

			newBody.add(new RelationalAtom(a.getName(), newTerms));
		}
		// Construct new Head from Body
		LinkedHashSet<RelationalAtom> newHead = new LinkedHashSet<RelationalAtom>();
		LinkedHashSet<RelationalAtom> oldHead = (LinkedHashSet<RelationalAtom>) this.getHead();
		for (RelationalAtom a : this.getBody()) {
			ArrayList<Term> terms = a.getTerms();
			ArrayList<Term> newTerms = new ArrayList<Term>();
			for (Term t : terms) {
				Term newTerm = t.copy();
				for (RelationalAtom atom : oldHead) {
					if (!(atom.getTerms().contains(t)) && t.isVariable()) {
						int countOccurrences = 0;
						for (RelationalAtom b : this.getBody()) {
							if (b.getTerms().contains(t)) {
								countOccurrences++;
								if (countOccurrences >= 2) {
									break;
								}
							}
						}
						if (countOccurrences >= 2) {
							// currently not supported because the nullCounter for variables during the
							// chase is based on the attributenames
							// newTerm = new Term(t.getTermValueVariable().toString().replaceFirst("#V_",
							// "#E_Prov"));

							// newTerm = new Term(t.getTermValueVariable().toString().replaceFirst("#V",
							// "#E"));
							newTerm = t.copy();

						} else {
							// newTerm = new Term(t.getTermValueVariable().toString().replaceFirst("#V",
							// "#E"));
							newTerm = t.copy();
						}
					}
				}
				newTerms.add(newTerm);
			}

			newHead.add(new RelationalAtom(a.getName(), newTerms));
		}
		return new Tgd(newBody, newHead, new LinkedHashSet<>(), new LinkedHashSet<>());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		String separator = System.lineSeparator();

		for (RelationalAtom atom : this.body) {
			builder.append(separator);
			builder.append(atom.toString());
			separator = "," + System.lineSeparator();
		}

		if (this.bodyComparisons != null) {
			for (Comparison<?> comparison : this.bodyComparisons) {
				builder.append(separator);
				builder.append(comparison.toString());
			}
		}

		builder.append(System.lineSeparator() + "->" + System.lineSeparator());
		separator = "";

		for (var atom : this.head) {
			builder.append(separator);
			builder.append(atom.toString());
			separator = "," + System.lineSeparator();
		}

		if (this.headComparisons != null) {
			for (Comparison<?> comparison : this.headComparisons) {
				builder.append(separator);
				builder.append(comparison.toString());
			}
		}

		return builder.toString();
	}

	@Override
	public String getTypeName() {
		return "tgd";
	}

	@Override
	public Tgd copy() {
		return new Tgd(copyBody(), copyHead(), copyBodyComparisons(), copyHeadComparisons());
	}

	public LinkedHashSet<RelationalAtom> copyBody() {
		var newBody = new LinkedHashSet<RelationalAtom>();

		this.body.forEach(atom -> newBody.add(atom.copy()));

		return newBody;
	}

	public LinkedHashSet<RelationalAtom> copyHead() {
		var newHead = new LinkedHashSet<RelationalAtom>();

		this.head.forEach(atom -> newHead.add((RelationalAtom) atom.copy()));

		return newHead;
	}

	public LinkedHashSet<Comparison<?>> copyBodyComparisons() {
		LinkedHashSet<Comparison<?>> newComp = new LinkedHashSet<>();

		if (this.bodyComparisons != null) {
			this.bodyComparisons.forEach(comparison -> newComp.add(comparison));
		}

		return newComp;
	}

	public LinkedHashSet<Comparison<?>> copyHeadComparisons() {
		LinkedHashSet<Comparison<?>> newComp = new LinkedHashSet<>();

		if (this.headComparisons != null) {
			this.headComparisons.forEach(comparison -> newComp.add(comparison));
		}

		return newComp;
	}
}
