package constraints;

import java.util.LinkedHashSet;

import atom.RelationalAtom;
import comparisons.Comparison;

public class STTgd extends Tgd {
	public STTgd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<RelationalAtom> head) {
		super(body, head, null, null);
	}

	public STTgd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<RelationalAtom> head,
			LinkedHashSet<Comparison<?>> bodyComparisons, LinkedHashSet<Comparison<?>> headComparisons) {
		this(body, head);

		this.bodyComparisons = bodyComparisons;
		this.headComparisons = headComparisons;
	}

	@Override
	public String getTypeName() {
		return "s-t tgd";
	}

	@Override
	public STTgd copy() {
		return new STTgd(copyBody(), copyHead(), copyBodyComparisons(), copyHeadComparisons());
	}
}
