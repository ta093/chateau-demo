package constraints;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import atom.EqualityAtom;
import atom.RelationalAtom;
import term.Term;

/**
 * The class represents egd's and inherits from
 * {@link constraints.Constraint}.
 *
 * @author Martin Jurklies
 */
public class Egd extends Constraint {
	/**
	 * constructor
	 */
	public Egd(LinkedHashSet<RelationalAtom> body, LinkedHashSet<EqualityAtom> head) {
		this.body = body;
		this.head = head;
	}

	/**
	 * @return the head
	 */
	@SuppressWarnings("unchecked")
	public LinkedHashSet<EqualityAtom> getHead() {
		return (LinkedHashSet<EqualityAtom>) this.head;
	}

	/**
	 * Adds an equality atom to the head of this egd.
	 *
	 * @param atom the equality atom to add to the head
	 * @return {@code true} if {@code atom} was added to {@code head}, {@code false}
	 *         otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean addToHead(EqualityAtom atom) {
		return ((LinkedHashSet<EqualityAtom>) this.head).add(atom);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		String sepAtom = "";

		for (RelationalAtom atom : this.body) {
			builder.append(sepAtom);
			builder.append(atom.toString());
			sepAtom = "," + System.lineSeparator();
		}

		builder.append(System.lineSeparator() + "->" + System.lineSeparator());
		sepAtom = "";

		for (EqualityAtom atom : (LinkedHashSet<EqualityAtom>) this.head) {
			builder.append(sepAtom);
			builder.append(atom.toString());
			sepAtom = "," + System.lineSeparator();
		}

		return builder.toString();
	}

	@Override
	public String getTypeName() {
		return "egd";
	}

	@Override
	public Egd copy() {
		var newBody = new LinkedHashSet<RelationalAtom>();
		var newHead = new LinkedHashSet<EqualityAtom>();

		this.body.forEach(bodyAtom -> {
			var newTerms = new ArrayList<Term>();
			bodyAtom.getTerms().forEach(term -> {
				newTerms.add(term.copy());
			});
			newBody.add(new RelationalAtom(bodyAtom.getName(), newTerms));
		});

		this.head.forEach(headAtom -> {
			var equalityHeadAtom = new EqualityAtom((EqualityAtom) headAtom);
			newHead.add(new EqualityAtom(equalityHeadAtom.getTerm1().copy(), equalityHeadAtom.getTerm2().copy()));
		});

		return new Egd(newBody, newHead);
	}

	public LinkedHashSet<RelationalAtom> copyBody() {
		var newBody = new LinkedHashSet<RelationalAtom>();

		this.body.forEach(atom -> newBody.add(atom.copy()));

		return newBody;
	}

	public LinkedHashSet<EqualityAtom> copyHead() {
		var newHead = new LinkedHashSet<EqualityAtom>();

		this.head.forEach(atom -> newHead.add((EqualityAtom) atom.copy()));

		return newHead;
	}
}
