package constraints;

import java.util.HashSet;
import java.util.LinkedHashSet;

import atom.Atom;
import atom.RelationalAtom;
import comparisons.Comparison;

/**
 * The class represents integrity constraints and is a parent class to them. It
 * contains a body which is a set of {@link atom.RelationalAtom} objects and a
 * head which is a set of objects whose class inherits from {@link atom.Atom}.
 * 
 * @author Martin Jurklies
 */
public abstract class Constraint {
	// the body of the dependency as a list of relational atoms
	protected LinkedHashSet<RelationalAtom> body;
	protected LinkedHashSet<? extends Atom> head;

	protected LinkedHashSet<Comparison<?>> bodyComparisons;
	protected LinkedHashSet<Comparison<?>> headComparisons;

	protected ScopeType scopeType;

	/**
	 * @return the constraint body
	 */
	public LinkedHashSet<RelationalAtom> getBody() {
		return this.body;
	}

	/**
	 * @param body the constraint body to set
	 */
	public void setBody(LinkedHashSet<RelationalAtom> body) {
		this.body = body;
	}

	/**
	 * Adds a relational atom to the body of this constraint.
	 * 
	 * @param atom the relational atom to add to the body
	 * @return {@code true} if {@code atom} was added to {@code body}, {@code false}
	 *         otherwise
	 */
	public boolean addToBody(RelationalAtom atom) {
		return this.body.add(atom);
	}

	/**
	 * @return the constraint head
	 */
	public HashSet<? extends Atom> getHead() {
		return this.head;
	}

	/**
	 * @param head the constraint head to set
	 */
	public void setHead(LinkedHashSet<? extends Atom> head) {
		this.head = head;
	}

	/**
	 * Returns the scope type (source or target of the constraint.)
	 * 
	 * @return The {@link ScopeType}
	 */
	public ScopeType getScopeType() {
		return scopeType;
	}

	/**
	 * Sets the scope type (source or target) of the constraint.
	 * 
	 * @param scopeType The {@link ScopeType}
	 */
	public void setScopeType(ScopeType scopeType) {
		this.scopeType = scopeType;
	}

	public abstract String getTypeName();

	public abstract Constraint copy();

	/**
	 * Returns a LinkedHashSet<RelationalAtom> whose atoms correspond to the
	 * relationName
	 * 
	 * @param relationNames
	 * @return
	 */
	public LinkedHashSet<RelationalAtom> getBodyAtomsBySchema(String relationName) {
		LinkedHashSet<RelationalAtom> result = new LinkedHashSet<RelationalAtom>();

		for (RelationalAtom atom : this.getBody()) {
			if (relationName.equals(atom.getName())) {
				result.add(atom);
			}
		}

		return result;
	}
}
