package gui;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ChateauGui extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        // load FXML file
        URL fxmlLocation = getClass().getResource(String.format("/%s/chateauGUI.fxml", getClass().getPackageName()));
        FXMLLoader loader = new FXMLLoader(fxmlLocation);
        Parent root = loader.load();

        // set controller and pass stage to it
        ChateauGuiController controller = loader.getController();
        controller.setStage(stage);

        // set file, if one is given as command-line argument
        Parameters params = getParameters();
        var unnamedParams = params.getUnnamed();
        String filepath = unnamedParams.size() > 0 ? unnamedParams.get(0) : null;

        if (filepath != null) {
            controller.setFile(filepath);
        }

        // set window title and min dimensions
        stage.setTitle("ChaTEAU v1.3");
        stage.setMinHeight(800);
        stage.setMinWidth(1000);
        stage.setScene(new Scene(root, 1000, 800));

        // show GUI
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
