package gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Alerts {
    /**
     * Alert for termination warnings.
     * 
     * @return An alert that displays a termination warning and asks if the user
     *         wants to continue.
     */
    public static Alert getTerminationAlert() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("CHASE termination");
        alert.setHeaderText("CHASE termination");
        alert.setContentText("At least one performed test shows that the CHASE algorithm "
                + "might not terminate. Are you sure that you want to continue?");
        return alert;
    }

    /**
     * Alert for file saving errors.
     * 
     * @return An alert that displays an error message if a file cannot be saved.
     */
    public static Alert getFileSaveAlert() {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("File can't be saved");
        alert.setContentText("The file couldn't be created. Make sure that "
                + "ChaTEAU has write permissions for the selected directory.");
        return alert;
    }
}
