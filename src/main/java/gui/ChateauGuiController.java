package gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.jdom2.JDOMException;

import chase.ChaseThread;
import constraints.Constraint;
import constraints.STTgd;
import instance.Instance;
import instance.OriginTag;
import io.ChateauLogger;
import io.InputReader;
import io.OutputWriter;
import io.SingleInput;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import termination.ConstraintRewriting;
import termination.SfS;
import termination.CheckType;
import termination.Tester;

public class ChateauGuiController {

    // UI controls
    @FXML
    private Button nextStepButton;
    @FXML
    private Button openFileButton;
    @FXML
    private Button previousStepButton;
    @FXML
    private Button runChaseButton;
    @FXML
    private Button runChecksButton;
    @FXML
    private Button saveLogButton;
    @FXML
    private Button saveResultButton;
    @FXML
    private Button selectAllTermChecksButton;
    @FXML
    private Button selectNoneTermChecksButton;

    @FXML
    private CheckBox acyEgdCheckCheckBox;
    @FXML
    private CheckBox acyCheckCheckBox;
    @FXML
    private CheckBox raCheckCheckBox;
    @FXML
    private CheckBox sftCheckCheckBox;
    @FXML
    private CheckBox waCheckCheckBox;

    @FXML
    private CheckBox egdCheckCheckBox;
    @FXML
    private CheckBox tgdCheckCheckBox;
    @FXML
    private CheckBox sttgdCheckCheckBox;
    @FXML
    private CheckBox negationCheckCheckBox;

    @FXML
    private Label chaseHeadingLabel;
    @FXML
    private Label chateauLabel;
    @FXML
    private Label constrainsChecksLabel;
    @FXML
    private Label copyrightLabel;
    @FXML
    private Label depsChaseLabel;
    @FXML
    private Label depsStartLabel;
    @FXML
    private Label inputChaseLabel;
    @FXML
    private Label inputStartLabel;
    @FXML
    private Label logHeadingLabel;
    @FXML
    private Label resultChaseLabel;
    @FXML
    private Label resultsChecksLabel;
    @FXML
    private Label startHeadingLabel;
    @FXML
    private Label terminationChecksLabel;
    @FXML
    private Label checksHeadingLabel;

    @FXML
    private Stage stage;

    @FXML
    private Tab chaseTab;
    @FXML
    private Tab checksTab;
    @FXML
    private Tab logTab;
    @FXML
    private Tab startTab;

    @FXML
    private Tab inputStartTab;
    @FXML
    private Tab schemaStartTab;

    @FXML
    private TabPane mainTabPane;

    @FXML
    private TextArea depsChaseTextBox;
    @FXML
    private TextArea depsStartTextBox;
    @FXML
    private TextArea inputChaseTextBox;
    @FXML
    private TextArea inputStartTextBox;
    @FXML
    private TextArea logTextBox;
    @FXML
    private TextArea resultChaseTextBox;
    @FXML
    private TextArea resultsChecksTextBox;
    @FXML
    private TextArea schemaStartTextBox;

    // custom fields
    private FileChooser fileChooser;
    private File file;
    private Instance resultInstance;
    private STTgd resultQuery;
    private SingleInput input;
    private String sls = System.lineSeparator();
    private boolean terminationWarning;
    private CompletableFuture<Void> asyncInfo;
    private ChaseThread chaseThread;

    @FXML
    void initialize() {
        terminationWarning = false;

        // text change listener for input text box
        inputStartTextBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> observable, final String oldValue,
                    final String newValue) {
                inputChaseTextBox.setText(newValue);
            }
        });

        // text change listener for dependency text box
        depsStartTextBox.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> observable, final String oldValue,
                    final String newValue) {
                depsChaseTextBox.setText(newValue);
            }
        });

        // tab selection change listener
        mainTabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> ov, Tab oldTab, Tab newTab) {
                toggleStepButtons();
            }
        });
    }

    // custom methods

    /**
     * Handles the enabled/disabled states of the previous and next button.
     */
    void toggleStepButtons() {
        nextStepButton.setDisable(mainTabPane.getSelectionModel().isSelected(mainTabPane.getTabs().size() - 1));
        previousStepButton.setDisable(mainTabPane.getSelectionModel().isSelected(0));
    }

    /**
     * Injects the stage defined in the JavaFX application class into the
     * controller.
     *
     * @param stage The {@link Stage} defined in {@link gui.ChateauGui}.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Adds an entry to the to the {@link gui.ChateauGuiController#logTextBox
     * logTextBox}. Also, the entry gets printed into stdout.
     *
     * @param entry The log entry to add.
     */
    @Deprecated
    private void addLogEntry(Level level, String entry) {
        ChateauLogger.log(level, entry);
    }

    public void setFile(String filepath) {
        openFile(new File(filepath));
    }

    /**
     * to-do
     *
     * @param label
     * @param success
     */
    private void setCheckCheckBoxStatus(CheckBox checkBox, boolean success) {
        if (!(checkBox.getText().endsWith("(passed)") || checkBox.getText().endsWith("(failed)"))) {
            checkBox.setText(success ? checkBox.getText() + " (passed)" : checkBox.getText() + " (failed)");
            if (!success) {
                checkBox.setStyle("-fx-font-weight: bold");
            }
        }
    }

    /**
     * Resets the GUI and any fields that are connected to it.
     */
    private void reset() {
        // reset fields
        file = null;
        terminationWarning = false;
        chaseThread = null;

        // reset strings
        runChaseButton.setText("Run CHASE");
        resultChaseLabel.setText("Result");

        // toggle buttons
        runChecksButton.setDisable(true);
        runChaseButton.setDisable(true);
        saveResultButton.setDisable(true);

        // clear text boxes
        depsStartTextBox.clear();
        inputStartTextBox.clear();
        resultsChecksTextBox.clear();
        resultChaseTextBox.clear();
        logTextBox.clear();

        // select all termination checks
        acyEgdCheckCheckBox.setSelected(true);
        acyCheckCheckBox.setSelected(true);
        raCheckCheckBox.setSelected(true);
        sftCheckCheckBox.setSelected(true);
        waCheckCheckBox.setSelected(true);

        // select all constraint checks
        egdCheckCheckBox.setSelected(true);
        tgdCheckCheckBox.setSelected(true);
        sttgdCheckCheckBox.setSelected(true);
        negationCheckCheckBox.setSelected(true);

        // reset termination check labels
        acyEgdCheckCheckBox.setText("Acyclicity with egd rewriting");
        acyCheckCheckBox.setText("Acyclicity");
        raCheckCheckBox.setText("Rich acyclicity");
        sftCheckCheckBox.setText("Safety");
        waCheckCheckBox.setText("Weak acyclicity");

        // reset constraint check labels
        egdCheckCheckBox.setText("Check egds");
        tgdCheckCheckBox.setText("Check tgds");
        sttgdCheckCheckBox.setText("Check s-t tgds");
        negationCheckCheckBox.setText("Check negations");

        // reset termination check font weights
        acyEgdCheckCheckBox.setStyle("-fx-font-weight: normal");
        acyCheckCheckBox.setStyle("-fx-font-weight: normal");
        raCheckCheckBox.setStyle("-fx-font-weight: normal");
        sftCheckCheckBox.setStyle("-fx-font-weight: normal");
        waCheckCheckBox.setStyle("-fx-font-weight: normal");

        // reset constraint check font weights
        egdCheckCheckBox.setStyle("-fx-font-weight: normal");
        tgdCheckCheckBox.setStyle("-fx-font-weight: normal");
        sttgdCheckCheckBox.setStyle("-fx-font-weight: normal");
        negationCheckCheckBox.setStyle("-fx-font-weight: normal");

        // reset window title
        stage.setTitle(stage.getTitle().split(" - ")[0]);
    }

    /**
     * Opens a file in ChaTEAU.
     *
     * @param file The file to open.
     */
    private void openFile(File file) {
        InputReader reader = new InputReader();

        try {
            input = reader.readFile(file);
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
            return;
        }

        String inputString = "";
        Instance instance = input.getInstance();

        addLogEntry(Level.INFO, "Reading from " + file.getName());

        // add schema
        schemaStartTextBox.setText(instance.schemaToString() + sls);

        if (instance.getOriginTag() == OriginTag.INSTANCE) {
            // input is an instance
            System.out.println("-Input object (instance):");
            // inputStartLabel.setText("Instance");
            inputStartTab.setText("Object (instance)");
            inputChaseLabel.setText("Object (instance)");

            System.out.println(instance);
            inputString += instance + sls;
        } else {
            // input is a query
            System.out.println("-Input object (query):");
            inputStartTab.setText("Object (query)");
            inputChaseLabel.setText("Object (query)");
            System.out.println(instance.getQuery() + sls);
            inputString += instance.getQuery() + sls;
        }

        inputStartTextBox.setText(inputString);
        String constraintsString = "";

        System.out.println("-Constraints:");
        input.getConstraints().forEach(c -> System.out.println(c.getTypeName() + ":" + sls + c + sls));
        // constraintsString += "Integrity Constraints" + sls + sls;
        for (Constraint c : input.getConstraints()) {
            constraintsString += c.getTypeName() + ": " + sls + c + sls + sls;
        }

        depsStartTextBox.setText(constraintsString);

        this.file = file;

        // update window title to include path to current file
        stage.setTitle(stage.getTitle() + " - " + file.getName());

        // enable test button
        runChecksButton.setDisable(false);
    }

    // event listener

    @FXML
    void onNextStepButtonAction(ActionEvent event) {
        mainTabPane.getSelectionModel().selectNext();
        toggleStepButtons();
    }

    @FXML
    void onToggleAllTermChecksButtonAction(ActionEvent event) {
        // if handler was triggered by selectAllTermTestsButton, tick all check boxes,
        // else (triggered by selectNoneTermTestsButton) untick all
        waCheckCheckBox.setSelected(event.getSource() == selectAllTermChecksButton);
        raCheckCheckBox.setSelected(event.getSource() == selectAllTermChecksButton);
        sftCheckCheckBox.setSelected(event.getSource() == selectAllTermChecksButton);
        acyCheckCheckBox.setSelected(event.getSource() == selectAllTermChecksButton);
        acyEgdCheckCheckBox.setSelected(event.getSource() == selectAllTermChecksButton);
    }

    @FXML
    void onOpenFileButtonAction(ActionEvent event) {
        // reset GUI
        reset();

        // define FileChooser; filter *.xml files
        fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("ChaTEAU file (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.setTitle("Open ChaTEAU (*.xml) file");

        // show FileChooser
        file = fileChooser.showOpenDialog(stage);

        if (file == null) {
            return;
        }

        openFile(file);
    }

    @FXML
    void onPreviousStepButtonAction(ActionEvent event) {
        mainTabPane.getSelectionModel().selectPrevious();
        toggleStepButtons();
    }

    @FXML
    void onRunChaseButtonAction(ActionEvent event) {
        if (chaseThread != null) {
            if (chaseThread.isAlive()) {
                chaseThread.interrupt();
            }
        } else {
            // warn user if CHASE might not terminate
            if (terminationWarning) {
                Alert alert = Alerts.getTerminationAlert();
                Optional<ButtonType> response = alert.showAndWait();
                if (response.isPresent() && response.get() != ButtonType.OK) {
                    return;
                }
            }

            // clear result;
            resultChaseTextBox.clear();

            chaseThread = new ChaseThread(input.getInstance(), input.getConstraints(), false);
            chaseThread.start();

            runChaseButton.setText("Stop CHASE");

            // run CHASE async
            CompletableFuture.runAsync(() -> {
                while (chaseThread.isAlive()) {
                    // wait until CHASE terminates
                }

                // get result
                resultInstance = chaseThread.getOutput();

                // Avoid throwing IllegalStateException by running from a non-JavaFX thread.
                Platform.runLater(() -> {
                    // Stop info thread - should not be necessary, but prevents interference
                    asyncInfo.cancel(true);

                    // Update GUI as before
                    String result = "";
                    // check for changes after CHASE execution
                    if (resultInstance.equals(input.getInstance())) {
                        addLogEntry(Level.INFO, "The CHASE yielded no new results.");
                        result += "The CHASE yielded no new results." + sls;
                    }

                    // output results (instance or query)
                    if (resultInstance.getOriginTag() == OriginTag.INSTANCE) {
                        System.out.println("-Output (Instance):");
                        System.out.println(resultInstance);

                        // result += "Instance:" + sls;
                        resultChaseLabel.setText("Result (instance)");
                        result += resultInstance;
                    } else if (resultInstance.getOriginTag() == OriginTag.QUERY) {
                        // If a query was created from an instance, transform it back
                        resultQuery = resultInstance.getQuery();

                        System.out.println("-Output (Query):");
                        System.out.println(resultQuery);

                        // result += "Query:" + sls;
                        resultChaseLabel.setText("Result (query)");
                        result += resultQuery;
                    }

                    // show result in text box
                    resultChaseTextBox.setText(result);

                    // enable save result button; reset run button
                    saveResultButton.setDisable(false);
                    runChaseButton.setText("Run CHASE");

                    // put ChaseLog into log text box
                    // logTextBox.appendText(log.getLog());
                    // logTextBox.setText(ChateauLogger.getLogger().toString());
                    logTextBox.clear();
                    for (LogRecord record : ChateauLogger.getRecords()) {
                        logTextBox.appendText(ChateauLogger.recordToString(record));
                        logTextBox.appendText(System.lineSeparator());
                    }
                });
            });

            // run info thread async
            asyncInfo = CompletableFuture.runAsync(() -> {
                // start time for info
                long sartTime = System.nanoTime();

                // wait 1s for chase to terminate
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // loop for update info every 1s
                while (chaseThread.isAlive()) {
                    Platform.runLater(() -> {
                        // calculate gone time
                        String info = String.format("CHASE is still running... (%d s)",
                                Math.round(Math.floorDiv(System.nanoTime() - sartTime, 1000000000)));
                        resultChaseTextBox.setText(info);
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @FXML
    void onRunChecksButtonAction(ActionEvent event) {
        resultsChecksTextBox.clear();

        // copy the constraints so that the termination test can modify them
        var terminationConstraintsCopy = new LinkedHashSet<Constraint>();
        input.getConstraints().forEach(constraint -> {
            // remove sttgds from constraints, as they are not relevant to termination
            // and the negations allowed in them were not tested in the termination test
            if (!(constraint instanceof STTgd)) {
                terminationConstraintsCopy.add(constraint.copy());
            }
        });

        // test for rich acyclicity
        if (raCheckCheckBox.isSelected()) {
            runCheck(CheckType.RICH_ACYCLICITY, terminationConstraintsCopy);
        }

        // test for weak acyclicity
        if (waCheckCheckBox.isSelected()) {
            runCheck(CheckType.WEAK_ACYCLICITY, terminationConstraintsCopy);
        }

        // test for safety
        if (sftCheckCheckBox.isSelected()) {
            runCheck(CheckType.SAFEFTY, terminationConstraintsCopy);
        }

        // check for acyclicity
        if (acyCheckCheckBox.isSelected()) {
            runCheck(CheckType.ACYCLICITY, terminationConstraintsCopy);
        }

        // check for acyclicity / egd rewriting
        if (acyEgdCheckCheckBox.isSelected()) {
            runCheck(CheckType.ACYCLICITY_WITH_EGD_REWRITING, terminationConstraintsCopy);
        }

        // check constraints
        if (egdCheckCheckBox.isSelected()) {
            runCheck(CheckType.EGDS, input.getConstraints());
        }

        if (tgdCheckCheckBox.isSelected()) {
            runCheck(CheckType.TGDS, input.getConstraints());
        }

        if (sttgdCheckCheckBox.isSelected()) {
            runCheck(CheckType.STTGDS, input.getConstraints());
        }

        if (negationCheckCheckBox.isSelected()) {
            runCheck(CheckType.NEGATIONS, input.getConstraints());
        }

        // enable CHASE button
        runChaseButton.setDisable(false);
    }

    /**
     * Runs a test, writes the result into the log, and updates the gui.
     *
     * @param testType    The {@link CheckType} to run.
     * @param constraints The integrity constraints that shall be tested.
     */
    private void runCheck(CheckType testType, LinkedHashSet<Constraint> constraints) {
        addLogEntry(Level.INFO, String.format("Testing for " + testType.displayName));

        Tester tester = new Tester();
        String result;
        boolean pass;
        CheckBox testCheckBox;
        Instance instance = input.getInstance();

        switch (testType) {
            case WEAK_ACYCLICITY:
                pass = tester.checkWeakAcyclicity(constraints);
                testCheckBox = waCheckCheckBox;
                break;

            case RICH_ACYCLICITY:
                pass = tester.checkRichAcyclicity(constraints);
                testCheckBox = raCheckCheckBox;
                break;

            case SAFEFTY:
                pass = tester.checkSafety(constraints);
                testCheckBox = sftCheckCheckBox;
                break;

            case ACYCLICITY:
                pass = new ConstraintRewriting().prepareConstraintAdn(constraints);
                testCheckBox = acyCheckCheckBox;
                break;

            case ACYCLICITY_WITH_EGD_REWRITING:
                pass = new ConstraintRewriting().prepareAdn(SfS.doSfS((constraints)));
                testCheckBox = acyEgdCheckCheckBox;
                break;

            case EGDS:
                pass = !instance.checkEgds(constraints);
                testCheckBox = egdCheckCheckBox;
                break;

            case TGDS:
                pass = !instance.checkTgds(constraints);
                testCheckBox = tgdCheckCheckBox;
                break;

            case STTGDS:
                pass = !instance.checkSttgds(constraints);
                testCheckBox = sttgdCheckCheckBox;
                break;

            case NEGATIONS:
                pass = !instance.checkNegations(constraints);
                testCheckBox = negationCheckCheckBox;
                break;

            default:
                return;
        }

        if (pass) {
            result = testType.positiveMessage;
            terminationWarning = true;
        } else {
            result = testType.negativeMessage;
        }

        addLogEntry(Level.INFO, result);
        resultsChecksTextBox.appendText(result + sls);
        setCheckCheckBoxStatus(testCheckBox, !pass);
    }

    @FXML
    void onSaveLogButtonAction(ActionEvent event) {
        fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("ChaTEAU log (.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.setInitialFileName("chateau-log.txt");
        fileChooser.setTitle("Save log");
        File file = fileChooser.showSaveDialog(stage);

        if (file == null) {
            return;
        }

        OutputWriter writer = new OutputWriter();
        try {
            writer.writeLog(file);
        } catch (FileNotFoundException e) {
            Alerts.getFileSaveAlert().show();
        }
    }

    @FXML
    void onSaveResultButtonAction(ActionEvent event) {
        fileChooser = new FileChooser();
        ExtensionFilter filter = new ExtensionFilter("ChaTEAU result (.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.setInitialFileName("chateau-result.xml");
        fileChooser.setTitle("Save result");
        File file = fileChooser.showSaveDialog(stage);

        if (file == null) {
            return;
        }

        OutputWriter writer = new OutputWriter();
        try {
            if (resultQuery == null) {
                writer.writeXML(resultInstance, file);
            } else {
                writer.writeXML(resultInstance, resultQuery, file);
            }
        } catch (IOException e) {
            Alerts.getFileSaveAlert().show();
        }
    }
}
