package chateau;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.jdom2.JDOMException;

import api.TerminationCheckFailure;
import api.TesterService;
import chase.ChaseThread;
import constraints.STTgd;
import gui.Alerts;
import instance.Instance;
import instance.OriginTag;
import io.InputReader;
import io.OutputWriter;
import io.SingleInput;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            // Launch JavaFX application
            gui.ChateauGui.main(args);
        } else {
            File file = null;
            boolean runChecks = false;
            boolean saveOutput = false;
            boolean preserveSourceSchema = false;
            boolean outputProvenance = false;

            /*
             * -c: turns on the termination checks
             * -o: turns on the saving of the output
             * -p: turns on the provenance calculation and output
             * -s: turns on the source scheme copy in the output
             */
            for (String arg : args) {
                if (arg.equals("-c")) {
                    runChecks = true;
                } else if (arg.equals("-o")) {
                    saveOutput = true;
                } else if (arg.equals("-p")) {
                    outputProvenance = true;
                } else if (arg.equals("-s")) {
                    preserveSourceSchema = true;
                } else {
                    File f = new File(arg);
                    if (f.exists() && !f.isDirectory()) {
                        file = new File(arg);
                    }
                }
            }

            if (file != null) {
                System.out.println("File path: " + file.getAbsolutePath() + "\n");

                InputReader reader = new InputReader();
                SingleInput input;
                try {
                    input = reader.readFile(file);
                } catch (JDOMException | IOException e) {
                    e.printStackTrace();
                    return;
                }

                if (runChecks) {
                    runChecks(input);
                }

                Instance resultInstance = runChase(input, outputProvenance);

                if (saveOutput) {
                    String path = file.getAbsolutePath();
                    File outputFile = new File(path.substring(0, path.length() - 4).concat("_ChaTEAU_CLI_Output.xml"));

                    OutputWriter writer = new OutputWriter();

                    writer.preserveSourceSchema = preserveSourceSchema;
                    writer.outputProvenance = outputProvenance;

                    try {
                        writer.writeXML(resultInstance, outputFile);
                    } catch (FileNotFoundException e) {
                        Alerts.getFileSaveAlert().show();
                    } catch (IOException e) {
                        Alerts.getFileSaveAlert().show();
                    }
                }
            }
        }
    }

    static Instance runChase(SingleInput input, boolean calculateProvenance) {
        ChaseThread chaseThread = new ChaseThread(input.getInstance(), input.getConstraints(), calculateProvenance);
        chaseThread.start();
        System.out.println("CHASE started...\n");
        long sartTime = System.nanoTime();
        int counter = 0;
        while (chaseThread.isAlive()) {
            // wait until CHASE terminates
            // check every 0.1 seconds if the CHASE is still running
            // write every second the elapsed time
            try {
                Thread.sleep(100);
                counter++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (chaseThread.isAlive() && counter >= 10) {
                String info = String.format("CHASE is still running... (%d s)",
                        Math.round(Math.floorDiv(System.nanoTime() - sartTime, 1000000000)));
                System.out.println(info);
                counter = 0;
            }
        }
        // get result
        Instance resultInstance = chaseThread.getOutput();

        // check for changes after CHASE execution
        if (resultInstance.equals(input.getInstance())) {
            System.out.println("The CHASE yielded no new results.");
        }

        // output results (instance or query)
        if (resultInstance.getOriginTag() == OriginTag.INSTANCE) {
            System.out.println("-Output (Instance):");
            System.out.println(resultInstance);
        } else if (resultInstance.getOriginTag() == OriginTag.QUERY) {
            // If a query was created from an instance, transform it back
            STTgd resultQuery = resultInstance.getQuery();

            System.out.println("-Output (Query):");
            System.out.println(resultQuery);
        }

        return resultInstance;
    }

    private static void runChecks(SingleInput input) {
        try {
            TesterService.runChecks(input);
        } catch (TerminationCheckFailure ttf) {
            System.out.println("WARNING: Some termination/constraint checks failed:");
            System.out.println(ttf.getMessage());
        }
    }
}