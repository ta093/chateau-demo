# ChaTEAU - Chase for Transforming, Evolving, and Adapting databases and queries, Universal approach: CHASE and BACKCHASE tool

## Introduction

Welcome to ChaTEAU, a Universal Toolkit for Applying the Chase. 

What do applications like semantic optimization, data exchange and integration, answering queries under dependencies, query reformulation with constraints, and data cleaning have in common? All these applications can be processed by the Chase, a family of algorithms for reasoning with constraints. While the theory of the Chase is well understood, existing implementations are confined to specific use cases and application scenarios, making it difficult to reuse them in other settings. ChaTEAU overcomes this limitation: It takes the logical core of the Chase, generalizes it, and provides a software library for different Chase applications in a single toolkit.

## Installation / Build

The [documentary](https://git.informatik.uni-rostock.de/ta093/chateau-demo/-/tree/main/doc) describes the preliminaries needed to build and execute ChaTEAU, how to use the command-line interface, and how to navigate through the graphical user interface of the tool. Literature about ChaTEAU and the CHASE algorithm in general can be found [here](Literature.md).

Building ChaTEAU requires [Apache Maven](https://maven.apache.org/) which requires Java.
To check whether Java is already installed, you can run

```
java --version
```

to check the Java version. If you get an error message, Java needs to be installed first.
Visit [this site](https://adoptopenjdk.net/), then choose OpenJDK 11 and install it
for your platform.

To package an executable .jar that includes all
dependencies (e.g. JDOM and JavaFX), run

```
mvn clean package
```

within the project's root directory. The .jar file will be created at
target/chateau-(version)-all.jar ("version" represents the current version
and might change, so please double-check the file name). This file can then be
executed directly or via the terminal (useful to track exceptions):

```
java -jar chateau-(version)-all.jar
```

## Documentation/Manual

A manual/walkthrough can be found [here](https://git.informatik.uni-rostock.de/ta093/chateau-demo/-/tree/main/doc). A short promo video can be found [here](https://mediathek2.uni-regensburg.de/playthis/6426bda2e4e328.20273312) (password: ChaTEAU).

## Literature referencing ChaTEAU

1. Auge, Heuer: ProSA -- Using the CHASE for Provenance Management (2019). [(paper)](https://link.springer.com/chapter/10.1007/978-3-030-28730-6_22)
2. Auge, Scharlau, Görres, Zimmer, Heuer: ChaTEAU -- A Universal Toolkit for Applying the Chase (2022). [(paper)](https://arxiv.org/abs/2206.01643)

## Usage

ChaTEAU is a stand-alone application for the Chase. It is implemented as a Maven project and can easily be accessed through its GUI. In addition, ChaTEAU can be accessed via its API, making it easy to employ it as building block or library for developing other Chase-based applications. One first application of ChaTEAU in the context of provenance management is ProSA. This system determines which part of a huge amount of data is necessary for evaluating a query result in terms of reproducibility, reconstructability and traceability by applying the chase several times. For details we refer to [(paper1)](https://link.springer.com/chapter/10.1007/978-3-031-15743-1_9),[(paper2)](https://link.springer.com/chapter/10.1007/978-3-030-28730-6_22) and the corresponding [(GIT repository)](https://git.informatik.uni-rostock.de/ta093/prosa-demo).


## Credits

...
