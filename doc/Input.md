# Input files for ChaTEAU

To input data into _ChaTEAU_, the well-established XML file format is used. In this section, you will learn how to create your own input file for the tool.

## Root element and main elements

The root elements of each XML file is represented by an `input` element without tags. This element consists of a `schema` element (without tags) and either an `instance` or a `query` element, both without any tags as well.

### Schema

The `schema` elements consists of a set of `relation` elements with a `name` tag holding the relation's name, wrapped in a `relations` element without tags. A `relation` element might also have a `tag` tag whose value can either be `S` or `T`, signalizing source and target relations. It consists of self-closing `attribute` elements that must have a `name` tag, representing the attribute's name, and a `type` tag whose value can be `string`, `int` or `double`, representing its data type.

Example:  
```xml
<relations>
    <relation name="Students">
        <attribute name="student_id" type="int" />
        <attribute name="lastname" type="string" />
        <attribute name="firstname" type="string" />
        <attribute name="course" type="string" />
        <attribute name="institute" type="string" />
    </relation>
    <relation name="Courses">
        <attribute name="course" type="string" />
        <attribute name="institute" type="string" />
        <attribute name="classpresident" type="int" />
    </relation>
</relations>
```

Additionally, the `schema` element might also hold a set of dependencies, wrapped in a `dependencies` element without any tags. This element then consists of `tgd`, `egd`, and `sttgd` elements (without tags) representing the corresponding dependency types.

Each of those follow a generic structure. Dependencies consist of a tag-less `body` element, followed by a tag-less `head` element. Both `body` and `head` include several `atom` elements. Details can be seen below.

Shortened example:  
```xml
<dependencies>
    <tgd>
        <body>
            <atom name="Students">
                <!-- ... -->
            </atom>
        </body>
        <head>
            <atom name="Courses">
                <!-- ... -->
            </atom>
        </head>
    </tgd>

    <tgd>
        <body>
            <atom name="Courses">
                <!-- ... -->
            </atom>
        </body>
        <head>
            <atom name="Students">
                <!-- ... -->
            </atom>
        </head>
    </tgd>
</dependencies>
```

### Instance and Query

Using the CHASE on instances, the `schema` element is followed by an `instance` element (without tags) which consists of `atom` elements. We can read more about atoms below. Using the CHASE on queries, the `schema` element is followed by a `query` element without tags instead. A `query` consists of a `body` and a `head` element, just like the dependencies, since queries can be interpreted and represented as special kinds of s-t tgds.

## Atoms

An `atom` element represents a tuple, w.r.t. instances, or a _logical expression atom_, w.r.t. queries and dependencies. Is has a `name` tag that holds the name of the corresponding relation and consists of `constant`, `variable`, and/or `null` elements.

### Constants

Constants can be represented via the self-closing `constant` element that has tags called `name` and `value`, representing the corresponding attribute name and the attribute value.

Example:  
```xml
<constant name="student_id" value="3" />
```

### Variables

Variables are represented using a self-closing `variable` element that holds a `name` tag providing the attribute name, a `type` tag with either `V` (all-quantified) or `E` (existential-quantified) as type value, and an `index` tag holding the index for this variable.

Example: To express the all-quantified variable `course` with index `1`, the XML syntax is:
```xml
<variable name="course" type="V" index="1" />
```

### Null values

Last but not least we take a closer look at null values. Just as `variable` elements, `null` elements are self-closing and consist of a `name` tag holding the corresponding attribute name, and an `index` tag representing the index of the null value.

Example: Let's say we don't know the value of `course` for certain tuples, and that our null value is the third null value for `course` in our instance or query. The XML syntax is:  
```xml
<null name="course" index="3" />
```

## Complete Example

To express the tuple `Students(3, Miller, Max, Electrical Engineering, IOF)`, we need to setup an `atom` element with `name = Students` and five corresponding `constant` values.

```xml
<atom name="Students">
    <constant name="student_id" value="3" />
    <constant name="lastname" value="Miller" />
    <constant name="firstname" value="Max" />
    <constant name="course" value="Electrical engineering" />
    <constant name="institute" value="IOF" />
</atom>
```

Full examples can be seen in the Examples section of this documentation or, if you want to directly use the example files, in the `example` directory of this repository.