# Literature

## CHASE

### "Benchmarking the Chase"

* __Title:__ Benchmarking the Chase
* __Authors:__ Michael Benedikt, Boris Motik, George Konstantinidis, Paolo Papotti, Efthymia Tsamoura, Giansalvatore Mecca and Donatello Santoro.
* __Year:__ 2017
* __Publisher:__ Association for Computing Machinery
* __Language:__ English
* __Number of pages:__ 16
* __Keywords:__ CHASE, dependencies, data exchange, query response
* __URL:__ [https://eprints.soton.ac.uk/423202/](https://eprints.soton.ac.uk/423202/)
* __Abstract:__ This article provides the first comprehensive and publicly available benchmark composed of two distinct parts. While the first part includes several tools that enable the generation and processing of test data in a common format, the second part comprises a set of test scenarios, correctness scenarios, and data exchange and query response scenarios. This benchmark is used in order to compare CHASE-based systems for data exchange and query tasks with each other and with systems capable of solving similar tasks developed in closely related communities. The ultimate objectives would be to evaluate CHASE implementations over a wide range of assumptions about dependencies and data, and to determine whether existing CHASE implementations can support data exchange and query answering for non-trivial inputs, as well as to identify the implementation decisions that are most important to performance.

### "Incomplete Data and Data Dependencies in Relational Databases"

* __Title:__ Incomplete Data and Data Dependencies in Relational Databases
* __Authors:__ Sergio Greco, Cristian Molinaro, Francesca Spezzano
* __Year:__ 2012
* __Publisher:__ Morgan & Claypool Publishers
* __Language:__ English
* __Number of pages:__ 125
* __Keywords:__ incomplete databases, inconsistent databases, data dependencies, CHASE, consistent query answers
* __URL:__ [https://ieeexplore.ieee.org/document/6812534/](https://ieeexplore.ieee.org/document/6812534/)
* __Abstract:__ The CHASE has long been used as a central tool to analyze dependencies and their effect on queries. It has been applied to different relevant problems in database theory such as query optimization, query containment and equivalence, dependency implication, and database schema design. Recent years have seen a renewed interest in the CHASE as an important tool in several database applications, such as data exchange and integration, query answering in incomplete data, and many others.  
It is well known that the CHASE algorithm might be non-terminating and thus, in order for it to find practical applicability, it is crucial to identify cases where its termination is guaranteed. Another important aspect to consider when dealing with the CHASE is that it can introduce null values into the database, thereby leading to incomplete data. Thus, in several scenarios where the CHASE is used the problem of dealing with data dependencies and incomplete data arises.  
This book discusses fundamental issues concerning data dependencies and incomplete data with a particular focus on the CHASE and its applications in different database areas. We report recent results about the crucial issue of identifying conditions that guarantee the CHASE termination. Different database applications where the CHASE is a central tool are discussed with particular attention devoted to query answering in the presence of data dependencies and database schema design.

### "ProSA - Using the CHASE for Provenance Management"

* __Title:__ ProSA - Using the CHASE for Provenance Management
* __Authors:__ Tanja Auge, Andreas Heuer
* __Year:__ 2019
* __Language:__ English
* __Number of pages:__ 16
* __Keywords:__ Theoretical foundation of databases, Data curation, Annotation, Provenance, Temporal databases, CHASE
* __URL:__ [https://link.springer.com/chapter/10.1007%2F978-3-030-28730-6_22](https://link.springer.com/chapter/10.1007%2F978-3-030-28730-6_22)
* __Abstract:__ Collecting, storing, tracking, and archiving scientific data is the main task of research data management, being the basis for scientific evaluations. In addition to the evaluation (a complex query in the case of structured databases) and the result itself, the important part of the original database used has also to be archived. To ensure reproducible and replicable research, the evaluation queries can be processed again at a later point in time in order to reproduce the result. Being able to calculate the origin of an evaluation is the main problem in provenance management, particularly in why and how data provenance. We are developing a tool called ProSA which combines data provenance and schema/data evolution using the CHASE for the different database transformations needed. Besides describing the main ideas of ProSA, another focus of this paper is the concrete use of our CHASE tool ChaTEAU for invertible query evaluation.

## ChaTEAU

### "Unification of CHASE on instances and queries using the example of ChaTEAU"

* __Title:__ Unification of CHASE on instances and queries using the example of ChaTEAU (German: _Vereinheitlichung des CHASE auf Instanzen und Anfragen am Beispiel ChaTEAU_)
* __Author:__ Jakob Zimmer
* __Year:__ 2020
* __Language:__ German
* __Number of pages:__ 89
* __Keywords:__ CHASE, BACKCHASE, ChaTEAU, Graal, PDQ, Llunatic
* __URL:__ [http://eprints.dbis.informatik.uni-rostock.de/1008/](http://eprints.dbis.informatik.uni-rostock.de/1008/)
* __Abstract:__ The CHASE is a universal database algorithm, which can incorporate various parameters such as dependencies into an object, so that a combination of the two components is the result. In the CHASE tool ChaTEAU, the CHASE algorithm was implemented individually for incorporating integrity conditions in instances and queries as CHASE objects. These different methods for the different objects do not correctly reflect the universality of the CHASE. The goal of the present bachelor thesis is to unify the CHASE algorithms on instances and queries. For this purpose, a concept was developed in which both CHASE objects are represented as one degenerated instance, into which a single adapted CHASE method then incorporates integrity conditions. This concept was then implemented in ChaTEAU, with additional adaptations that improved the functionality of the tool and the correctness of the result. A comprehensive overview of the underlying theory of the CHASE was crucial for the correct unification, so that this work is appealing to both those interested in the CHASE and those interested in ChaTEAU.

### "Extension of the CHASE tool ChaTEAU by a termination criterion"

* __Title:__ Extension of the CHASE tool ChaTEAU by a termination criterion (German: _Erweiterung des CHASE-Werkzeugs ChaTEAU um ein Terminierungskriterium_)
* __Author:__ Andreas Oliver Görres
* __Year:__ 2020
* __Language:__ German
* __Number of pages:__ 175
* __Keywords:__ CHASE, ChaTEAU, PDQ, Llunatic, ChaseTEQ, Graal, scheduling criterion, Adn++ algorithm
* __URL:__ [http://eprints.dbis.informatik.uni-rostock.de/1009/](http://eprints.dbis.informatik.uni-rostock.de/1009/)
* __Abstract:__ The CHASE is a basic algorithm of database theory. In particular, it can be used to incorporate integrity constraints into database instances. While the CHASE is confluent and terminates in most cases, this is not guaranteed for general integrity constraints. CHASE termination has been shown to be an undecidable problem. Nonetheless, several criteria for integrity constraints are known which guarantee CHASE terminating on any database instance. This thesis compares termination criteria found in literature using examples from a concrete database. Based on our analysis, we choose the most suitable termination tests and integrate them into the CHASE tool ChaTEAU. Users may select an arbitrary combination of termination tests. In case none of the test results guarantees safe termination of the CHASE, there is a high chance the algorithm will not terminate. However, the user is free to ignore this prediction and to start the CHASE anyway. While the termination tester was able to predict CHASE termination correctly in all given test cases, the runtime analysis suggests that not all of the test criteria are scalable. Since none of the test criteria is suitable for every use case, we provide recommendations to support the user's decision for an appropriate termination test. In particular, users should perform a test with polynomial time complexity (e.g. Safety) first, and only afterwards - based on the results of the previous test - they should execute a test with exponential time complexity (e.g. Acyclicity).