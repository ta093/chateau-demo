# ChaTEAU documentation

Welcome to the documentation for the _ChaTEAU_ tool. This file describes the preliminaries needed to build and execute _ChaTEAU_, how to use the command-line interface, and how to navigate through the graphical user interface of the tool. Literature about _ChaTEAU_ and the CHASE algorithm in general can be found [here](Literature.md).

## Getting started / Preliminaries

Needed software:
* Java Development Kit 11
* Apache Maven 3.6.5+

To run _ChaTEAU_, you first need to package it into an executable Java Archive file (.jar). The easiest way to do so is by using _Apache Maven_. Within the repository's root directory, run `mvn clean package` and you will find two .jar files in the `target` directory, `chateau-1.2.jar` and `chateau-1.2-all.jar`. The second one is the single-executable one we need, since it includes all needed dependencies and can easily be transferred between devices.

## Using the CLI

The packaged _ChaTEAU_ executable can be used via the command-line by running it with certain parameters. Below you can find a list of all available flags and their function.

Flag | Function | Description
---|---|---
`-c` | Run checks | Runs the termination and integrity checks.
`-o` | Save output file | Can be used to redirect the CHASE result into a ChaTEAU-formatted XML file.
`-p` | Track provenance information | Can be used to collect where-, why-, and how-provenance during the CHASE execution. Only useful in combination with `-o`.
`-s` | Save source schema | Can be used to keep the schema of source tables in the result file. Only useful in combination with `-o`.

In addition to these flags, the path to a _ChaTEAU_-formatted XML file must be provided.

#### Example

To run _ChaTEAU_ with a file stored at `D:\chateau.xml`, run checks, keep the source schema and save the output, run the command `java -jar chateau-1.2-all.jar 'D:\chateau.xml' -c -o -s`. The resulting file including the source schema will then be stored at `D:\chateau_ChaTEAU_CLI_Output.xml`.

![ChaTEAU command-line interface](img/cli_01.png)

## Using the GUI

Additionally to the command-line interface, _ChaTEAU_ also provides a graphical user interface that can be used to run the CHASE algorithm the graphical way.

### Start

After starting _ChaTEAU_, either by directly executing the packaged .jar file or by running `java -jar chateau-1.2-all.jar` in a terminal, you will be greeted with the Start tab. Here you can open a _ChaTEAU_-formatted XML file by pressing the "Open ChaTEAU file" button. Example files are provided within the "examples" folder of this repository. After selecting a file, you get a brief overview of the file's input object (an instance or a query) as well as its input parameters (a set of dependencies).

![ChaTEAU's Start tab](img/start_02.png)

At each stage (Start, Checks, CHASE, Log) you can easily navigate backwards by using the "Previous step" button. The "Next step" button, on the other hand, is disabled util the current stage has been completed. After opening a file, we can press "Next step" to get to the Checks tab.

### Checks

_ChaTEAU_ supports a bunch of termination and integrity checks, including checks for weak and rich acyclicity. You can see every currently supported check in the Checks tab. By default, all checks are selected, hence, you only have to press the "Run selected checks" button in order to complete this stage. If you want to skip certain checks, simply uncheck them, but notice that you have to run at least one terminiation check to proceed.

![ChaTEAU's Checks tab](img/checks_01.png)

After executing the selected checks, you will see the results in a text box. Checks that failed become bold and will trigger a warning on the next stage.

### CHASE

The next and most important stage is represented in the CHASE tab. It consists of the input object and parameters as well as the result after running the CHASE algorithm, which can be done by pressing the "Run CHASE" button, which then turns into a "Stop CHASE" button. If at least one check failed in the previous stage, you will see a warning about a potentially non-terminating CHASE run. If you choose to cancel the process, nothing happens, if you continue, the CHASE might not terminating due to, for example, a circulating creation of infinite new tuples. However, you can still stop the CHASE's execution at any time by pressing the "Stop CHASE" button.

![ChaTEAU's CHASE tab](img/chase_01.png)

After the CHASE algorithm has been executed, you will see the final result. Additionally, you can save that result by pressing the "Save result" button which then creates a _ChaTEAU_-formatted XML file that can be stored anywhere on your computer.

![ChaTEAU's CHASE tab after its execution](img/chase_02.png)

### Log

The final stage is the Log tab. Here you can see a detailled log about operations that have been run during the whole execution, including termination and integrity checks, found (active) triggers and homomorphisms, and the final result.

![ChaTEAU's Log tab](img/log_01.png)