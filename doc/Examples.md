# Examples

All examples follow a fictional university database schema:

* student(id, name, course)
* participant(module, id, semester)
* grade(module, id, semester, score)

The only exception can be seen in *example 4* which uses a slightly different schema:

* student(id, name, course, institute)
* lecture(course, institute, classpresident)

## example_instance_1.xml

This example showcases data exchange use cases by using an s-t tgd to migrate data between different schemas, including null values for unknown values.

* Chase parameter (s-t tgd): 

$\texttt{student}(V_{id},'\text{Max}',V_{course}) \wedge \texttt{participant}(V_{module},V_{id},V_{semester}) \rightarrow \exists E_{semester}, E_{score} : \texttt{grade}(V_{module},V_{id},E_{semester},E_{score})$

* Chase object (instance): 

| id | name | course |	
| --- | --- | --- |		
| 3 | Max | Math |
| 1 | Max | Math |
| 7 | Mia | NULL_1 |

| module | id | semester |
| --- | --- | --- |
| 2 | 3 | 4 |
| 7 | 3 | NULL_2 |

* result (instance):

| moudle | id | semester | score |
| ------ | ------ | --- | --- |
| 2 | 3 |  NULL_3 | NULL_4 |
| 7 | 3 | NULL_5 | NULL_6 |

```xml
<input>
    <schema>
        <relations>
            <relation name="student" tag="S">
                <attribute name="id" type="int"/>
                <attribute name="name" type="string"/>
                <attribute name="course" type="string"/>
            </relation>
            <relation name="participant" tag="S">
                <attribute name="module" type="int"/>
                <attribute name="id" type="int"/>
                <attribute name="semester" type="int"/>
            </relation>
            <relation name="grade" tag="T">
                <attribute name="module" type="int"/>
                <attribute name="id" type="int"/>
                <attribute name="semester" type="int"/>
                <attribute name="score" type="int"/>
            </relation>
        </relations>
        <dependencies>
            <sttgd>
                <body>
                    <atom name="student">
                        <variable name="id" type="V" index="1" />
                        <constant name="name" value="Max" />
                        <variable name="course" type="V" index="1" />
                    </atom>
                    <atom name="participant">
                        <variable name="module" type="V" index="1" />
                        <variable name="id" type="V" index="1" />
                        <variable name="semester" type="V" index="1" />
                    </atom>
                </body>
                <head>
                    <atom name="grade">
                        <variable name="module" type="V" index="1" />
                        <variable name="id" type="V" index="1" />
                        <variable name="semester" type="E" index="1" />
                        <variable name="score" type="E" index="1" />
                    </atom>
                </head>
            </sttgd>
        </dependencies>
    </schema>
    <instance>
        <atom name="student">
            <constant name="id" value="3" />
            <constant name="name" value="Max" />
            <constant name="course" value="Math" />
        </atom>
        <atom name="student">
            <null name="id" index="1" />
            <constant name="name" value="Max" />
            <constant name="course" value="Math" />
        </atom>
        <atom name="student">
            <constant name="id" value="7" />
            <constant name="name" value="Mia" />
            <null name="course" index="1" />
        </atom>
        <atom name="participant">
            <constant name="module" value="2" />
            <constant name="id" value="3" />
            <constant name="semester" value="4"/>
        </atom>
        <atom name="participant">
            <constant name="module" value="7" />
            <constant name="id" value="3" />
            <null name="semester" index="1" />
        </atom>
    </instance>
</input>
```

## example_query_1.xml

This example uses a tgd for which no active triggers exist. The CHASE will therefore generate no new tuples, but _ChaTEAU_ will still provide the unchanged result.

* Chase parameter (tgd): 

$\texttt{student}(V_{id},V_{name},V_{course}) \rightarrow \exists E_{module}, E_{semester} : \texttt{participant}(E_{module},V_{id},E_{semester})$

* Chase object (query): 

$\texttt{student}(\#E\_\text{id}\_1, \#V\_\text{name}\_1, \#E\_\text{course}\_1), \texttt{participant}(\#E\_\text{module}\_1, \#E\_\text{id}\_1, \#E\_\text{semester}\_1) \rightarrow (\#V\_\text{name}\_1)$

* result (query): 

The CHASE yielded no new results.

$\texttt{student}(\#E\_\text{id}\_1, \#V\_\text{name}\_1, \#E\_\text{course}\_1), \texttt{participant}(\#E\_\text{module}\_1, \#E\_\text{id}\_1, \#E\_\text{semester}\_1) \rightarrow (\#V\_\text{name}\_1)$

```xml
<input>
    <schema>
        <relations>
            <relation name="student">
                <attribute name="id" type="int" />
                <attribute name="name" type="string" />
                <attribute name="course" type="string" />
            </relation>
            <relation name="participant">
                <attribute name="module" type="int" />
                <attribute name="id" type="int" />
                <attribute name="semester" type="int" />
            </relation>
        </relations>
        <dependencies>
            <tgd>
                <body>
                    <atom name="student">
                        <variable name="id" type="V" index="1" />
                        <variable name="name" type="V" index="1" />
                        <variable name="course" type="V" index="1" />
                    </atom>
                </body>
                <head>
                    <atom name="participant">
                        <variable name="module" type="E" index="1" />
                        <variable name="id" type="V" index="1" />
                        <variable name="semester" type="E" index="1" />
                    </atom>
                </head>
            </tgd>
        </dependencies>
    </schema>
    <query>
        <body>
            <atom name="student">
                <variable name="id" type="E" index="1" />
                <variable name="name" type="V" index="1" />
                <variable name="course" type="E" index="1" />
            </atom>
            <atom name="participant">
                <variable name="module" type="E" index="1" />
                <variable name="id" type="E" index="1" />
                <variable name="semester" type="E" index="1" />
            </atom>
        </body>
        <head>
            <atom>
                <variable name="name" type="V" index="1" />
            </atom>
        </head>
    </query>
</input>
```

## example_query_2.xml

This example showcases query rewritings by using an egd applied on a query.

* Chase parameter (egd):

$\texttt{student}(V_{id_1},V_{name_1},V_{course_1}) \wedge \texttt{student}(V_{id_2},V_{name_1},V_{course_2}) \rightarrow V_{course_1} = V_{course_2}$

* Chase object (query):

$\texttt{student}(\#V\_\text{id}\_1, \#V\_\text{name}\_1, \#E\_\text{course}\_1), \texttt{student}(\#E\_\text{id}\_1, \#V\_\text{name}\_1, \#V\_\text{course}\_1) \rightarrow (\#V\_\text{id}\_1, \#V\_\text{name}\_1, \#V\_\text{course}\_1)$

* result (query):

$\texttt{student}(\#V\_\text{id}\_1, \#V\_\text{name}\_1, \#V\_\text{course}\_1), \texttt{student}(\#E\_\text{id}\_1, \#V\_\text{name}\_1, \#V\_\text{course}\_1) \rightarrow (\#V\_\text{id}\_1, \#V\_\text{name}\_1, \#V\_\text{course}\_1)$

```xml
<input>
    <schema>
        <relations>
            <relation name="student">
                <attribute name="id" type="int" />
                <attribute name="name" type="string" />
                <attribute name="course" type="string" />
            </relation>
        </relations>
        <dependencies>
            <egd>
                <body>
                    <atom name="student">
                        <variable name="id" type="V" index="1" />
                        <variable name="name" type="V" index="1" />
                        <variable name="course" type="V" index="1" />
                    </atom>
                    <atom name="student">
                        <variable name="id" type="V" index="2" />
                        <variable name="name" type="V" index="1" />
                        <variable name="course" type="V" index="2" />
                    </atom>
                </body>
                <head>
                    <atom>
                        <variable name="course" type="V" index="1" />
                        <variable name="course" type="V" index="2" />
                    </atom>
                </head>
            </egd>
        </dependencies>
    </schema>
    <query>
        <body>
            <atom name="student">
                <variable name="id" type="V" index="1" />
                <variable name="name" type="V" index="1" />
                <variable name="course" type="E" index="1" />
            </atom>
            <atom name="student">
                <variable name="id" type="E" index="1" />
                <variable name="name" type="V" index="1" />
                <variable name="course" type="V" index="1" />
            </atom>
        </body>
        <head>
            <atom>
                <variable name="id" type="V" index="1" />
                <variable name="name" type="V" index="1" />
                <variable name="course" type="V" index="1" />
            </atom>
        </head>
    </query>
</input>
```

## example_instance_2.xml

This example demonstrates the termination checks _ChaTEAU_ includes. Two tgds that link to each other will make the test for rich acyclicity fail and trigger a warning, although the implemented Standard CHASE will still terminate.

* Chase parameter (tow tgds):

$\texttt{student}(V_{id},V_{name},V_{course},V_{institute}) \rightarrow \exists E_{classpresident} : \texttt{lecture}(V_{course},V_{institute},E_{classpresident})$ 

$\texttt{lecture}(V_{course},V_{institute},V_{classpresident}) \rightarrow \exists E_{name} : \texttt{student}(V_{id},E_{name},V_{course},V_{institute})$

* Chase object (instance):

| id | name | course | institute |
| ------ | ------ | --- | --- |
| 3 | Max | Electrical engineering | IOF |

* result (instance):

| course | institute | classpresident |
| ------ | ------ | --- |
| Electrical engineering | IOF | NULL_1 |

| id | name | course | institute |
| ------ | ------ | --- | --- |
| 3 | Max | Electrical engineering | IOF |
| NULL_1 | NULL_2 | Electrical engineering | IOF |

```xml
<input>
	<schema>
		<relations>
			<relation name="student">
				<attribute name="id" type="int" />
				<attribute name="name" type="string" />
				<attribute name="course" type="string" />
				<attribute name="institute" type="string" />
			</relation>
			<relation name="lecture">
				<attribute name="course" type="string" />
				<attribute name="institute" type="string" />
				<attribute name="classpresident" type="int" />
			</relation>
		</relations>

		<dependencies>
			<tgd>
				<body>
					<atom name="student">
						<variable name="id" type="V" index="1" />
						<variable name="name" type="V" index="1" />
						<variable name="course" type="V" index="1" />
						<variable name="institute" type="V" index="1" />
					</atom>
				</body>
				<head>
					<atom name="lecture">
						<variable name="course" type="V" index="1" />
						<variable name="institute" type="V" index="1" />
						<variable name="classpresident" type="E" index="1" />
					</atom>
				</head>
			</tgd>

			<tgd>
				<body>
					<atom name="lecture">
						<variable name="course" type="V" index="1" />
						<variable name="institute" type="V" index="1" />
						<variable name="classpresident" type="V" index="1" />
					</atom>
				</body>
				<head>
					<atom name="student">
						<variable name="classpresident" type="V" index="1" />
						<variable name="name" type="E" index="1" />
						<variable name="course" type="V" index="1" />
						<variable name="institute" type="V" index="1" />
					</atom>
				</head>
			</tgd>
		</dependencies>
	</schema>
    
	<instance>
		<atom name="student">
			<constant name="id" value="3" />
			<constant name="name" value="Max" />
			<constant name="course" value="Electrical engineering" />
			<constant name="institute" value="IOF" />
		</atom>
	</instance>
</input>
```

## example_instance_3.xml

This example showcases a data repairing use case. An egd is used to express a functional dependency between `id` and `name` as well as `course`. Incomplete tuples in our instance will be repaired and merged.

* Chase parameter (egd):

$\texttt{student}(V_{id_1},V_{name_1},V_{course_1}) \wedge \texttt{student}(V_{id_1},V_{name_2},V_{course_2}) \rightarrow V_{name_1} = V_{name_2} \wedge V_{course_1} = V_{course_2}$

* Chase object (instance):

| id | name | course |
| ------ | ------ | --- |
| 3 | Max | 1 |
| 3 | NULL_1 | Computer Science |

* result (instance):

| id | name | course |
| ------ | ------ | --- |
| 3 | Max | Computer Science |

```xml
<input>
	<schema>
		<relations>
			<relation name="student">
				<attribute name="id" type="int" />
				<attribute name="name" type="string" />
				<attribute name="course" type="string" />
			</relation>
		</relations>

		<dependencies>
			<egd>
				<body>
					<atom name="student">
						<variable name="id" type="V" index="2" />
						<variable name="name" type="V" index="1" />
						<variable name="course" type="V" index="1" />
					</atom>
					<atom name="student">
						<variable name="id" type="V" index="2" />
						<variable name="name" type="V" index="2" />
						<variable name="course" type="V" index="2" />
					</atom>
				</body>
				<head>
					<atom>
						<variable name="name" type="V" index="1" />
						<variable name="name" type="V" index="2" />
					</atom>
					<atom>
						<variable name="course" type="V" index="1" />
						<variable name="course" type="V" index="2" />
					</atom>
				</head>
			</egd>
		</dependencies>
	</schema>

	<instance>
		<atom name="student">
			<constant name="id" value="3" />
			<constant name="name" value="Max" />
			<null name="course" index="1" />
		</atom>
		<atom name="student">
			<constant name="id" value="3" />
			<null name="name" index="1" />
			<constant name="course" value="Computer Science" />
		</atom>
	</instance>
</input>
```

## example_query_3.xml

This example showcases query rewritings by using a tgd applied on a query.

* Chase parameter (tgd):

$\texttt{student}(V_{id},V_{name},V_{course}) \wedge \texttt{participant}(V_{module},V_{id}) \rightarrow \exists E_{semester}, E_{score} : \texttt{grade}(V_{module},V_{id},E_{semester},E_{score})$

* Chase object (query):

$\texttt{student}(\#V\_\text{id}\_2, \#V\_\text{name}\_2, \#V\_\text{course}\_2), \texttt{participant}(\#V\_\text{module}\_2, \#V\_\text{id}\_2), \texttt{student}(\#E\_\text{id}\_3, \#V\_\text{name}\_3, \#V\_\text{course}\_3),$ 
$\texttt{participant}(\#V\_\text{module}\_3, \#V\_\text{id}\_3), \texttt{grade}(\#V\_\text{module}\_1, \#V\_\text{id}\_1, \#E\_\text{semester}\_1, \#E\_\text{score}\_1)$ 
$\rightarrow (\#V\_\text{id}\_2, \#V\_\text{id}\_3)$

* result (query):

$\texttt{student}(\#V\_\text{id}\_2, \#V\_\text{name}\_2, \#V\_\text{course}\_2), \texttt{participant}(\#V\_\text{module}\_2, \#V\_\text{id}\_2), \texttt{student}(\#E\_\text{id}\_3, \#V\_\text{name}\_3, \#V\_\text{course}\_3),$ 
$\texttt{participant}(\#V\_\text{module}\_3, \#V\_\text{id}\_3), \texttt{grade}(\#V\_\text{module}\_1, \#V\_\text{id}\_1, \#E\_\text{semester}\_1, \#E\_\text{score}\_1),$ 
$\texttt{grade}(\#V\_\text{module}\_2, \#V\_\text{id}\_2, \#E\_\text{semester}\_2, \#E\_\text{score}\_2)$ 
$\rightarrow (\#V\_\text{id}\_2, \#V\_\text{id}\_3)$

```xml
<input>
	<schema>
		<relations>
			<relation name="student">
				<attribute name="id" type="int" />
				<attribute name="name" type="string" />
				<attribute name="course" type="string" />
			</relation>
			<relation name="participant">
				<attribute name="module" type="int" />
				<attribute name="id" type="int" />
			</relation>
			<relation name="grade">
				<attribute name="module" type="int" />
				<attribute name="id" type="int" />
				<attribute name="semester" type="int" />
				<attribute name="score" type="int" />
			</relation>
		</relations>

		<dependencies>
			<tgd>
				<body>
					<atom name="student">
						<variable name="id" type="V" index="1" />
						<variable name="name" type="V" index="1" />
						<variable name="course" type="V" index="1" />
					</atom>
					<atom name="participant">
						<variable name="module" type="V" index="1" />
						<variable name="id" type="V" index="1" />
					</atom>
				</body>
				<head>
					<atom name="grade">
						<variable name="module" type="V" index="1" />
						<variable name="id" type="V" index="1" />
						<variable name="semester" type="E" index="1" />
						<variable name="score" type="E" index="1" />
					</atom>
				</head>
			</tgd>
		</dependencies>
	</schema>

	<query>
		<body>
			<atom name="student">
				<variable name="id" type="V" index="2" />
				<variable name="name" type="V" index="2" />
				<variable name="course" type="V" index="2" />
			</atom>
			<atom name="participant">
				<variable name="module" type="V" index="2" />
				<variable name="id" type="V" index="2" />
			</atom>
			<atom name="student">
				<variable name="id" type="E" index="3" />
				<variable name="name" type="V" index="3" />
				<variable name="course" type="V" index="3" />
			</atom>
			<atom name="participant">
				<variable name="module" type="V" index="3" />
				<variable name="id" type="V" index="3" />
			</atom>
			<atom name="grade">
				<variable name="module" type="V" index="1" />
				<variable name="id" type="V" index="1" />
				<variable name="semester" type="E" index="1" />
				<variable name="score" type="E" index="1" />
			</atom>
		</body>
		<head>
			<atom>
				<variable name="id" type="V" index="2" />
				<variable name="id" type="V" index="3" />
			</atom>
		</head>
	</query>
</input>
```
